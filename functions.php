<?php

/*======= backend functionality ========*/
require_once 'functions-back-end.php';
/*======= backend functionality ========*/


/*======= API record functionality ========*/
require_once 'functions-API.php';
/*======= API record functionality ========*/

function al_youcoached_register_scripts_styles()
{
    $version = '0.0.3';
    $bower_dir = '/bower_components';

    wp_register_script('angular', get_template_directory_uri() . $bower_dir . '/angular/angular.min.js', array(), $version);
    wp_enqueue_script('angular');

    wp_register_script('angular-ui-router', get_template_directory_uri() . $bower_dir . '/angular-ui-router/angular-ui-router.js', array('angular'), $version);
    wp_enqueue_script('angular-ui-router');

    wp_register_script('angular-sanitize', get_template_directory_uri() . $bower_dir . '/angular-sanitize/angular-sanitize.min.js', array('angular'), $version);
    wp_enqueue_script('angular-sanitize');

    // image uploader (front-end)
    wp_enqueue_script('thickbox');
    wp_enqueue_style('thickbox');

    wp_register_script('ycweb', get_template_directory_uri() . '/assets/js/app.js', array('angular-ui-router', 'angular-sanitize'), $version, true);
    wp_register_script('ycweb', get_template_directory_uri() . '/assets/js/app.js', array('angular-ui-router', 'angular-sanitize'), $version, true);
    wp_localize_script('ycweb', 'themeDir', get_template_directory_uri());
    wp_localize_script('ycweb', 'endpoint', site_url('/wp-json/api/'));
    wp_localize_script('ycweb', 'nonce', wp_create_nonce('wp_rest'));


    $pages = get_all_pages_permalinks();

    wp_localize_script('ycweb', 'pages', $pages);
    wp_enqueue_script('ycweb');
//
    // menu
    wp_enqueue_script('slicknav', get_template_directory_uri() . '/assets/dist/jquery.slicknav.js', array('jquery'));
    wp_localize_script('slicknav', 'BlogUrl', get_bloginfo('wpurl'));
    // slider

    //simple modal
    wp_enqueue_script('simplemodal', get_template_directory_uri() . '/assets/js/jquery.simplemodal.js', array('jquery'));


//  wp_enqueue_script('stripe', 'https://js.stripe.com/v2/', array('jquery'), false, true);
    wp_enqueue_script('al-front-main', get_template_directory_uri() . '/assets/js/front-main.js', array('jquery', 'jssor-slider'));
    $data = array(
        'ajax_url' => admin_url('admin-ajax.php'),
        'site_url' => get_bloginfo('url')
    );
    wp_localize_script('al-front-main', 'urls', $data);

    wp_enqueue_script('jssor-slider', get_template_directory_uri() . '/assets/js/jssor.slider.min.js', array('jquery'));
    wp_enqueue_script('slider-jquery', '//code.jquery.com/ui/1.11.4/jquery-ui.js');
    wp_enqueue_script('punch-jquery', '//cdnjs.cloudflare.com/ajax/libs/jqueryui-touch-punch/0.2.3/jquery.ui.touch-punch.min.js');

    // for stripe
    wp_enqueue_script('bootstrap-validator', get_template_directory_uri() . '/assets/js/bootstrapValidator-min.js', array('jquery'), false, true);

    // styles
    wp_enqueue_style('grid', get_template_directory_uri() . '/assets/css/grid.css', array(), $version);
    wp_enqueue_style('style', get_template_directory_uri() . '/assets/css/style.css', array(), $version);
    wp_enqueue_style('style_my', get_template_directory_uri() . '/assets/css/style_my.css', array(), $version);
//  wp_enqueue_style( 'stylesheet-pure-css', get_template_directory_uri() . '/assets/css/stylesheet-pure-css.css', array(), $version );
    wp_enqueue_style('jquery-slider', '//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css');

    wp_enqueue_style('scss_output', get_template_directory_uri() . '/assets/css/scss-output.css', array(), $version);


}

function get_all_pages_permalinks()
{
    $args = array(
        'post_type' => 'page',
        'posts_per_page' => -1,
        'meta_query' => array(
            array(
                'key' => 'state_template'
            )
        )
    );
    $posts = get_posts($args);
    $pages = array();
    foreach ((array)$posts as $key => $menu_item) {
        $state_template = get_post_custom_values('state_template', $menu_item->ID);
        if (!empty($state_template[0])) {
            $pages[$state_template[0]] = get_the_permalink($menu_item->ID);
        }
    }

    return $pages;
}

add_action('wp_enqueue_scripts', 'al_youcoached_register_scripts_styles');

// enq scripts wp-login
function al_youcoached_register_scripts_styles_login()
{
    wp_enqueue_style('style_my', get_template_directory_uri() . '/assets/css/style_my.css', array(), '0.0.3');
}

add_action('login_enqueue_scripts', 'al_youcoached_register_scripts_styles_login', 10);
//

function al_youcoached_register_scripts_styles_admin()
{
    $version = '0.0.3';
    wp_enqueue_style('admin-main-css', get_template_directory_uri() . '/assets/css/admin-main.css', array(), $version);

    // chosen
    wp_enqueue_style('chosen', get_template_directory_uri() . '/library/chosen/chosen.css');

    wp_enqueue_script('chosen-jquery', get_template_directory_uri() . '/library/chosen/chosen.jquery.js', array('jquery'));

    wp_enqueue_script('admin-main', get_template_directory_uri() . '/assets/js/admin-main.js', array('jquery'));
    wp_localize_script('admin-main', 'ajax_url', admin_url('admin-ajax.php'));

    wp_enqueue_script('jquery-ui-datepicker');
    wp_enqueue_style('jquery-ui-style', '//ajax.googleapis.com/ajax/libs/jqueryui/1.8.1/themes/smoothness/jquery-ui.css', true);
}

add_action('admin_enqueue_scripts', 'al_youcoached_register_scripts_styles_admin');

///////////////////////////////////////////// API /////////////////////////////////////////////////////////////////

function basic_authentication_htaccess_contents($rules)
{
    $content = <<<EOD
\n# BEGIN Basic Authentication
<IfModule mod_rewrite.c>
RewriteEngine On
RewriteBase /
RewriteCond %{HTTP:Authorization} ^(.*)
RewriteRule ^(.*) - [E=HTTP_AUTHORIZATION:%1]
</IfModule>

Header set Access-Control-Allow-Origin "*"
Header set Access-Control-Allow-Headers "authorization, content-type"
Header set Access-Control-Allow-Credentials true


# END Basic Authentication\n
EOD;
    return $content . $rules;
}

add_filter('mod_rewrite_rules', 'basic_authentication_htaccess_contents');


//Header always set Access-Control-Allow-Origin "*"
//Header always set Access-Control-Allow-Headers "Authorization, Origin, X-Requested-With, Content-Type, Access-Control-Allow-Origin"
//Header always set Access-Control-Allow-Methods "PUT, GET, POST, DELETE, OPTIONS"


function __json($data)
{
    header('Content-Type: application/json');

    header('Access-Control-Allow-Origin: *');
    header('Access-Control-Allow-Methods: PUT, GET, POST, DELETE');
    header('Access-Control-Allow-Headers: Authorization, X-Custom-Header');
    header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Authorization');

    echo json_encode($data);
    exit;
}

// DEBUG
//add_action('wp_head', function(){
//  $user = wp_get_current_user();
//  delete_user_meta(21, 'sex');
//
//  var_dump(get_user_meta($user->ID, 'entries', true));
//  update_user_meta($user->ID, 'entries', 5);
//});

//add_filter( 'wp_headers', array( 'eg_send_cors_headers' ), 11, 1 );ssss
//function eg_send_cors_headers( $headers ) {
//
//  $headers['Access-Control-Allow-Origin']      = get_http_origin(); // Can't use wildcard origin for credentials requests, instead set it to the requesting origin
//  $headers['Access-Control-Allow-Credentials'] = 'true';
//
//  // Access-Control headers are received during OPTIONS requests
//  if ( 'OPTIONS' == $_SERVER['REQUEST_METHOD'] ) {
//
//    if ( isset( $_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD'] ) ) {
//      $headers['Access-Control-Allow-Methods'] = 'GET, POST, OPTIONS';
//    }
//
//    if ( isset( $_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS'] ) ) {
//      $headers['Access-Control-Allow-Headers'] = $_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS'];
//    }
//
//  }
//
//  return $headers;
//}


//  http://ycweb.woocoder.com/wp-json/api/get_post_info?slug=name
add_action('rest_api_init', function () {
    register_rest_route('api', '/get_post_info', array(
        'methods' => 'GET',
        'callback' => 'al_api_get_post_info',
    ));
});
function al_api_get_post_info(WP_REST_Request $request)
{

    $slug = $request->get_param('slug');

    $data = array('status' => false);

    if ($post_id = url_to_postid($slug)) {

        $state_template = get_post_custom_values('state_template', $post_id);

        $data = array(
            'status' => true,
            'template_name' => 'page',
            'post_id' => $post_id
        );
        $post = get_post($post_id);
        $data['post'] = $post;
        $data['raw_post'] = do_shortcode('[post-content id="' . $post_id . '"]');;
//    $data['raw_post'] = do_shortcode(get_post_field('post_content', $post_id));;

        if (!empty($state_template) && !empty($state_template[0])) {
            $data['template_name'] = $state_template[0];
        } else {
            if ($post->post_type === 'page') {
                $data['template_name'] = 'page-custom';
            }
        }

    }

    __json($data);
}

/*================================================== get post content with shortcode =====================================================*/

if (!function_exists('is_yes')) {
    function is_yes($arg)
    {
        if (is_string($arg)) {
            $arg = strtolower($arg);
        }
        return in_array($arg, array(true, 'true', 'yes', 'y', '1', 1), true);
    }
}

if (!function_exists('split_comma')) {
    function split_comma($csv)
    {
        return array_map('trim', explode(',', $csv));
    }
}

function wde_post_content_status($status = '', $default_status = 'publish')
{
    $valid_fields = array_intersect(split_comma($status), get_post_stati());

    if (empty($valid_fields)) {
        $valid_fields[] = $default_status;
    }

    return $valid_fields;
}

function wde_post_content_field($field)
{
    $allowed_fields = apply_filters(
        'post-content-allowed-fields',
        array(
            'post_author',
            'post_date',
            'post_date_gmt',
            'post_content',
            'post_title',
            'post_excerpt',
            'post_status',
            'comment_status',
            'ping_status',
            'post_name',
            'to_ping',
            'pinged',
            'post_modified',
            'post_modified_gmt',
            'post_content_filtered',
            'post_parent',
            'guid',
            'menu_order',
            'post_type',
            'post_mime_type',
            'comment_count'
        )
    );

    foreach (array($field, 'post_' . $field) as $field_name) {
        if (in_array($field_name, $allowed_fields)) {
            return $field_name;
        }
    }

    return false;
}

function wde_get_post_content_shortcode_attributes($attributes = array())
{
    $default_attributes = array(
        'id' => 0,
        'autop' => true,
        'shortcode' => true,
        'field' => 'post_content',
        'status' => 'publish'
    );

    $attributes = shortcode_atts(
        array_merge(
            $default_attributes,
            apply_filters('post-content-default-attributes', $default_attributes)
        ),
        $attributes,
        'post-content'
    );

    return array(
        'id' => (int)$attributes['id'],
        'autop' => is_yes($attributes['autop']),
        'shortcode' => is_yes($attributes['shortcode']),
        'field' => wde_post_content_field($attributes['field']),
        'status' => wde_post_content_status($attributes['status'])
    );
}

function wde_get_post_content_shortcode($attributes, $shortcode_content = null, $code = '')
{
    global $post;

    $content = '';
    $attributes = wde_get_post_content_shortcode_attributes($attributes);

    if ($attributes['field'] && get_the_ID() !== $attributes['id'] && in_array(get_post_status($attributes['id']), $attributes['status'])) {
        $original_post = $post;

        $post = get_post($attributes['id']);

        if (is_a($post, 'WP_Post')) {
            $content = get_post_field($attributes['field'], $post->ID);

            if (!empty($content)) {
                if ($attributes['shortcode']) {
                    $content = do_shortcode($content);
                }

                if ($attributes['autop']) {
                    $content = wpautop($content);
                }
            }
        }

        $post = $original_post;
    }

    return $content;
}

add_shortcode('post-content', 'wde_get_post_content_shortcode');

/*================================================== get post content with shortcode =====================================================*/

//  http://ycweb.woocoder.com/wp-json/api/get_post_by_id?post_id=post_id
add_action('rest_api_init', function () {
    register_rest_route('api', '/get_post_by_id', array(
        'methods' => 'GET',
        'callback' => 'al_api_get_post_by_id',
    ));
});
function al_api_get_post_by_id(WP_REST_Request $request)
{

    $post_id = intval($request->get_param('post_id'));

    $data = array('status' => false);
    $post = get_post($post_id, ARRAY_A);

    if (!is_null($post)) {
        $campaign = wp_get_post_terms($post['ID'], 'campaign');
        if (empty($campaign)) {
            $main_cat = wp_get_post_terms($post['ID'], 'main_category');
            if (isset($main_cat[0]->name)) {
                $main_cat_name = $main_cat[0]->name;
                $main_cat_color = get_term_meta($main_cat[0]->term_id, 'color', true);
                $main_cat_color = $main_cat_color ? '#' . $main_cat_color : '#000';
            } else {
                $main_cat_name = '';
                $main_cat_color = '#000';
            }
        } else {
            $camp_id = $campaign[0]->term_id;
            $main_cat = get_term(get_term_meta($camp_id, 'parent', true));
            $main_cat_color = get_term_meta($main_cat->term_id, 'color', true);
            $main_cat_color = $main_cat_color ? '#' . $main_cat_color : '#000';
            $main_cat_name = $main_cat->name;
        }
        $image_src = '';
        $image_id = get_post_meta($post['ID'], '_image_id', true);
        $image_src = wp_get_attachment_url($image_id);
        // here can be default messages
        $image_src = $image_src ? $image_src : '';

        $challenge = get_post_meta($post['ID'], 'challenge', true);
        $challenge_is = false;
        if ($challenge == 'yes') {
            $type = 'challenge';
            $challenge_is = 'not_accepted';
        } else {
            $type = 'message';
        }


        $message_content_main = $post_content = $post['post_content'];

        $user = wp_get_current_user();

        $saved_messages = get_user_meta($user->ID, 'saved_messages', true);
        $saved_messages = is_array($saved_messages) ? $saved_messages : array();

        $accept_challenges = get_user_meta($user->ID, 'accepted_challenges', true);
        $acc_chall_ids = $accept_challenges ? array_keys(unserialize($accept_challenges)) : array();

//    $acc_chall_all_data = get_user_meta($user->ID, 'accepted_challenges', true);
//    if($acc_chall_all_data){
//      $acc_chall_all_data = unserialize($acc_chall_all_data);
//      $acc_chall_ids = array_keys($acc_chall_all_data);
//    }else{
//      $acc_chall_ids = array();
//    }


        if (in_array($post_id, $acc_chall_ids)) {
            $challenge_is = 'accepted';
            $challenge_content = get_post_meta($post['ID'], 'accept-chall-descr', true);
            if (!empty($challenge_content)) {
                $post_content = $challenge_content;
            }
        }

        $completed_challenges = get_user_meta($user->ID, 'completed_challenges', true);
        $completed_challenges = $completed_challenges ? array_keys(unserialize($completed_challenges)) : array();
        if (in_array($post_id, $completed_challenges)) {
            $challenge_is = 'completed';
        }


        $data['status'] = true;
        $data['main_cat_link'] = get_bloginfo('url') . '/category/' . $main_cat_name;

        $data['post'] = array(
            'message_id' => $post['ID'],
            'message_title' => $post['post_title'],
            'message_image' => $image_src,
            'message_type' => $type,
            'message_content' => wpautop(do_shortcode($post_content)),
            'message_content_main' => wpautop(do_shortcode($message_content_main)),
            'main_cat_name' => $main_cat_name,
            'main_cat_name_color' => $main_cat_color,
            'in_array_then_bg' => in_array($post_id, $saved_messages),
            'img_src' => get_template_directory_uri() . '/img/saved-stencil.png',
            'challenge_is' => $challenge_is,
            'iframe_url' => 'https://www.facebook.com/plugins/share_button.php?href=' . get_bloginfo('url') . '/al_webhook.php?message_id=' . $post['ID']
        );
    }

    __json($data);
}

//  http://ycweb.woocoder.com/wp-json/api/is_user_logged_in
add_action('rest_api_init', function () {
    register_rest_route('api', '/is_user_logged_in', array(
        'methods' => 'GET',
        'callback' => 'al_api_is_user_logged_in',
    ));
});
function al_api_is_user_logged_in(WP_REST_Request $request)
{
    __json(array('is_user_logged_in' => is_user_logged_in()));
}

//  http://ycweb.woocoder.com/wp-json/api/al_user_has_subscription
add_action('rest_api_init', function () {
    register_rest_route('api', '/al_user_has_subscription', array(
        'methods' => 'GET',
        'callback' => 'al_api_user_has_subscription',
    ));
});
function al_api_user_has_subscription(WP_REST_Request $request)
{
    __json(array('al_user_has_subscription' => al_user_has_subscription()));
}


function get_quiz()
{
    $user = wp_get_current_user();

//  $terms = get_terms( array(
//    'taxonomy' => 'main_category',
//    'hide_empty' => false
//  ) );
//
//  // adding color to term array
//  foreach ( $terms as $k => $v) {
//    $terms[$k]->color = get_term_meta( $v->term_id, 'color', true );
//  }
//  delete_user_meta(62, 'quiz');
    return get_user_meta($user->ID, 'quiz', true);
}

//  http://ycweb.woocoder.com/wp-json/api/does_user_have_a_quiz
add_action('rest_api_init', function () {
    register_rest_route('api', '/does_user_have_a_quiz', array(
        'methods' => 'GET',
        'callback' => 'al_api_does_user_have_a_quiz',
    ));
});
function al_api_does_user_have_a_quiz(WP_REST_Request $request)
{

    if (is_user_logged_in()) {
        $quiz = get_quiz();
        if (empty($quiz)) {
            __json(array('does_user_have_a_quiz' => false));
        }
        $quiz = unserialize($quiz);

        if (isset($quiz['date'])) {
            __json(array('does_user_have_a_quiz' => true));
        } else {
            __json(array('does_user_have_a_quiz' => false));
        }
    }
}

//add_action( 'rest_api_init', function () {
//  register_rest_route( 'api', '/get', array(
//    'methods' => 'GET',
//    'callback' => 'al_api_get',
//  ) );
//} );
//function al_api_get( WP_REST_Request $request ) {
//
//  var_dump(is_user_logged_in());
//  exit;
//}

//  http://ycweb.woocoder.com/wp-json/api/get_default_messages
add_action('rest_api_init', function () {
    register_rest_route('api', '/get_default_messages', array(
        'methods' => 'GET',
        'callback' => 'al_api_get_default_messages',
    ));
});
function al_api_get_default_messages(WP_REST_Request $request)
{

    if (is_user_logged_in()) {

        $quiz = get_quiz();
        $quiz = unserialize($quiz);
        //

        $args = array(
            'taxonomy' => 'main_category',
            'hide_empty' => true,
            'number' => 6
        );
        $categories = get_categories($args);
        $result_messages = array();

        foreach ($categories as $category) {

            $color = get_term_meta($category->term_id, 'color', true);
            $color = $color ? '#' . $color : '#000';

            $args = array('hide_empty' => true,
                'meta_query' => array(
                    array(
                        'key' => 'parent',
                        'value' => $category->term_id,
                        'operator' => '='
                    )
                ),
                'suppress_filters' => false,);
            $campaigns = get_terms($args);
            $coach_name = $coach_avatar = '';
            if (isset($campaigns[0]->term_id)) {
                $coach_id = get_term_meta($campaigns[0]->term_id, 'coach', true);
                $coach = get_user_by('id', $coach_id);

                $coach_name = $coach->first_name . ' ' . $coach->last_name;
                $coach_avatar = user_avatar_fetch_avatar(array('item_id' => $coach->ID, 'width' => 170, 'height' => 170, 'html' => false));
            }


            $args_1 = array(
                'post_type' => 'default_message',
                'posts_per_page' => 1,
                'tax_query' => array(
                    array(
                        'taxonomy' => 'main_category',
                        'field' => 'term_id',
                        'terms' => $category->term_id,
                    ),
                )
            );
            $query = new WP_Query($args_1);
            $default_message_permalink = '';
            if ($query->have_posts()) {
                while ($query->have_posts()) {
                    $query->the_post();
                    $default_message_id = get_the_ID();
                    $default_message_permalink = get_permalink();
                }
            }
            if ($default_message_permalink != '') {
                $image_id = get_post_meta($default_message_id, '_image_id', true);
                $image_src = wp_get_attachment_url($image_id);

                $image_src = $image_src ? $image_src : get_template_directory_uri() . '/img/card1.png';
            } else {
                $image_src = get_template_directory_uri() . '/img/card1.png';
            }

            $result_messages[] = array(
                'name' => $category->name,
                'color' => $color,
                'coach_name' => $coach_name,
                'coach_avatar' => $coach_avatar,
                'permalink' => $default_message_permalink,
                'coach_quote' => get_field('quote', 'user_' . $coach->ID),
                'coach_quote_author' => get_field('quote_author', 'user_' . $coach->ID),
                'last_message_img' => $image_src
            );
        }

        __json(array('default_messages' => $result_messages));
    } else {
        __json('not logged');
    }
}

function get_default_messages($category = false)
{


    if ($category) {
        $args = array(
            'taxonomy' => 'main_category',
            'name' => $category
        );
        $category = get_terms($args);
        if ($category) {
            $categories = $category;
        }

    } else {
        $args = array(
            'taxonomy' => 'main_category',
            'hide_empty' => true,
            'number' => 6
        );
        $categories = get_categories($args);
    }


    $result_messages = array();

    foreach ($categories as $category) {
        $color = get_term_meta($category->term_id, 'color', true);
        $color = $color ? '#' . $color : '#000';
        $args = array(
            'taxonomy' => 'campaign',
            'hide_empty' => true,
            'meta_query' => array(
                array(
                    'key' => 'parent',
                    'value' => $category->term_id,
                    'operator' => '='
                )
            ),
            'suppress_filters' => false,
        );


        $campaigns = get_terms($args);

        $coach_name = $coach_avatar = '';
        if (isset($campaigns[0]->term_id)) {
            $coach_id = get_term_meta($campaigns[0]->term_id, 'coach', true);
            $coach = get_user_by('id', $coach_id);

            $coach_name = $coach->first_name . ' ' . $coach->last_name;
            $coach_avatar = user_avatar_fetch_avatar(array('item_id' => $coach->ID, 'width' => 200, 'height' => 200, 'html' => false));
        }


        $args_1 = array(
            'post_type' => 'default_message',
            'posts_per_page' => 1,
            'tax_query' => array(
                array(
                    'taxonomy' => 'main_category',
                    'field' => 'term_id',
                    'terms' => $category->term_id,
                ),
            )
        );
        $query = new WP_Query($args_1);
        $default_message_permalink = $excerpt = $title = '';
        if ($query->have_posts()) {
            while ($query->have_posts()) {
                $query->the_post();
                $default_message_id = get_the_ID();
                $default_message_permalink = get_permalink();

                $excerpt = get_the_excerpt();
                $content = get_the_content();
                if ($excerpt) {
                    $excerpt = strlen($excerpt) > 150 ? substr($excerpt, 0, 150) . '...' : $excerpt;
                } else {
                    $excerpt = strlen($content) > 150 ? substr($content, 0, 150) . '...' : $content;
                }

                $title = get_the_title();
            }
        }
        if ($default_message_permalink != '') {
            $image_id = get_post_meta($default_message_id, '_image_id', true);
            $image_src = wp_get_attachment_url($image_id);

            $image_src = $image_src ? $image_src : get_template_directory_uri() . '/img/card1.png';
        } else {
            $image_src = get_template_directory_uri() . '/img/card1.png';
        }

        $result_messages[$category->name] = array(
            'coach_avatar' => $coach_avatar,
            'coach_name' => $coach_name,
            'coach_quote' => get_field('quote', 'user_' . $coach->ID),
            'coach_quote_author' => get_field('quote_author', 'user_' . $coach->ID),
            'color' => $color,
            'last_message_img' => $image_src,
            'main_cat_name' => $category->name,
            'permalink' => $default_message_permalink,

            'excerpt' => $excerpt,
            'title' => $title,
            'post_id' => $default_message_id,
            'message_title' => $title
        );
    }


    return $result_messages;
}

//function al_get_campaign_ids_today()
//{
//    global $wpdb;
//    try {
//        $current_date = strtotime(date("Y") . '-' . date("m") . '-' . date("d"));
//        $table = $wpdb->prefix . 'al_postscat_by_date';
//        $sql = "SELECT * FROM $table WHERE date = $current_date";
//        $calendar_camp = $wpdb->get_results($sql, ARRAY_A);
//        $camp_ids_for_today = isset($calendar_camp[0]['category']) ? $calendar_camp[0]['category'] : '';
//        $camp_ids_for_today = array_filter(explode(',', $camp_ids_for_today));
//    } catch (Exception $e) {
//        $camp_ids_for_today = array();
//    }
//    return $camp_ids_for_today;
//}

//  http://ycweb.woocoder.com/wp-json/api/isset_camp_ids_for_today
add_action('rest_api_init', function () {
    register_rest_route('api', '/isset_camp_ids_for_today', array(
        'methods' => 'GET',
        'callback' => 'al_api_isset_camp_ids_for_today',
    ));
});
function al_api_isset_camp_ids_for_today( WP_REST_Request $request ) {

  if ( is_user_logged_in() ) {

//    $camp_ids_for_today = al_get_campaign_ids_today();

    __json( array( 'isset_camp_ids_for_today' => true) );
  }
}


//  http://ycweb.woocoder.com/wp-json/api/get_users_new_messages
add_action('rest_api_init', function () {
    register_rest_route('api', '/get_users_new_messages', array(
        'methods' => 'GET',
        'callback' => 'al_api_get_users_new_messages',
    ));
});
function al_api_get_users_new_messages(WP_REST_Request $request)
{
    if (is_user_logged_in()) {

        $default_messages = get_default_messages();

//    die;
        $user = wp_get_current_user();
        $quiz = get_quiz();
        $quiz = unserialize($quiz);
//        $camp_ids_for_today = al_get_campaign_ids_today();

//        $viewed_messages = get_user_meta($user->ID, 'viewed_messages', true);
//        $accept_challenges = get_user_meta($user->ID, 'accepted_challenges', true);
//        $accept_challenges = $accept_challenges ? array_keys(unserialize($accept_challenges)) : array();
//
//
        $completed_challenges = get_user_meta($user->ID, 'completed_challenges', true);
        $completed_challenges = $completed_challenges ? array_keys(unserialize($completed_challenges)) : array();

//        $not_in = array();
//        $not_in = array_merge($completed_challenges);

        if (!$quiz) {
            foreach ($default_messages as $k => $v) {
                $result_single[] = $v;
            }
            __json(array('new_messages' => $result_single));
        }


        /*new part*/
        $user_camp_sch = get_user_meta($user->ID, 'camp_to_show', true);
        if (!$user_camp_sch) {
            foreach ($default_messages as $k => $v) {
                $result_single[] = $v;
            }
            __json(array('new_messages' => $result_single));
        }

        // not empty - show scheduled messages and campaigns
        $home_user_camp_main_arr = array();
        foreach ($user_camp_sch as $camp_id => $val) {
            $parentid = get_term_meta($camp_id, 'parent', true);
//      $parent_term = get_term($parentid, 'main_category');
            // check if number of posts for campaign > 0
            if ($val)
                $home_user_camp_main_arr[$parentid][$camp_id] = $val;
        }

//    echo '<pre>';
//    print_r($home_user_camp_main_arr);
//    echo '</pre>';
        $homepage_clean_arr = array();
        foreach ($home_user_camp_main_arr as $main_cat => $campaigns) {

            $main_category = get_term($main_cat, 'main_category');
            $main_cat_link = get_term_link($main_cat);
            $color = get_term_meta($main_cat, 'color', true);
            $color = $color ? '#' . $color : '#000';
            $camp_messages = array();
            foreach ($campaigns as $campaign => $count_new_messages) {
                if ($count_new_messages == 0) continue;
                $args = array(
                    'post_type' => 'message',
//                    'post__not_in' => $not_in,
                    'orderby' => 'date',
                    'order' => 'ASC',
                    'posts_per_page' => -1,

                    'tax_query' => array(
                        array(
                            'taxonomy' => 'campaign',
                            'field' => 'id',
                            'terms' => $campaign,
                        ),
                    )
                );

                $query = new WP_Query($args);

                if ($query->have_posts()) {


                    $zero_arr = $order_arr = array();
                    foreach ($query->posts as $post) {

                        if ($post->menu_order == 0) {
                            array_unshift($zero_arr, $post);
                        } else {
                            $order_arr[] = $post;
                        }
                    }
                    usort($order_arr, function ($a, $b) {
                        return strcmp($a->menu_order, $b->menu_order);
                    });
                    $clean_array = array_merge($order_arr, $zero_arr);
                    $clean_array = array_slice($clean_array, 0, $count_new_messages);
//          echo '<br>';
//var_dump($clean_array);
                    $query->posts = $clean_array;


                    if ($clean_array) {
                        $i = count($clean_array);
                        while ($i) {
                          $query->the_post();

                          if(in_array(get_the_ID(), $completed_challenges)){
                            $i--;
                            continue;
                          }
                            $camp_messages[get_the_ID()] = get_the_date();
                            $i--;
                        }
                    }
                }
                wp_reset_postdata();
            }
//      var_dump($camp_messages);
            if (!$camp_messages) {
                continue;
            }
            // get the newest post
//var_dump($camp_messages);
            end($camp_messages);
            $latest_post_id = key($camp_messages);
//      var_dump($latest_post_id);
//      var_dump($latest_post_id);
//      continue;
//      var_dump($latest_post_id);
            // if no latest post - skip this main_category
            if (!$latest_post_id) {
                continue;
            }
            // if exists latest post - get all data for main category
            $last_post_camp = wp_get_post_terms($latest_post_id, 'campaign');
            if (!$last_post_camp) {
                continue;
            }
            $coach_id = get_term_meta($last_post_camp[0]->term_id, 'coach', true);
            $coach = get_user_by('id', $coach_id);

            $image_id = get_post_meta($latest_post_id, '_image_id', true);
            $image_src = wp_get_attachment_url($image_id);
            $image_src = $image_src ? $image_src : get_template_directory_uri() . '/img/card1.png';
            $message_title = get_the_title($latest_post_id);
            $permalink = get_the_permalink($latest_post_id);


            $homepage_clean_arr[$main_category->name] = array(
                'main_cat_name' => $main_category->name,
                'main_cat_link' => $main_cat_link,
                'order' => $main_category->term_order,
                'color' => $color,
                'coach_name' => $coach->first_name . ' ' . $coach->last_name,
                'coach_avatar' => user_avatar_fetch_avatar(array('item_id' => $coach->ID, 'width' => 170, 'height' => 170, 'html' => false)),
                'coach_quote' => get_field('quote', 'user_' . $coach->ID),
                'coach_quote_author' => get_field('quote_author', 'user_' . $coach->ID),
                'last_message_img' => $image_src,
                'permalink' => $permalink,
                'message_title' => $message_title,
                'post_id' => $latest_post_id
            );
        }

        /*new part end*/

        $merged_arrays_assoc = array_merge($default_messages, $homepage_clean_arr);


        $result_single = array();
//    var_dump($merged_arrays_assoc);
        foreach ($merged_arrays_assoc as $k => $v) {
            $result_single[] = $v;
        }

        $pages = get_all_pages_permalinks();

        $rewards = get_rewards(1);
        if (isset($rewards[0]) && isset($rewards[0]->post_title)) {
            $object = new stdClass();;
            $object->main_cat_name = 'rewards';
            $object->main_cat_link = $pages['rewards'];
            $object->order = 7;
            $object->color = '#494949';
            $object->last_message_img = get_attached_image_src($rewards[0]->ID);
            $object->permalink = $pages['rewards'] . '?message_id=' . $rewards[0]->ID;
            $object->message_title = $rewards[0]->post_title;
            $object->post_id = $rewards[0]->ID;

            $result_single[] = $object;
        }


        __json(array('new_messages' => $result_single));
    } else {
        __json('not logged');
    }
}

//  http://ycweb.woocoder.com/wp-json/api/get_user_name
add_action('rest_api_init', function () {
    register_rest_route('api', '/get_user_name', array(
        'methods' => 'GET',
        'callback' => 'al_api_get_user_name',
    ));
});
function al_api_get_user_name(WP_REST_Request $request)
{
    if (is_user_logged_in()) {
        $user = wp_get_current_user();
        $user_info = get_userdata($user->ID);
        $first_last = $user_info->first_name . ' ' . $user_info->last_name;
        $user_name = $first_last !== ' ' ? $first_last : $user->user_nicename;
        __json(array('user_name' => $user_name));
    }
}

//  http://ycweb.woocoder.com/wp-json/api/isset_main_category
add_action('rest_api_init', function () {
    register_rest_route('api', '/isset_main_category', array(
        'methods' => 'GET',
        'callback' => 'al_api_isset_main_category',
    ));
});
function al_api_isset_main_category(WP_REST_Request $request)
{
    if (is_user_logged_in()) {

        $slug = $request->get_param('slug');

        $slug = trim($slug, '/');
        $main_category = get_term_by('name', $slug, 'main_category');
        __json(array('isset_main_category' => !empty($main_category)));
    }
}

//  http://ycweb.woocoder.com/wp-json/api/get_camp_ids
add_action('rest_api_init', function () {
    register_rest_route('api', '/get_camp_ids', array(
        'methods' => 'GET',
        'callback' => 'al_api_get_camp_ids',
    ));
});
function al_api_get_camp_ids( WP_REST_Request $request ) {
  if ( is_user_logged_in() ) {

    global $wpdb;
    $user = wp_get_current_user();

    $main_category_name = $slug = $request->get_param( 'slug' );



    $slug = trim( $slug, '/' );



    $main_category = get_term_by('name', $slug, 'main_category');

    $quiz = get_quiz();
    $quiz = unserialize($quiz);

    $main_category_color = get_term_meta($main_category->term_id, 'color', true);
    $main_category_color = $main_category_color ? '#' . $main_category_color : '#000';

//    $current_date = strtotime(date("Y") . '-' . date("m") . '-' . date("d"));
//    $table = $wpdb->prefix . 'al_postscat_by_date';
//    $sql = "SELECT * FROM $table WHERE date = $current_date";
//    $calendar_camp = $wpdb->get_results($sql, ARRAY_A);
//    $camp_ids_for_today = $calendar_camp[0]['category'];


    $main_category_name = strtolower(trim( $main_category_name, '/'));
    $accept_challenges = get_user_meta($user->ID, 'accepted_challenges', true);
    $accept_challenges = $accept_challenges ? array_keys(unserialize($accept_challenges)) : array();

    $completed_challenges = get_user_meta($user->ID, 'completed_challenges', true);
    $completed_challenges = $completed_challenges ? array_keys(unserialize($completed_challenges)) : array();

    $not_in = array();

    $saved_messages = get_user_meta($user->ID, 'saved_messages', true);
    $saved_messages = is_array($saved_messages) ? $saved_messages : array();


    if ($main_category_name == 'fashion') {
      $data['body_type'] = isset($quiz['body_type']) ? $quiz['body_type'] : '';
    }
    else {
      $data['body_type'] = '';
    }



    /*Default message part*/
    $data = array();
    $data['posts'] = array();
    $default_message = get_default_messages($slug);
    foreach ($default_message as $k => $v) {
      $default_message = array($v);
    }
//    var_dump($default_message);
    if($default_message){
      if(isset($default_message[0])){

        $default_message = $default_message[0];

        $data['main_category_name'] = strtoupper($slug);
        $data['user_avatar_fetch_avatar'] = $default_message['coach_avatar'];
        $data['main_category_color'] = $main_category_color;
        $data['fname'] = $default_message['coach_name'];


        $data['quote'] = $default_message['coach_quote'];
        $data['quote_author'] = $default_message['coach_quote_author'];
        $data['category'] = strtoupper($slug);

        $data['self_goals'] = get_permalink_by_cf('self-goals');

        $data['posts'][] = array(
          'the_ID' =>  get_the_ID(),
          'the_permalink' =>  $default_message['permalink'],
          'image_src' => $default_message['last_message_img'],
          'main_category_color' =>  $main_category_color,
          'the_title' =>  $default_message['title'],
          'the_excerpt' =>  $default_message['excerpt'],
          'in_array_then_bg' =>  'default_message',
          'img_src' => get_template_directory_uri(). '/img/saved-stencil.png',
        );
      }
    }
    /*Default message part end*/






    /*new part*/

    $viewed_messages_ids = get_user_meta($user->ID, 'viewed_messages', true);
    $viewed_messages_ids = $viewed_messages_ids ? $viewed_messages_ids : array();

    $viewed_challenges = get_user_meta($user->ID, 'viewed_challenges', true);
    $viewed_challenges = $viewed_challenges ? $viewed_challenges : array();

//    $viewed_challenges_messages = array_merge($viewed_messages_ids, $viewed_challenges);
    $viewed_challenges_messages = $viewed_messages_ids;

    $user_camp_sch = get_user_meta($user->ID, 'camp_to_show', true);
    if(!$user_camp_sch){
      __json( array( 'status' => 'yes', 'data' => $data ) );
    }

    // not empty - show scheduled messages and campaigns
    $home_user_camp_main_arr  = array();
    foreach ($user_camp_sch as $camp_id => $val) {
      $parentid = get_term_meta($camp_id,'parent', true);
//      $parent_term = get_term($parentid, 'main_category');
      // check if number of posts for campaign > 0
      if($val)
        $home_user_camp_main_arr[$parentid][$camp_id] = $val;
    }

//    echo '<pre>';
//    print_r($home_user_camp_main_arr);
//    echo '</pre>';
    $homepage_clean_arr = array();
    $last_camp_id = false;

    foreach ($home_user_camp_main_arr as $main_cat => $campaigns) {
//      var_dump($campaigns);

      $main_category = get_term($main_cat, 'main_category');


      // search results only for this category
      if(strtolower($main_category->name) != $main_category_name){
        continue;
      }


      $main_cat_link = get_term_link($main_cat);
      $color = get_term_meta($main_cat, 'color', true);
      $color = $color ? '#' . $color  : '#000';
      $camp_messages = array();
      foreach ($campaigns as $campaign => $count_new_messages) {
//var_dump($count_new_messages);
        $args = array(
          'post_type' => 'message',
          'post__not_in' => $not_in,
          'posts_per_page' => -1,
          'tax_query' => array(
            array(
              'taxonomy' => 'campaign',
              'field'    => 'id',
              'terms'    => $campaign,
            ),
          )
        );

        $query = new WP_Query( $args );
        if($query->have_posts()) {
//          $data[1][] = 'have posts';

          $last_camp_id = $campaign;

          $zero_arr = $order_arr = array();
          foreach ($query->posts as $post) {


            if ($post->menu_order == 0) {
              array_unshift($zero_arr, $post);
            } else {
              $order_arr[] = $post;
            }
          }
          usort($order_arr, function($a, $b){
            return strcmp($a->menu_order, $b->menu_order);
          });

          $clean_array = array_merge($order_arr, $zero_arr);
          $clean_array = array_slice($clean_array, 0, $count_new_messages);

          $query->posts = $clean_array;
          $i = count($clean_array);
          while ( $i ) {
//            $query->the_post();
//            $camp_messages[get_the_ID()] = strtotime(get_the_date());
            $query->the_post();
            if(in_array($post->ID, $completed_challenges)){
              $i--;
              continue;
            }

            $image_id = get_post_meta(get_the_ID(), '_image_id', true);
            $image_src = wp_get_attachment_url($image_id);
            $image_src = $image_src ? $image_src : get_template_directory_uri() . '/img/card1.png';
            $is_challenge = get_post_meta(get_the_ID(), 'challenge', true);

            $excerpt = get_the_excerpt();
            $content = get_the_content();
            if($excerpt){
              $excerpt = strlen($excerpt) > 150 ? substr($excerpt, 0, 150) . '...' : $excerpt;
            }else{
              $excerpt = strlen($content) > 150 ? substr($content, 0, 150) . '...' : $content;
            }

            $data['posts'][] = array(
              'the_ID' =>  get_the_ID(),
              'accepted' => in_array(get_the_ID(), $accept_challenges),
              'the_permalink' =>  get_the_permalink(),
              'image_src' => $image_src,
              'main_category_color' =>  $main_category_color,
              'the_title' =>  html_entity_decode( get_the_title() ),
              'the_excerpt' =>  html_entity_decode( $excerpt ),
              'in_array_then_bg' => in_array(get_the_ID(), $saved_messages),
              'img_src' =>  get_template_directory_uri(). '/img/saved-stencil.png',
              'is_new_for_user' => !in_array(get_the_ID(), $viewed_challenges_messages)
            );
            $i--;
          }


        }
        wp_reset_postdata();
      }

    }
    if(!isset($last_camp_id) || !$last_camp_id){
      __json( array( 'status' => 'yes', 'data' => $data ) );
    }
    $coach_id = get_term_meta($last_camp_id, 'coach', true);
    $coach = get_user_by('id', $coach_id);
    $data['main_category_name'] = trim( strtoupper($main_category_name), '/' );
    $data['user_avatar_fetch_avatar'] = user_avatar_fetch_avatar(array('item_id' => $coach->ID, 'width' => 200, 'height' => 200, 'html' => false));
    $data['fname'] = $coach->first_name . ' ' . $coach->last_name;


    $data['quote'] = get_field('quote', 'user_' . $coach_id);
    $data['quote_author'] = get_field('quote_author', 'user_' . $coach_id);
    $data['category'] = strtoupper($main_category_name);
    /*new part end*/




    __json( array( 'status' => 'yes', 'data' => $data ) );

  }
}


//  http://ycweb.woocoder.com/wp-json/api/get_self_goals
add_action('rest_api_init', function () {
    register_rest_route('api', '/get_self_goals', array(
        'methods' => 'GET',
        'callback' => 'al_api_get_self_goals',
    ));
});
function al_api_get_self_goals(WP_REST_Request $request)
{
    if (is_user_logged_in()) {
        $user = wp_get_current_user();
        $category = $request->get_param('category');
        $category = strtolower(trim($category, '/'));
        $self_goals = get_user_meta($user->ID, 'self_goals', true);
        $goals = unserialize($self_goals);

//    var_dump($category);
//echo '<pre>';
//print_r($goals);
//echo '</pre>';
        $data = array();
        $data['data'] = array();
        $data['status'] = false;
        if (!empty($goals)) {
            foreach ($goals as $k => $v) {
                if (isset($v['category']) && $v['category'] == $category) {

                    if ($v['status'] === 1) {
                        continue;
                    }
                    $data['status'] = true;
                    $data['data']['id'] = $k;
                    $data['data']['status'] = $v['status'];
                    $data['data']['category'] = $v['category'];
                    $data['data']['text'] = $v['text'];
                }
            }
        }
        __json($data);
    }
}

function get_challenge_percentage($user_id){
  if(!$user_id){
    return false;
  }
  $user = get_userdata($user_id);

  if($user === false){
    return false;
  }
  $percentage = 0;
  $points_user = get_user_meta($user->ID, 'points', true);

  // let it be this way, because points possibly can be whatever value
  if($points_user){
    $percentage = round( $points_user / (100 ), 2)*100;
  }

  return $percentage;
}

//  http://ycweb.woocoder.com/wp-json/api/get_challenges
add_action('rest_api_init', function () {
    register_rest_route('api', '/get_challenges', array(
        'methods' => 'GET',
        'callback' => 'al_api_get_challenges',
    ));
});
function al_api_get_challenges( WP_REST_Request $request ) {
  if ( is_user_logged_in() ) {

    global $wpdb;
    $data = array();
    $user = wp_get_current_user();

    $quiz = get_user_meta($user->ID, 'quiz', true);

    $quiz = unserialize($quiz);

    // user meta accepted challenges
    $acc_chall_all_data = get_user_meta($user->ID, 'accepted_challenges', true);
    if($acc_chall_all_data){
      $acc_chall_all_data = unserialize($acc_chall_all_data);
      $acc_chall_ids = array_keys($acc_chall_all_data);
    }else{
      $acc_chall_ids = array();
    }
//    var_dump($acc_chall_ids);
//    foreach ($acc_chall_ids as $acc_chall_id) {
//      $post = get_post($acc_chall_id);
//      var_dump($post->ID);
////      var_dump(get_post_meta($post->ID, 'challenge', true));
//      echo '<br>';
//      echo '<br>';
//    }

    // viewed challenges
    $viewed_challenges = get_user_meta($user->ID, 'viewed_challenges', true);
    if(empty($viewed_challenges)){
      $viewed_challenges = array();
    }

    $completed_challenges = get_user_meta($user->ID, 'completed_challenges', true);
    if(!empty($completed_challenges)){
      $completed_challenges = unserialize($completed_challenges);
      $completed_challenges = array_keys($completed_challenges);
    }else{
      $completed_challenges = array();
    }
//    var_dump($completed_challenges);


//    $viewed_and_completed_challenges = array_unique(array_merge($viewed_challenges, $completed_challenges, $acc_chall_ids));
    $viewed_and_completed_challenges = array_unique(array_merge( $completed_challenges));
//    var_dump($viewed_and_completed_challenges);

    // user meta completed challenges
    $compl_chall_all_data = get_user_meta($user->ID, 'completed_challenges', true);

    $compl_chall_ids = array();
    $compl_tooday_chall = 0;
    $compl_chall_count = 0;
    if($compl_chall_all_data){
      $compl_chall_all_data = unserialize($compl_chall_all_data);
//      var_dump($compl_chall_all_data);

      $compl_chall_all_data_old = $compl_chall_all_data;
      $compl_chall_all_data = array();
//      var_dump($compl_chall_all_data_old);
      foreach ($compl_chall_all_data_old as $item => $k) {
        if(get_post_status($item) && get_post_status($item) === 'published');
        $compl_chall_all_data[] = $item;
      }
//      var_dump($compl_chall_all_data);
      $compl_chall_count = count($compl_chall_all_data);
      $compl_chall_ids = array_keys($compl_chall_all_data);
      $date_now = strtotime(date("Y").'-'.date("m").'-'.date("d"));
      foreach ($compl_chall_all_data_old as $item) {
        if($item == $date_now){
          $compl_tooday_chall++;
        }
      }
    }


    $new_challenges_ids = $new_challenges_count = 0;


//    $current_date = strtotime(date("Y").'-'.date("m").'-'.date("d"));
//    $table = $wpdb->prefix . 'al_postscat_by_date';
//    $sql = "SELECT * FROM $table WHERE date = $current_date";
//    $calendar_camp = $wpdb->get_results($sql, ARRAY_A);
//    if(isset($calendar_camp[0]['category'])){
//      $camp_ids_for_today = $calendar_camp[0]['category'];
//      $camp_ids_for_today = explode(',', $camp_ids_for_today);
//
//      $campaigns_from_quiz = array();
//      foreach ($quiz as $k => $v) {
//        // skip for gender and personality
//        if( $k == 'user_personality') continue;
//
//        $args = array('hide_empty' => true,
////        'include' => $camp_ids_for_today,
//          'meta_query' => array(
//            array(
//              'key'=> 'relations',
//              'value' => $v,
//              'compare' => '='
//            )
//          ),
//          'suppress_filters' => false,);
//
//        $campaigns = get_terms($args);
//        if($campaigns){
//          foreach ($campaigns as $campaign) {
//            $campaigns_from_quiz[] = $campaign->term_id;
//          }
//        }
//      }
//      $campaigns_from_quiz = array_unique($campaigns_from_quiz);
//      $campaigns_from_quiz = array_intersect($campaigns_from_quiz, $camp_ids_for_today);
//
//      $today_main_categories = array();
//      foreach ($campaigns_from_quiz as $k => $v) {
//        $parent = get_term_meta($v,'parent', true);
//
//        if(isset($today_main_categories[$parent])){
//          $today_main_categories[$parent] .= ',' . $v;
//        }else{
//          $today_main_categories[$parent] = $v;
//        }
//      }
//
//      $new_challenges_ids = array();
//
//      $i = 0;
//      foreach ($today_main_categories as $k => $v) {
//        // check if campaign has unread messages
//        $camp_ids_clean = explode(',', $v);
//        foreach($camp_ids_clean as $key => $value){
//          $args = array(
//            'post_type' => 'message',
//            'post__not_in' => $viewed_and_completed_challenges,
//            'orderby' => 'date',
//            'order' => 'DESC',
//            'post_status' => 'publish',
//            'posts_per_page' => -1,
//            'tax_query' => array(
//              array(
//                'taxonomy' => 'campaign',
//                'field'    => 'id',
//                'terms'    => $value,
//                'operator' => 'IN'
//              ),
//            ),
//            'meta_query' => array(
//              array(
//                'key' => 'challenge',
//                'value'    => 'yes',
//                'compare' => '='
//              ),
//            ),
//          );
//          $query = new WP_Query( $args );
//
//          if($query->have_posts()) {
//
//            $not_empty_camp_id = $value;
//
//            while ( $query->have_posts() ) {
//              $query->the_post();
//              $new_challenges_ids[] = get_the_ID();
//            }
//          }
//          wp_reset_postdata();
//        }
//        $i++;
//      }
//
//      $new_challenges_ids = array_unique($new_challenges_ids);
//      $new_challenges_count = count($new_challenges_ids);
//    }

    $new_challenges_array = array();
    $i = 0;
    /////////////////////////////////////////////////////////////////////////////////
    // adding to new challenges accepted challenges
//    $new_challenges_ids = array_merge($new_challenges_ids, $acc_chall_ids);
    $new_challenges_ids = $acc_chall_ids;
//    var_dump($new_challenges_ids);
    /////////////////////////////////////////////////////////////////////////////////

    $acc_challenges_count = 0;
    $percentage = get_challenge_percentage($user->ID);
    $data['percentage'] = $percentage === false ? 0 : $percentage;

    if($new_challenges_ids){
      $acc_challenges_count = count($new_challenges_ids);
    }

    if(!empty($new_challenges_ids[0])){
      foreach ($new_challenges_ids as $new_challenges_id) {

        if(!get_post_status($new_challenges_id) && get_post_status($new_challenges_id) !== 'published')
          continue;

        $new_chal_camp = wp_get_post_terms($new_challenges_id, 'campaign');


        $new_challenge_camp_id = $new_chal_camp[0]->term_id;
        $new_chal_main_cat = get_term($new_chal_camp[0]->parent);
        //    var_dump($new_chal_main_cat);echo '<br>';
        $main_cat_color = get_term_meta($new_chal_main_cat->term_id, 'color', true);
        $main_cat_color = $main_cat_color ? '#' . $main_cat_color : '#000';

        $image_id = get_post_meta( $new_challenges_id, '_image_id', true );
        $image_src = wp_get_attachment_url( $image_id );
        $image_src = $image_src ? $image_src : get_template_directory_uri() . '/img/card1.png';

        $post = get_post($new_challenges_id);

        $excerpt = '';
        if($post->post_excerpt){
          $excerpt = strlen($post->post_excerpt) > 150 ? substr($post->post_excerpt, 0, 150) . '...' : $post->post_excerpt;
        }else{
          $excerpt = strlen($post->post_content) > 150 ? substr($post->post_content, 0, 150) . '...' : $post->post_content;
        }

        $new_challenges_array[$i]['id'] = $new_challenges_id;
        $new_challenges_array[$i]['title'] = $post->post_title;
        $new_challenges_array[$i]['excerpt'] = $excerpt;
        $new_challenges_array[$i]['main_cat_name'] = $new_chal_main_cat->name;
        $new_challenges_array[$i]['color'] = $main_cat_color;
        $new_challenges_array[$i]['image'] = $image_src;
        $new_challenges_array[$i]['in_array'] = in_array($new_challenges_id, $acc_chall_ids);
        $new_challenges_array[$i]['in_array_completed'] = in_array($new_challenges_id, $compl_chall_ids);
        $new_challenges_array[$i]['permalink'] = get_the_permalink( $new_challenges_id );



        $i++;
      }
    }
//    var_dump($new_challenges_array);

    $data['isset_new_challenge_camp_id'] = false;
    $qoute = '';
    $qoute_author = '';
    if ( isset( $new_challenge_camp_id ) ) {
      $coach_id = get_term_meta($new_challenge_camp_id, 'coach', true);
      $coach = get_user_by('id', $coach_id);
      $qoute = get_field('quote', 'user_' . $coach->ID);
      $qoute_author = get_field('quote_author', 'user_' . $coach->ID);
      $qoute = $qoute ? $qoute : '';
      $qoute_author = $qoute_author ? $qoute_author : '';
      $data['isset_new_challenge_camp_id'] = true;

    }

    $data['new_challenges'] = $new_challenges_array;
    $data['qoute'] = $qoute;
    $data['qoute_author'] = $qoute_author;
    $data['src_waves'] = get_template_directory_uri() . '/img/waves.png';
    $data['new_challenges_count'] = $new_challenges_count;
    $data['acc_challenges_count'] = $acc_challenges_count;
    $data['compl_tooday_chall'] = $compl_tooday_chall;
    $data['compl_chall_count'] = $compl_chall_count;
    $data['specif_challenges_page'] = get_permalink_by_cf('specif-challenges');
    $data['rewards_page'] = get_permalink_by_cf('rewards');


    /*06.09.2016*/
    $data['user_entries'] = get_user_meta($user->ID, 'entries', true);
    $data['user_points'] = get_user_meta($user->ID, 'points', true);
    /*06.09.2016*/

    __json( $data );
  }
}


//  http://ycweb.woocoder.com/wp-json/api/get_specif_challenges
add_action('rest_api_init', function () {
    register_rest_route('api', '/get_specif_challenges', array(
        'methods' => 'GET',
        'callback' => 'al_api_get_specif_challenges',
    ));
});
function al_api_get_specif_challenges( WP_REST_Request $request ) {
  if ( is_user_logged_in() ) {

    $data = array();


    $user = wp_get_current_user();

    $category = $request->get_param( 'category' );

    if ( ! in_array( $category, array( 'completed-today', 'new-challenges', 'completed' ) ) ) {
      $category = 'new-challenges';
    }

    $flag = false;

    if($category == 'completed-today'){
      // completed today
      $compl_chall_all_data = get_user_meta($user->ID, 'completed_challenges', true);

      if($compl_chall_all_data){
        $compl_chall_all_data = unserialize($compl_chall_all_data);

        $date_now = strtotime(date("Y").'-'.date("m").'-'.date("d"));
        $result_array = array();
        foreach ($compl_chall_all_data as $k => $v) {
          if($v == $date_now){
            $result_array[] = $k;
          }
        }
      }
      $flag = true;
    }
    else if($category == 'completed'){
      // completed challenges
      $compl_chall_all_data = get_user_meta($user->ID, 'completed_challenges', true);
      if($compl_chall_all_data){
        $compl_chall_all_data = unserialize($compl_chall_all_data);
        $result_array = array_keys($compl_chall_all_data);
      }
      $flag = true;
    }

    //1
    if($flag){

      if(!empty($result_array)){

        $args = array(
          'post_type' => 'message',
          'orderby' => 'date',
          'order' => 'DESC',
          'post__in' => $result_array,
          'posts_per_page' => -1,

          'meta_query' => array(
            array(
              'key' => 'challenge',
              'value'    => 'yes',
              'compare' => '='

            ),
          ),
        );
        $query = new WP_Query( $args );

        if($query->have_posts()) {
          $data['category'] = $category;
          $data['posts'] = array();
          $data['status'] = 'completed';


          while ($query->have_posts()) {
            $query->the_post();
//                the_title();

            $campaign = wp_get_post_terms(get_the_ID(), 'campaign');
            if(!empty($campaign[0]->term_id)) {
              $camp_id = $campaign[0]->term_id;
            }else{
              continue;
            }


            $main_cat = get_term(get_term_meta($camp_id, 'parent', true));
            $main_cat_color = get_term_meta($main_cat->term_id, 'color', true);

            $image_id = get_post_meta(get_the_ID(), '_image_id', true);
            $image_src = wp_get_attachment_url($image_id);
            $image_src = $image_src ? $image_src : get_template_directory_uri() . '/img/card1.png';

            $data['posts'][] = array(
              'camp_id' => $camp_id,
              'main_cat_color' => $main_cat_color,
              'image_id' => $image_id,
              'image_src' => $image_src,
              'main_cat_name' => $main_cat->name,
              'the_ID' => get_the_ID(),
              'the_excerpt' => html_entity_decode( get_the_excerpt() ),
              'the_title' => html_entity_decode( get_the_title() ),
              'the_permalink' => get_the_permalink( get_the_ID() )
            );
          }

        } else {
          $data['status'] = 'no_challenges_with_category';

        }

      } else {
        $data['status'] = 'no_challenges';
      }
    } else {
//    if new challenges

      $new_challenges = get_users_new_challenges($user->ID);
      if(!$new_challenges){
        $data['status'] = 'no_new_challenges';
      }else{
        $data['status'] = 'no_completed';
        $data['posts'] = $new_challenges;
      }

    }
    __json( $data );
  }
}


function get_rewards_color()
{
    return '9100ff';
}

//  http://ycweb.woocoder.com/wp-json/api/get_saved_messages
add_action('rest_api_init', function () {
    register_rest_route('api', '/get_saved_messages', array(
        'methods' => 'GET',
        'callback' => 'al_api_get_saved_messages',
    ));
});
function al_api_get_saved_messages(WP_REST_Request $request)
{
    if (is_user_logged_in()) {

        $user = wp_get_current_user();

        $saved_messages = get_user_meta($user->ID, 'saved_messages', true);
        $data = array();


        $args = array(
            'post_type' => array('message', 'rewards'),
            'post__in' => $saved_messages,
            'post_status' => 'publish',
//      'meta_query' => array(
//        array(
//          'key' => 'challenge',
//          'value'    => 'yes',
//          'compare' => '!='
//        ),
//      ),

        );
        $query = new WP_Query($args);

        if (!empty($saved_messages) && $query->have_posts()) {
            $data['status'] = true;

            $data['messages'] = array();
            while ($query->have_posts()) {

                $query->the_post();
                $post = get_post(get_the_ID());
                $campaign = wp_get_post_terms(get_the_ID(), 'campaign');
                if (isset($campaign[0])) {
                    $camp_id = $campaign[0]->term_id;
                    $main_cat = get_term(get_term_meta($camp_id, 'parent', true));
                    $main_cat_name = $main_cat->name;
                    $main_cat_color = get_term_meta($main_cat->term_id, 'color', true);
                } else {
                    $main_cat_name = 'rewards';
                    $main_cat_color = get_rewards_color();
                }

                $image_id = get_post_meta(get_the_ID(), '_image_id', true);
                $image_src = wp_get_attachment_url($image_id);
                $image_src = $image_src ? $image_src : get_template_directory_uri() . '/img/card1.png';

                $excerpt = get_the_excerpt();
                $content = get_the_content();
                if ($excerpt) {
                    $excerpt = strlen($excerpt) > 150 ? substr($excerpt, 0, 150) . '...' : $excerpt;
                } else {
                    $excerpt = strlen($content) > 150 ? substr($content, 0, 150) . '...' : $content;
                }

                $data['messages'][] = array(
                    'main_cat_name' => $main_cat_name,
                    'main_cat_color' => $main_cat_color,
                    'image_src' => $image_src,
                    'the_ID' => get_the_ID(),
                    'the_title' => html_entity_decode(get_the_title()),
                    'the_excerpt' => html_entity_decode($excerpt),
                    'the_permalink' => get_the_permalink(),
                    'saved_stencil_img' => get_template_directory_uri() . '/img/saved-stencil.png',
                    'in_array' => in_array(get_the_ID(), $saved_messages),
                    'post_type' => $post->post_type
                );

            }

        } else {
            $data['status'] = false;
        }

        __json($data);
    }
}


function get_user_stats_on_challenge_category($userId, $categoryId)
{
    global $wpdb;

    $completedChallengesQuery = 
        "select count(*) 
        from wpqr_challenge_status as cl
        join wpqr_term_relationships as tr on cl.challenge_id = tr.object_id
        where tr.term_taxonomy_id = {$categoryId} 
        and cl.STATUS = 4 and cl.user_id = {$userId}";

    $allChallengesQuery = 
        "select count(*) 
        from wpqr_posts as p 
        join wpqr_term_relationships as tr on p.ID = tr.object_id
        where p.post_type='challenge' and tr.term_taxonomy_id = {$categoryId}";

    $allChallenges = $wpdb -> get_var($allChallengesQuery);
    $completedChallenges = $wpdb -> get_var($completedChallengesQuery);

    return Array(
        'all_challenges' => $allChallenges,
        'completed_challenges' => $completedChallenges,
        'progress' => $allChallenges != 0 ? $completedChallenges/$allChallenges : 0
    );
}

function get_users_new_challenges($user_id){
  if(!$user_id){
    return false;
  }
  $user = get_userdata($user_id);

  if($user === false){
    return false;
  }

  $user_camp_sch = get_user_meta($user_id, 'camp_to_show', true);

  // if there is nothing in inbox
  if(empty($user_camp_sch)){
    return false;
  }

  // should be a viewed challenges
  $viewed_messages = get_user_meta($user_id, 'viewed_messages', true);
  $viewed_messages_ids = $viewed_messages ? $viewed_messages : array();

  $completed_challenges = get_user_meta($user->ID, 'completed_challenges', true);
  if(!empty($completed_challenges)){
    $completed_challenges = unserialize($completed_challenges);
    $completed_challenges = array_keys($completed_challenges);
  }else{
    $completed_challenges = array();
  }

  $accept_challenges = get_user_meta($user->ID, 'accepted_challenges', true);
  $accept_challenges = $accept_challenges ? array_keys(unserialize($accept_challenges)) : array();

//  var_dump($viewed_challenges);die;
  $not_in = array();


  $home_user_camp_main_arr  = array();
  foreach ($user_camp_sch as $camp_id => $val) {
    $parentid = get_term_meta($camp_id,'parent', true);
    if($val)
      $home_user_camp_main_arr[$parentid][$camp_id] = $val;
  }

  $camp_messages = array();
  foreach ($home_user_camp_main_arr as $main_cat => $campaigns) {

    foreach ($campaigns as $campaign => $count_new_messages) {

      if($count_new_messages == 0) continue;
      $args = array(
        'post_type' => 'message',
//        'post__not_in' => $not_in,
        'posts_per_page' => -1,
        'tax_query' => array(
          array(
            'taxonomy' => 'campaign',
            'field'    => 'id',
            'terms'    => $campaign,
          ),
        ),
        'meta_query' => array(
          'key' => 'challenge',
          'value'    => 'yes',
          'compare' => '='
        )
      );

      $query = new WP_Query( $args );
      $not_in = array_unique(array_merge($completed_challenges,$viewed_messages_ids));

      if($query->have_posts()) {


        $zero_arr = $order_arr = array();
        foreach ($query->posts as $post) {
          if ($post->menu_order == 0) {
            array_unshift($zero_arr, $post);
          } else {
            $order_arr[] = $post;
          }
        }
        usort($order_arr, function($a, $b){
          return strcmp($a->menu_order, $b->menu_order);
        });
        $clean_array = array_merge($order_arr, $zero_arr);
        $clean_array = array_slice($clean_array, 0, $count_new_messages);


//        echo '<pre>';
//        print_r($not_in);
//        echo '</pre>';
        foreach ($clean_array as $post) {
          $main_category = get_term($main_cat, 'main_category');
          if(!$main_category) continue;
          $color = get_term_meta($main_cat, 'color', true);
          $color = $color ? '#' . $color  : '#000';
          $main_cat_link = get_term_link($main_cat);

          if(in_array($post->ID, $not_in)) continue;

          $image_id = get_post_meta( $post->ID, '_image_id', true );
          $image_src = wp_get_attachment_url( $image_id );
          $image_src = $image_src ? $image_src : get_template_directory_uri() . '/img/card1.png';



          $post->main_cat_name = $main_category->name;
          $post->main_cat_color = $color;
          $post->main_cat_link = $main_cat_link;
          $post->image_src = $image_src;
          $post->the_ID = $post->ID;

          $excerpt = '';
          if($post->post_except){
            $excerpt = strlen($post->post_except) > 150 ? substr($post->post_except, 0, 150) . '...' : $post->post_except;
          }else{
            $excerpt = strlen($post->post_content) > 150 ? substr($post->post_content, 0, 150) . '...' : $post->post_content;
          }
          $post->the_excerpt =  $excerpt;


          $post->the_permalink =  get_the_permalink($post->ID);


          $camp_messages[] =  $post;
        }
      }
    }
  }
  return $camp_messages;
}


function get_selfie_badge($user_id)
{
    $wp_upload_dir = wp_upload_dir();
    $destination_path = $wp_upload_dir['basedir'] . '/users_selfie_badges/' . $user_id;
    if (file_exists($destination_path)) {

        $images = glob("$destination_path/*.png");
        sort($images);

        // Image selection and display:
        //display first image
        if (count($images) > 0) { // make sure at least one image exists
            $img = $images[0]; // first image
            return $wp_upload_dir['baseurl'] . '/users_selfie_badges/' . $user_id . '/' . basename($img);
        }
        return false;
    }
    return false;
}

//  http://ycweb.woocoder.com/wp-json/api/get_quiz_results
add_action('rest_api_init', function () {
    register_rest_route('api', '/get_quiz_results', array(
        'methods' => 'GET',
        'callback' => 'al_api_get_quiz_results',
    ));
});
function al_api_get_quiz_results(WP_REST_Request $request)
{
    if (is_user_logged_in()) {
        $user = wp_get_current_user();

        $res = get_user_meta($user->ID, 'quiz', true);
        $res_unserial = unserialize($res);

        $data = array();
        $data['empty_res'] = empty($res);

        if (!empty($res)) {
            if (!isset($res_unserial['date'])) {
                $data['empty_res'] = true;
                $data['is_user_logged_in'] = true;
                __json($data);

            }
            $personality = $res_unserial['user_personality'];
            $quiz_personality = get_option('al_quiz_personality');

            $data['personality'] = $personality;
            $data['quiz_personality'] = $quiz_personality[strtolower($personality)];
            $res = array();

            $options_answers_custom_fields = get_option('al_answers_custom_fields');
            $data['lifestyle'] = $data['user_personality_readmore'] = '';
            if (!empty($options_answers_custom_fields['lifestyle'])) {
                $data['lifestyle'] = wp_unslash($options_answers_custom_fields['lifestyle']);
            }
            if (!empty($options_answers_custom_fields[$personality])) {
                $data['user_personality_readmore'] = wpautop(wp_unslash($options_answers_custom_fields[$personality]));
            }
            $data['user_image'] = user_avatar_get_avatar($user->ID, 150);


            $data['user_selfie_badge'] = get_selfie_badge($user->ID);


            foreach ($res_unserial as $k => $v) {
                if ($k == 'fashion' || $k == 'fitness' || $k == 'relationships' || $k == 'interior_design' || $k == 'hair_type' || $k == 'skin_tone') {
                    $read_more = '';

//          $term = get_term_by('slug', $k);
//          var_dump($term);
                    if (!empty($options_answers_custom_fields[$v])) {
                        $read_more = wp_unslash($options_answers_custom_fields[$v]);
                    }
                    $header = str_replace('_', ' ', $k);
                    $name = str_replace('_', ' ', $v);
                    $name = str_replace('fash', '', $name);
                    $name = str_replace('des', '', $name);
                    $name = str_replace('qual-', '', $name);
                    $name = str_replace('-', ' ', $name);

                    $color = 'rgb(47, 179, 186)';
                    $term = get_term_by('name', $header, 'main_category');
                    if ($term) {
                        $color_term = get_term_meta($term->term_id, 'color', true);
                        $color = $color_term ? '#' . $color_term : $color;
                    }


                    $res[] = array(
                        'header' => $header,
                        'name' => $name,
                        'class' => str_replace('-', '_', $k),
                        'k' => $k,
                        'image' => get_template_directory_uri() . '/img/img_results_new/' . str_replace('-', '_', $k) . '_' . str_replace('-', '_', $v) . '.png',
                        'read_more' => $read_more,
                        'color' => $color
                    );
                }
            }
            $iframe_url = 'https://www.facebook.com/plugins/share_button.php?href=' . get_bloginfo('url') . '/al_webhook.php?user_id=' . $user->ID;
            $selfie_badge_src = get_selfie_badge($user->ID);
            if ($selfie_badge_src) {
//        $res[] = array(
//          'header' => 'Selfie Badge',
//          'name' => 'results',
//          'class' => 'class',
//          'k' => 'Selfie Badge',
//          'image' => $selfie_badge_src,
//          'read_more' => "<img src='". $selfie_badge_src ."' alt='selfie_badge'>",
//          'color' => $color
//        );
                $data['iframe_url'] = $iframe_url;
            }

            $data['res'] = $res;

        } else {

        }


        $data['is_user_logged_in'] = true;
    } else {
        $data['is_user_logged_in'] = false;
    }

    __json($data);
}

//  http://ycweb.woocoder.com/wp-json/api/get_user_data
add_action('rest_api_init', function () {
    register_rest_route('api', '/get_user_data',
        array(
            'methods' => 'GET',
            'callback' => 'al_api_get_user_data',
        ));
});

function al_api_get_user_data(WP_REST_Request $request)
{
    if (is_user_logged_in()) {
        $user = wp_get_current_user();
        $user_info = get_userdata($user->ID);

        $data['user_avatar'] = user_avatar_get_avatar($user->ID, 150);
        $data['customize_profile_avatar'] = admin_url('admin-ajax.php') . '?action=user_avatar_add_photo&step=1&uid=' . $user->ID . '&TB_iframe=true&width=720&height=450';
        $data['user_first_name'] = $user_info->first_name;
        $data['user_last_name'] = $user_info->last_name;
        $data['user_email'] = $user->user_email;

        $data['href_update_avatar'] = admin_url('admin-ajax.php') . '?action=user_avatar_add_photo&step=1&uid=' . $user->ID . '&TB_iframe=true&width=720&height=450';


        $data['is_user_logged_in'] = true;
    } else {
        $data['is_user_logged_in'] = false;
    }

    __json($data);

}

//////////////////////////////DEBUG DEBUG DEBUG DEBUG DEBUG !!!!!!///////////////////////////////
//add_filter( 'rest_pre_dispatch', 'my_prefix_test_request_headers', 10, 3 );

function my_prefix_test_request_headers($result, $server, $request)
{
    $result = $request->get_headers();
    return $result;
}

////////////////////////////////END DEBUG, on PRODUCTION DELETE ////////////////////////////////////////////////////


/////////////////////////////////////////// functions
function get_permalink_by_cf($state_template)
{
    $args = array(
        'post_type' => 'page',
        'meta_query' => array(
            array(
                'key' => 'state_template',
                'value' => $state_template,
            )
        )
    );
    if ($post = get_posts($args)) {
        return get_the_permalink($post[0]->ID);
    };
}

add_action('init', 'al_menus');
function al_menus()
{

    register_nav_menus(
        array(
            'header-menu' => __('Top Menu')
        )
    );
}


/*
	Call to http://yoursite.com/


 to check the membership level of a user.
*/
//function my_init_pmpro_mini_api()
//{
//  if(function_exists('pmpro_getMembershipLevelForUser') &&
//    !empty($_REQUEST['verify']) &&
//    !empty($_REQUEST['secret']))
//  {
//    if($_REQUEST['secret'] != "SOMESECRETHERE")
//      wp_die("Invalid secret.");
//
//    $user = get_user_by("email", str_replace(" ", "+", ($_REQUEST['verify'])));
//    if(empty($user))
//      wp_die("User not found.");
//
//    $membership_level = pmpro_getMembershipLevelForUser();
//    if(empty($membership_level))
//      die("0");
//
//    //user and membership level found, output json encoded membership level info
//    echo json_encode($membership_level);
//
//    exit;
//  }
//}
//add_action('init', 'my_init_pmpro_mini_api');


//function my_init_stripe_mini_api()
//{
//
//  if ( isset( $_GET['test'] ) && $_GET['test'] === 'stripe' ) {
//
//    include dirname(__FILE__) . '/lib/Stripe/Stripe.php';
//
//    Stripe::setApiKey("sk_test_kr0tcvgpffIEiI6jKcIU5eSj");
//
//    $result = Stripe_Token::create(
//      array(
//        "card" => array(
//          "number" => '4242424242424242',
//          "exp_month" => 12,
//          "exp_year" => 2019,
//          "cvc" => 123
//        )
//      )
//    );
//
//    $token = $result['id'];
//    $morder = new MemberOrder();
//
//    $customer = Stripe_Customer::create(array(
//      'email' => 'customer@example.com',
//      'source'  => $token
//    ));
//
//    $charge = Stripe_Charge::create(array(
//      'customer' => $customer->id,
//      'amount'   => 5000,
//      'currency' => 'usd'
//    ));
//
//    echo '<pre>';
//    var_dump($result);
//
//    exit;
//  }
//}
//add_action('init', 'my_init_stripe_mini_api');


//  http://ycweb.woocoder.com/wp-json/api/get_rewards
add_action('rest_api_init', function () {
    register_rest_route('api', '/get_rewards',
        array(
            'methods' => 'GET',
            'callback' => 'al_api_get_rewards',
        ));

});
function al_api_get_rewards(WP_REST_Request $request)
{
    $data['status'] = false;
    if (is_user_logged_in()) {
        $user = wp_get_current_user();
        $data['user_entries'] = get_user_meta($user->ID, 'entries', true);
        $data['user_points'] = get_user_meta($user->ID, 'points', true);


        $posts_array = get_rewards();
        if (!$posts_array) __json($data);

        $saved_messages = get_user_meta($user->ID, 'saved_messages', true);
        $saved_messages = $saved_messages ? $saved_messages : array();

        $applied_rewards = get_user_meta($user->ID, 'applied_rewards', true);
        if ($applied_rewards) {
            $applied_rewards = unserialize($applied_rewards);
        }

        foreach ($posts_array as $post) {

            if ($post->post_excerpt) {
                $excerpt = $post->post_excerpt;
            } else {
                $excerpt = strlen($post->post_content) > 150 ? substr($post->post_content, 0, 150) . '...' : $post->post_content;
            }

            $post->post_excerpt = $excerpt;

            $post->image_src = get_attached_image_src($post->ID);
            $post->in_array_then_bg = in_array($post->ID, $saved_messages);
            $post->img_src = get_template_directory_uri() . '/img/saved-stencil.png';

            if ($applied_rewards) {
                if (array_key_exists($post->ID, $applied_rewards)) {
                    $post->applied_count = $applied_rewards[$post->ID];
                }
            }

            $data['posts'][] = $post;

        }
        $data['status'] = true;

    }
    __json($data);
}

function get_rewards($one_reward = false)
{
    $args = array(
        'post_type' => 'rewards',
        'post_status' => 'publish',
        'posts_per_page' => -1,
    );

    $data['posts'] = array();
    $posts_array = get_posts($args);
    $clean_arr = array();
    foreach ($posts_array as $reward) {
        $start = get_post_meta($reward->ID, 'start', true);
        $end = get_post_meta($reward->ID, 'end', true);
        $start_unix = $start ? strtotime($start) : false;
        $end_unix = $end ? strtotime($end) : false;
        if (!$start || !$end) continue;
        if ($start_unix <= time() && $end_unix >= time()) {
            $clean_arr[] = $reward;
        }
    }
    if (empty($clean_arr)) {
        return false;
    }

    if ($one_reward && !empty($clean_arr)) {
        return array_slice($clean_arr, 0, 1);
    }
    return $clean_arr;
}

function get_attached_image_src($post_id)
{
    if (!$post_id)
        return false;

    $image_id = get_post_meta($post_id, '_image_id', true);
    $image_src = wp_get_attachment_url($image_id);

    return $image_src;
}

// get all quiz questions
function al_get_quiz_questions()
{
    $quiz_questions = array(
        array(
            'quest_text' => 'I am a',
            'input_type' => 'radio',
            'input_name' => 'gender',
            'answers' => array(
                array(
                    'value' => 'man',
                    'field_bg' => al_img_path('/img/quiz_questions/small1/man.png')
                ),
                array(
                    'value' => 'woman',
                    'field_bg' => al_img_path('/img/quiz_questions/small1/woman.png')
                )
            )
        ),


        array(
            'quest_text' => 'Let�s discover your style. Click on all the looks you like, but be quick! You only have 20 seconds. Ready, set, go!',
            'input_type' => 'checkbox',
            'input_name' => 'fashion',
            'answers' => al_quiz_fashion_questions()
        ),


        array(
            'quest_text' => 'Choose the shape that best describes your body.',
            'input_type' => 'radio',
            'input_name' => 'body_type',
            'answers' => array(
                array(
                    'value' => 'rectangle',
                    'field_bg' => al_img_path('/img/quiz_questions/small3/rectangle.png')
                ),
                array(
                    'value' => 'round',
                    'field_bg' => al_img_path('/img/quiz_questions/small3/round.png')
                ),
                array(
                    'value' => 'hourglass',
                    'field_bg' => al_img_path('/img/quiz_questions/small3/hourglass.png')
                ),
                array(
                    'value' => 'inverted-triangle',
                    'field_bg' => al_img_path('/img/quiz_questions/small3/inverted-triangle.png')
                ),
                array(
                    'value' => 'pear',
                    'field_bg' => al_img_path('/img/quiz_questions/small3/pear.png')
                )
            ),
        ),


        array(
            'quest_text' => 'Choose the picture that best describes your current relationship status.',
            'input_type' => 'radio',
            'input_name' => 'relationships',
            'answers' => array(
                array(
                    'value' => 'single',
                    'field_bg' => al_img_path('/img/quiz_questions/small4/single.png')
                ),
                array(
                    'value' => 'new-relationship',
                    'field_bg' => al_img_path('/img/quiz_questions/small4/new-relationship.png')
                ),
                array(
                    'value' => 'long-term-relationship',
                    'field_bg' => al_img_path('/img/quiz_questions/small4/long-term-relationship.png')
                ),
                array(
                    'value' => 'loss-of-relationship',
                    'field_bg' => al_img_path('/img/quiz_questions/small4/loss-of-relationship.png')
                )
            ),
        ),


        array(
            'quest_text' => 'Choose the picture that best describes the type of health and fitness training you�d like to receive.',
            'input_type' => 'radio',
            'input_name' => 'fitness',
            'answers' => array(
                array(
                    'value' => 'weight-loss',
                    'field_bg' => al_img_path('/img/quiz_questions/small5/weight-loss.jpg')
                ),
                array(
                    'value' => 'lean-and-tone',
                    'field_bg' => al_img_path('/img/quiz_questions/small5/lean-and-tone.jpg')
                ),
                array(
                    'value' => 'athlete',
                    'field_bg' => al_img_path('/img/quiz_questions/small5/firm-and-fit.jpg')
                )
            ),
        ),


        array(
            'quest_text' => 'Help us discover your design style. Click on all the looks that you like. To get the best results, do this fast and don�t overthink.',
            'input_type' => 'checkbox',
            'input_name' => 'interior_design',
            'answers' => al_quiz_interior_design_questions()
        ),


        array(
            'quest_text' => 'Choose the color that is closest to your skin tone.',
            'input_type' => 'radio',
            'input_name' => 'skin_tone',
            'answers' => array(
                array(
                    'value' => 'fair',
                    'field_bg' => al_img_path('/img/quiz_questions/small7/light.jpg')
                ),
                array(
                    'value' => 'medium',
                    'field_bg' => al_img_path('/img/quiz_questions/small7/medium.jpg')
                ),
                array(
                    'value' => 'dark',
                    'field_bg' => al_img_path('/img/quiz_questions/small7/dark.jpg')
                ),
            )
        ),


        array(
            'quest_text' => 'Choose the wave pattern that is closest to your hair type.',
            'input_type' => 'radio',
            'input_name' => 'hair',
            'answers' => array(
                array(
                    'value' => 'straight',
                    'field_bg' => al_img_path('/img/quiz_questions/small8/straight.png')
                ),
                array(
                    'value' => 'wavy',
                    'field_bg' => al_img_path('/img/quiz_questions/small8/wavy.png')
                ),
                array(
                    'value' => 'defined-curly',
                    'field_bg' => al_img_path('/img/quiz_questions/small8/defined-curly.png')
                ),
                array(
                    'value' => 'spiral-curl',
                    'field_bg' => al_img_path('/img/quiz_questions/small8/spiral-curl.png')
                )
            )
        ),


        array(
            'quest_text' => 'Pluck out a piece of your hair. We\'re serious. Now rub it between your thumb and finger. How does it feel?',
            'input_type' => 'radio',
            'input_name' => 'hair_type',
            'answers' => array(
                array(
                    'value' => 'qual-fine',
                    'field_txt' => 'I don\'t feel anything.'
                ),
                array(
                    'value' => 'qual-medium',
                    'field_txt' => 'I feel the hair.'
                ),
                array(
                    'value' => 'qual-coarse',
                    'field_txt' => 'I feel the hair and it\'s strong and thick.'
                )
            )
        ),


        array(
            'type_quest' => 'personality',
            'quest_text' => 'Here are 32 pairs of personality descriptions connected by a five point scale. For each pair, you must choose where on the scale between them you think you are. For example, if the pair is "angry" versus "calm", you should slide the slider all the way to angry if you are always angry and half way if you are sometimes angry.',
            'answers' => array(
                array(
                    'left' => 'makes lists',
                    'right' => 'relies on memory'
                ),
                array(
                    'left' => 'sceptical',
                    'right' => 'wants to believe'
                ),
                array(
                    'left' => 'bored by time alone',
                    'right' => 'needs time alone'
                ),
                array(
                    'left' => 'accepts things as they are',
                    'right' => 'unsatisfied with the ways things are'
                ),
                array(
                    'left' => 'keeps a clean room',
                    'right' => 'just puts stuff where ever'
                ),
                array(
                    'left' => 'thinks "robotic" is an insult',
                    'right' => 'strives to have a mechanical mind'
                ),
                array(
                    'left' => 'energetic',
                    'right' => 'mellow'
                ),
                array(
                    'left' => 'prefer to take multiple choice test',
                    'right' => 'prefer essay answers'
                ),
                array(
                    'left' => 'chaotic',
                    'right' => 'organized'
                ),
                array(
                    'left' => 'easily hurt',
                    'right' => 'thick-skinned'
                ),
                array(
                    'left' => 'works best in groups',
                    'right' => 'works best alone'
                ),
                array(
                    'left' => 'focused on the present',
                    'right' => 'focused on the future'
                ),
                array(
                    'left' => 'plans far ahead',
                    'right' => 'plans at the last minute'
                ),
                array(
                    'left' => 'wants people\'s respect',
                    'right' => 'wants their love'
                ),
                array(
                    'left' => 'gets worn out by parties',
                    'right' => 'gets fired up by parties'
                ),
                array(
                    'left' => 'fits in',
                    'right' => 'stands out'
                ),
                array(
                    'left' => 'keeps options open',
                    'right' => 'commits'
                ),
                array(
                    'left' => 'wants to be good at fixing things',
                    'right' => 'wants to be good at fixing people'
                ),
                array(
                    'left' => 'talks more',
                    'right' => 'listens more'
                ),
                array(
                    'left' => 'when describing an event, will tell people what happened',
                    'right' => 'when describing an event, will tell people what it meant'
                ),
                array(
                    'left' => 'gets work done right away',
                    'right' => 'procrastinates'
                ),
                array(
                    'left' => 'follows the heart',
                    'right' => 'follows the head'
                ),
                array(
                    'left' => 'stays at home',
                    'right' => 'goes out on the town'
                ),
                array(
                    'left' => 'wants the big picture',
                    'right' => 'wants the details'
                ),
                array(
                    'left' => 'improvises',
                    'right' => 'prepares'
                ),
                array(
                    'left' => 'bases morality on justice',
                    'right' => 'bases morality on compassion'
                ),
                array(
                    'left' => 'finds it difficult to yell very loudly',
                    'right' => 'yelling to others when they are far away comes naturally'
                ),
                array(
                    'left' => 'Likes to think about ideas and solutions',
                    'right' => 'Likes to work things out by experiencing them'
                ),
                array(
                    'left' => 'works hard',
                    'right' => 'plays hard'
                ),
                array(
                    'left' => 'uncomfortable with emotions',
                    'right' => 'values emotions'
                ),
                array(
                    'left' => 'likes to perform in front of other people',
                    'right' => 'avoids public speaking'
                ),
                array(
                    'left' => 'likes to know "who?", "what?", "when?"',
                    'right' => 'likes to know "why?"'
                )
            )
        )
    );

    return $quiz_questions;
}

function al_quiz_fashion_questions()
{
    $res_arr = array();
    $i = 1;

    while ($i <= 8) {
        $res_arr[] = array(
            'value' => 'fash_bohemian',
            'field_bg' => al_img_path('/img/quiz_questions/small2/bohemian_' . $i . '.jpg')
        );
        $res_arr[] = array(
            'value' => 'fash_chic',
            'field_bg' => al_img_path('/img/quiz_questions/small2/chic_' . $i . '.jpg')
        );
        $res_arr[] = array(
            'value' => 'fash_classic',
            'field_bg' => al_img_path('/img/quiz_questions/small2/classic_' . $i . '.jpg')
        );
        $res_arr[] = array(
            'value' => 'fash_rocker',
            'field_bg' => al_img_path('/img/quiz_questions/small2/rocker_' . $i . '.jpg')
        );
        $res_arr[] = array(
            'value' => 'fash_tomboy',
            'field_bg' => al_img_path('/img/quiz_questions/small2/tomboy_' . $i . '.jpg')
        );
        $i++;
    }

    return $res_arr;
}

function al_quiz_interior_design_questions()
{
    $res_arr = array();
    $i = 1;

    while ($i <= 8) {
        $res_arr[] = array(
            'value' => 'des_bohemian',
            'field_bg' => al_img_path('/img/quiz_questions/small6/bohemian_' . $i . '.jpg')
        );
        $res_arr[] = array(
            'value' => 'des_modern',
            'field_bg' => al_img_path('/img/quiz_questions/small6/modern_' . $i . '.jpg')
        );
        $res_arr[] = array(
            'value' => 'des_rustic_farmhouse',
            'field_bg' => al_img_path('/img/quiz_questions/small6/rustic_' . $i . '.jpg')
        );
        $res_arr[] = array(
            'value' => 'des_industrial_urban',
            'field_bg' => al_img_path('/img/quiz_questions/small6/industrial_' . $i . '.jpg')
        );
        $i++;
    }

    return $res_arr;
}

function al_img_path($path)
{
    return get_template_directory_uri() . $path;
}


//  http://ycweb.woocoder.com/wp-json/api/get_quiz_questions
add_action('rest_api_init', function () {
    register_rest_route('api', '/get_quiz_questions',
        array(
            'methods' => 'GET',
            'callback' => 'al_api_get_quiz_questions',
        ));

});

function al_api_get_quiz_questions(WP_REST_Request $request)
{
    $data = al_get_quiz_questions();
    $count = count($data);

    $number = $request->get_param('number');

    if (!isset($number))
        __json($data);

    if (!intval($number))
        __json('error');

    if ($number < 1) {
        __json('error');
    }

    if ($number > $count) {
        __json('error');
    }

    __json($data[$number - 1]);
}

//
//if(isset($_GET['checking'])){
//  $res = get_post_meta(488, 'challenge', true);
//  $res = get_post(468);
//
//  var_dump($res);
//}

function update_user_gender()
{
    $user_id = 1;
    $new_value = 'Woman';

    if (get_user_meta($user_id, 'gender', true) != $new_value) {
        update_user_meta($user_id, 'gender', $new_value);
    }
}

add_action('youcoached_update_user_gender', 'update_user_gender');
do_action('youcoached_update_user_gender', 'update_user_gender');


// redirect to home and open login modal after reset password
function action_after_password_reset($wp_password_change_notification)
{
    wp_redirect(home_url());
    exit;
}

;
add_action('after_password_reset', 'action_after_password_reset', 10, 1);

require_once('API/account.php');
require_once('API/y-challenges.php');
require_once('API/plan-subscription.php');
require_once('API/messages.php');


// tos - terms of service
add_action('rest_api_init', function () {
    register_rest_route('api', '/get_accepted_tos', array(
        'methods' => 'GET',
        'callback' => 'al_api_get_accepted_tos',
    ));
});
function al_api_get_accepted_tos(WP_REST_Request $request)
{
    if (is_user_logged_in()) {
        $currentUser = wp_get_current_user();
        
        $meta = get_user_meta($currentUser->ID, 'accepted_tos', true);

        __json($meta ? true : false);
    } else {
        __json('not logged');
    }
}

add_action('rest_api_init', function () {
    register_rest_route('api', '/set_accepted_tos', array(
        'methods' => 'GET',
        'callback' => 'al_api_set_accepted_tos',
    ));
});
function al_api_set_accepted_tos(WP_REST_Request $request)
{
    if (is_user_logged_in()) {
        $currentUser = wp_get_current_user();

        $result = update_user_meta($currentUser->ID, 'accepted_tos', 1);

        if($result)
            __json('ok');
        else
            __json('error');
    } else {
        __json('not logged');
    }
}

add_action('rest_api_init', function () {
    register_rest_route('api', '/handle_contact_form_data', array(
        'methods' => 'GET',
        'callback' => 'al_api_handle_contact_form_data',
    ));
});
function al_api_handle_contact_form_data(WP_REST_Request $request)
{
    if (is_user_logged_in()) {
        $currentUser = wp_get_current_user();
        $name = $request->get_param('name');
        $email = $request->get_param('email');
        $text = $request->get_param('text');
        $adminEmail = get_option('admin_email');

        $result = wp_mail("narek.mal@gmail.com", "YouChallenged: Contact Form Submitted", 
            "<i>Name: </i><b>{$name}</b><br/> 
            <i>Email: </i><b>{$email}</b><br/><br/>
            <i>Message:</i><br/><b>{$text}</b>");

        if($result)
            __json('ok');
        else
            __json('error');
    } else {
        __json('not logged');
    }
}