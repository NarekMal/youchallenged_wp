jQuery(document).ready(function () {


    (function ($) {


        function user_avatar_refresh_image(img) {


            jQuery('#user-avatar-display-image').html(img);


        }


        function add_remove_avatar_link() {



            /*nothing to do here, cause we are going to use our own delete link later. This function still here because of the user avatar plugin still need this*/


        }


        function mostUsedFromArray(array) {


            if (array.length == 0)



                return null;


            var modeMap = {};


            var maxEl = array[0], maxCount = 1;


            for (var i = 0; i < array.length; i++) {


                var el = array[i];


                if (modeMap[el] == null)



                    modeMap[el] = 1;


                else



                    modeMap[el]++;


                if (modeMap[el] > maxCount) {


                    maxEl = el;


                    maxCount = modeMap[el];


                }


            }


            return maxEl;


        }


        $("body").on("click", "#prev", function () {


            var _prev = $(".pt-page.current").parent().prev().find('.pt-page');


            if (!_prev) {


                return false;


            }


            $(".pt-page.current").fadeOut(function () {


                $(this).removeClass("current");


                _prev.fadeIn(function () {


                    if ($('#next').data('last') === 1) {


                        $('#next').data('last', 0).text('NEXT')


                    }


                    $(this).addClass("current");


                });


            });


        });


        //window.navigationQuiz = function(){

        $("body").on("click", "#next", function () {
            var _next = $(".pt-page.current").parent().next().find('.pt-page');

            //console.log(urls.ajax_url);

            if (!_next.length) {

                // if this is a last page

                if ($(this).data('last') == 1) {

                    var peronalityTestRes = {};
                    var i;
                    for (i = 1; i <= 32; i++) {
                        peronalityTestRes['Q' + i] = 3;
                    }
                    $(".test-personality").find('input[type="radio"]:checked').each(function () {

                        peronalityTestRes[$(this).attr('name')] = $(this).val();

                    });

                    var Data = {

                        action: 'al_personality_quiz',

                        data: peronalityTestRes

                    };

                    $.ajax({

                        type: "POST",

                        url: urls.ajax_url,

                        data: Data,

                        complete: function (r) {

                            // todo add responces explain

                            if (r.responseText == 'error') {

                                console.log('Quiz last question error');

                                //window.location.href = urls.site_url + '/#/results';

                            } else if (r.responseText == 'done') {

                                window.location.href = pages['quiz-results'];

                            } else {

                                // critical error


                            }

                        }

                    });

                }


                return false;


            }


            var answerInput = $(".pt-page.current").find('input[type="radio"]:checked');

            // if there are radio buttons

            if (answerInput.length !== 0) {

                var quest = answerInput.attr('name'),

                    answer = answerInput.val();

            } else {

                var resultArr = [];

                answerInput = $(".pt-page.current").find('input[type="checkbox"]:checked');

                answerInput.each(function () {

                    resultArr.push($(this).val());

                });

                var quest = answerInput.attr('name'),

                    answer = mostUsedFromArray(resultArr);

            }

            // if nothing is selected

            if (answerInput.length == 0) {

                alert('select an answer');

                return false;

            } else {

                // if it is a man

                if (quest == 'gender' && answer == 'man') {

                    var data = {

                        action: 'al_change_user_to_man',

                        addToList: false

                    };

                    if (confirm('Were\'re coaching only women at the moment, but plan to add men.Do you want to signup for our beta program for men?')) {

                        data.addToList = true;

                    }

                    $.ajax({

                        type: "POST",

                        url: urls.ajax_url,

                        data: data,

                        complete: function (r) {

                            if (r.responseText == 'done') {

                                window.location.href = urls.site_url + '/';

                            } else if (r.responseText == 'error') {

                            }

                        }

                    });

                    // todo change url

                    return false;

                }


                var data = {

                    action: 'al_user_quiz',

                    quest: quest,

                    answer: answer

                };

                $.ajax({

                    type: "POST",

                    url: urls.ajax_url,

                    data: data,

                    complete: function (r) {

                        // todo add responces explain

                        if (r.responseText == 'error') {

                            alert('error');

                        } else {

                        }

                    }

                });


                if (_next.hasClass('test-personality')) {

                    $(this).text('FINISH').data('last', 1)


                }


            }


            $(".pt-page.current").fadeOut(function () {


                $(this).removeClass("current");


                _next.fadeIn(function () {


                    $(this).addClass("current");


                });


            });


        });

        //};


        window.radioToSlider = function () {
            jQuery(".question").each(function () {
                var radios = $(this).find(":radio").hide();
                var s = jQuery("<div></div>").slider({
                    min: parseInt(radios.first().val(), 10),
                    max: parseInt(radios.last().val(), 10),
                    slide: function (event, ui) {
                        radios.filter("[value=" + ui.value + "]").click();
                    },
                }).appendTo(this);
                s.slider('value', 3)
            });
        };

        window.addtoRead = function () {
            setTimeout(function () {
                if ($('.al-message-text').is(':visible')) {
                    var $this = $('.al-message-text'),
                        messageId = $this.data('message-id'),
                        type = $this.data('type');
                    if (messageId && (type == 'message' || type == 'challenge')) {
                        var data = {
                            action: 'al_save_viewed_history',
                            message_id: messageId,
                            type: type
                        };
                        $.ajax({
                            type: "POST",
                            url: urls.ajax_url,
                            data: data,
                            complete: function (r) {
                                if (r.responseText == 'done') {
                                } else if (r.responseText == 'error') {
                                } else {
                                }
                            }
                        })
                    }
                }
            }, 3000)
        };


        $("body").on("click", ".save-icon", function () {


            var $this = $(this),


                messageId = $this.data('message-id'),


                data = {


                    action: 'al_save_to_favourite',


                    message_id: messageId


                };


            $.ajax({


                type: "POST",


                url: urls.ajax_url,


                data: data,


                complete: function (r) {



                    // todo add responces explain


                    if (r.responseText == 'done') {


                        if ($this.css('background-color') == 'rgb(242, 186, 37)') {


                            $this.css('background-color', 'rgb(123, 123, 123)')


                        } else {


                            $this.css('background-color', 'rgb(242, 186, 37)')


                        }


                    } else if (r.responseText == 'error') {
                    } else {
                    }
                }
            })
        });


        $("body").on("click", ".accept-challenge", function () {
            var $this = $(this),
                messageId = $this.data('message-id'),
                data = {
                    action: 'al_accept_challenge',
                    message_id: messageId
                };


            $.ajax({
                type: "POST",
                url: urls.ajax_url,
                data: data,
                complete: function (r) {
                    // todo add responces explain
                    if (r.responseText == 'added') {
                        $this.fadeOut(function () {
                            window.location.href = urls.site_url + '/challenges';
                            $this.text("Undo")
                        }).fadeIn();
                    } else if (r.responseText == 'deleted') {
                        $this.fadeOut(function () {
                            $this.text("accept challenge")
                        }).fadeIn();
                    } else {
                        console.log('error, accepting challenge AJAX');
                    }
                }
            });
        });

        $("body").on("click", ".complete-challenge", function () {

            // if (!confirm('Are you sure? it can\'t be undo')){
            //
            //   return false;
            //
            // }

            var $this = $(this),

                messageId = $this.data('message-id'),

                data = {

                    action: 'al_complete_challenge',

                    message_id: messageId

                };

            if ($this.hasClass('completed')) {

                return false;

            }


            $.ajax({

                type: "POST",

                url: urls.ajax_url,

                data: data,

                complete: function (r) {

                    // todo add responses explain

                    if (r.responseText == 'added') {

                        $this.attr('disabled', true);

                        $this.fadeOut(function () {

                            $this.addClass('complete-challenge completed');

                            $this.find('span').text("completed");

                            increaseStatisticOnChallenges();

                        }).fadeIn();


                    } else if (r.responseText == 'deleted') {

                        // no delete (undo) now

                        $this.fadeOut(function () {

                            $this.find('span').text("Complete challenge")

                        }).fadeIn();

                    } else if (r.responseText == 'not accepted') {

                        alert('This challenge is not accepted');

                    } else if (r.responseText == 'already') {

                        alert('This challenge is already completed');

                    } else if (r.responseText == 'error') {

                        alert('This challenge is already completed');

                    }


                }


            });


        });


      function increaseStatisticOnChallenges() {


        var angController = jQuery('#ChallengesController').scope(),

          data = {

            action: 'al_get_user_points',

          };


        $.ajax({

          type: 'POST',

          url: urls.ajax_url,

          data: data,

          complete: function (r) {

            if (r.responseText == 'error') {


            } else {

              var resp = JSON.parse(r.responseText);


              angController.data.user_entries = resp.entries;

              angController.data.user_points = resp.points;
              angController.data.percentage = resp.percentage;

              angController.data.compl_chall_count++;

              angController.data.compl_tooday_chall++;

              angController.$apply()


            }

          }

        })


      }


        // add self goal
        $("body").on("click", "#add-self-goal", function () {

            $.modal.close();

            $('#modal-self-goal').modal();

        });


        $("body").on("click", "#add-pers-goal", function (e) {
            e.preventDefault();
            var $this = $(this),
                textarea = $this.siblings('textarea'),
                goal = textarea.val(),
                category = $this.data('category'),
                update = $this.attr('data-update');

            if (goal != '' && category != '') {

                data = {
                    action: 'al_add_self_goal',
                    goal: goal,
                    category: category,
                    update: update
                };

                $.ajax({
                    type: "POST",
                    url: urls.ajax_url,
                    data: data,
                    complete: function (r) {
                        var resp = JSON.parse(r.responseText);
                        if (!resp.status) {
                            console.log('error');
                            return false;
                        } else {
                            if (resp.status == 'no goal') {
                                alert('enter a goal please');
                                return false;
                            } else if (resp.status == 'no category') {
                                console.log('no category');
                                return false;
                            } else if (resp.status == 'goal already exist') {
                                alert('goal already exists for this category');
                                return false;
                            } else if (resp.status == 'done') {
                                $.modal.close();

                                textarea.val('');
                                $('.add-self-goal-container').hide();
                                // $('.hide-when-gaol-added').hide();
                                // $("table.all-self-goals").append('<tr><td><input class="status-goal-checkbox" type="checkbox" data-id=" ' + resp.ID + ' " ></td><td class="text-left">' + goal + '</td><td><button class="delete-this-goal" data-id=" ' + resp.ID + ' ">Delete</button></td></tr>')
                                $('.self-goal-container .goal-text').html(goal);
                                $('.self-goal-container .goal-complete').attr('ng-data-id', resp.ID);
                                $('.self-goal-container').fadeIn();

                            }
                        }
                        // todo add responces explain
                        // if (r.responseText == 'error') {
                        //   console.log('error');
                        // } else {
                        //   // all ok - add new row
                        //   if ($('#temporary-no-goals') != undefined) {
                        //     $('#temporary-no-goals').fadeOut();
                        //   }
                        //   $('.hide-when-gaol-added').hide();
                        //   $("table.all-self-goals").append('<tr><td><input class="status-goal-checkbox" type="checkbox" data-id=" ' + r.responseText + ' " ></td><td class="text-left">' + goal + '</td><td><button class="delete-this-goal" data-id=" ' + r.responseText + ' ">Delete</button></td></tr>')
                        // }
                    }
                });
            }
            return false;
        });
        // status self goal

        $("body").on("click", "#status-goal", function () {
            var $this = $(this),
                container = $this.parents('.self-goal-container'),
                id = $this.attr('ng-data-id');

            if (id !== '' && id !== undefined && id !== 0) {
                data = {
                    action: 'al_status_self_goal',
                    id: id
                };
                $.ajax({
                    type: "POST",
                    url: urls.ajax_url,
                    data: data,
                    complete: function (r) {
                        if (r.responseText == 'error') {
                            console.error('cant complete self goal');
                            return false;
                        } else if (r.responseText == 'completed') {
                            container.hide();
                            $('.goal-text').html('');
                            $('.add-self-goal-container').show()
                        }
                    }
                });
            }


        });


        // edit self goal

        $("body").on("click", ".js-edit-self-goal", function () {

            var id = $(this).attr('ng-data-id'),
                currentGoal = $('#js-self-goal').html();
            $.modal.close();
            $('.self-goal-new').html(currentGoal);
            $('#modal-self-goal').modal();
            $('#add-pers-goal').attr('data-update', 'update').attr('data-goal-id', id);
        });


        $("body").on("click", "#user-avatar-remove", function () {

            var uid = $('#userid').val();

            var nonce = $('#_nonce').val();


            jQuery.ajax({

                type: 'POST',

                url: urls.ajax_url,

                data: ({action: 'delete_avatar', uid: uid, nonce: nonce}),

                success: function (html) {

                    if (html) {

                        var image = $('.noimage').val();

                        $("#user-avatar-display-image")

                            .fadeOut(400, function () {

                                $("#user-avatar-display-image").find('img').attr('src', $(".noimage").val());

                            })

                            .fadeIn(400);

                        $('#user-avatar-remove').remove();

                    }


                }

            });

        });


        //$("body").on("click", "#unsubscribe-stripe", function(){


        //  if(confirm("Are you sure you want unsubscribe?")) {


        //    $.ajax({


        //      type: 'POST',


        //      url: urls.ajax_url,


        //      data: ({action: 'al_cancel_subscription'}),


        //      complete: function (r) {


        //        console.log(r);


        //        if(r.responseText == 'done') {


        //          setTimeout(function(){


        //            window.location = urls.site_url;


        //          }, 2000)


        //


        //        }else{


        //          console.log('cancel subscription error');


        //        }


        //      }


        //    });


        //  }else{


        //    return false;


        //  }


        //


        //});


        $("body").on("click", "#save-profile-front", function (e) {


            e.preventDefault();


            var $this = $(this),


                parentForm = $this.parents('form'),


                firstName = parentForm.find('input[name="first-name"]').val(),


                lastName = parentForm.find('input[name="last-name"]').val(),


                email = parentForm.find('input[name="email"]').val(),


                pass1 = parentForm.find('input[name="pass1"]').val(),


                pass2 = parentForm.find('input[name="pass2"]').val();


            var data = {

                action: 'save_profile_front',

                first_name: firstName,

                last_name: lastName,

                email: email,

                pass1: pass1,

                pass2: pass2

            };


            $.ajax({

                type: 'POST',

                url: urls.ajax_url,

                data: data,

                complete: function (r) {

                    //console.log(r.responseText);

                    if (r.responseText == 'done') {

                        window.location = urls.site_url

                    } else {

                        alert(r.responseText);

                        return false;

                    }

                }

            })

        });


        $("body").on("click", ".checkbox-click", function () {

            var $this = $(this),

                parent = $this.parents('.content-bx');

            if ($this.attr('type') == "radio") {

                parent.find('.checkboxBlock').removeClass('checked')

            }

            $this.parent().toggleClass('checked');

            // $this.parent().siblings('.checkbox').toggleClass('checked')

        });


        // registration from front-end


        $("body").on("click", "#login-sub-btn", function (e) {


            e.preventDefault();


            var $this = $(this),


                parentForm = $this.parents('form'),

                userName = parentForm.find('input[name="username"]').val(),

                email = parentForm.find('input[name="email"]').val(),

                pass1 = parentForm.find('input[name="pass1"]').val(),

                pass2 = parentForm.find('input[name="pass2"]').val(),

                gender = parentForm.find('input[name="gender"]:checked').val();

            var data = {

                action: 'al_registration_front',

                user_name: userName,

                email: email,

                pass1: pass1,

                pass2: pass2,

                gender: gender

            };

            $.ajax({

                type: 'POST',

                url: urls.ajax_url,

                data: data,

                complete: function (r) {

                    // console.log(r.responseText);

                    if (r.responseText == 'done') {

                        if (gender == 'man') {

                            console.log(gender);

                            var data = {
                                action: 'al_change_user_to_man',
                                addToList: false
                            };

                            if (confirm('Were\'re coaching only women at the moment, but plan to add men.Do you want to signup for our beta program for men?')) {
                                data.addToList = true;
                            }

                            $.ajax({
                                type: "POST",
                                url: urls.ajax_url,
                                data: data,
                                complete: function (r) {
                                    if (r.responseText == 'done') {
                                        window.location.href = urls.site_url + '/';
                                    } else if (r.responseText == 'error') {

                                    }
                                }
                            });
                            return false;
                        }

                        window.location = urls.site_url + '/membership-account/membership-levels/';

                    } else {

                        alert(r.responseText);

                        return false;

                    }

                }

            })

        });

        // registration from front-end end


        // apply reward

        $("body").on("click", ".rewards-button", function (e) {
            e.preventDefault();
            var angController = jQuery('#RewardsController').scope();
            if (angController.data.user_entries == 0) {
                alert('you have no entries');
                return false;
            }


            if (!confirm('Are you sure? process can\'t be undo')) {
                return false;
            }

            var allRewardBottons = $(".rewards-button"),
                $this = $(this),
                id = $this.data('id'),
                data = {
                    action: 'al_apply_reward',
                    id: id
                };

            // disabling all rewards button
            allRewardBottons.attr('disabled', true);

            $.ajax({
                type: 'POST',
                url: urls.ajax_url,
                data: data,
                complete: function (r) {
                    var resp = JSON.parse(r.responseText);

                    if (resp.error == 'no entries')
                        return false;

                    if (resp.error == 'not enough entries') {
                        alert('not enough entries');
                        return false;
                    }

                    if (resp.error == 'error')
                        return false;
                    if (resp.left_entries > 0) {
                        allRewardBottons.attr('disabled', false);
                    }

                    $this.parents('div.sub-box').find('span.entr-in-rew').text(resp.applied_to_this);
                    angController.data.user_entries = resp.left_entries;
                    angController.$apply()
                }
            })
        });

        // old front-end functions
        function modal2() {
            $.modal.close();
            $('#myModal2').modal({modal: false});
        }

        jQuery(function ($) {
            // Load dialog on click
            $("body").on("click", ".open-login-form", function () {
                $('#myModal2').modal({
                    overlayId: 'modal-fixed'
                });
                return false;
            });


            // Load dialog on click
            $("body").on("click", ".signup-link", function (e) {
                e.preventDefault();
                $.modal.close();
                $('#myModal2').modal();
            });
            $("body").on("click", ".signin-link", function (e) {
                e.preventDefault();
                $.modal.close();
                $('#myModal').modal();
            });
            // fogot pass modal
            $("body").on("click", ".forgot-pass", function (e) {
                e.preventDefault();
                $.modal.close();
                $('#myModal3').modal();
            });


            // open login modal if from reset-password page
            function ifFromResetPassPage() {
                var parentUrl = document.referrer;
                if (parentUrl === 'http://' + window.location.hostname + '/wp-login.php?action=rp') {
                    $('#myModal').modal();
                }
            }

            ifFromResetPassPage();

        });

        // old front-end functions end
        //$('.ui-slider-handle').draggable();
        $("body").on("click", "#al-load-more-mess", function () {
            var $this = $(this),
                // container = jQuery(this).parents('.al-container-js'),
                nextShowItems = $('.al-container-js').find('.al-hidden').slice(0, 3);
            // console.log(nextShowItems);

            $(nextShowItems).removeClass('al-hidden');
            if (nextShowItems.length < 3) {
                $this.hide();
            }
        });

        $("body").on("click", "#logout-ajax", function () {

            var data = {

                action: 'al_logout'

            };

            $.ajax({

                type: 'POST',

                url: urls.ajax_url,

                data: data,

                complete: function (r) {

                    if (r.responseText = 'done')

                        window.location.href = urls.site_url + '/';

                }

            })

        });


        $("body").on("click", "#contact-us-button", function () {
            return;
            $.modal.close();

            $('#modal-contact').modal({
                overlayId: 'modal-bottom'
            });

        });


        $("body").on("click", "#send-contact-email", function (e) {

            e.preventDefault();

            var $this = $(this),

                parent = $this.parents('form'),

                isLogged = parent.find('input[name="is-logged-in"]').val(),

                message = parent.find('textarea[name="message"]').val(),

                name = parent.find('input[name="user-name"]').val(),

                data = {
                    action: 'al_send_contact_email',
                    isLogged: isLogged,
                    message: message,
                    name: name
                };


            if (isLogged != 1) {

                var email = parent.find('input[name="user-email"]').val();

                var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

                if (email == '' || !re.test(email)) {

                    alert('Please enter a valid email address.');

                    return false;

                }

                data.userEmail = email;

                if (!name) {
                    alert('Please enter the name.');
                    return false;
                }
            }


            if (!message) {
                alert('Please enter a message.');
                return false;
            }


            $.ajax({
                type: 'POST',
                url: urls.ajax_url,
                data: data,
                complete: function (r) {
                    if (r.responseText == 'done') {
                        alert("Message is sent");
                        $.modal.close();
                    } else {
                        alert(r.responseText);
                        $.modal.close();
                    }
                }
            })
        });

        // Hide message after valid discount
        var sucessMsg = $('#pmpro_message');
        var discountCodeMessage = $('#discount_code_message');
        $(document).bind("ajaxComplete", function () {
            if ((sucessMsg.hasClass('pmpro_discount_code_msg') && sucessMsg.hasClass('pmpro_success')) || (discountCodeMessage.hasClass('pmpro_discount_code_msg') && discountCodeMessage.hasClass('pmpro_success'))) {
                $('#pmpro_pricing_fields > tbody > tr:nth-child(1) > td > p:nth-child(2)').hide();
            }
        });

    })(jQuery);

});
