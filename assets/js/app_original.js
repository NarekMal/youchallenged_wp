"use strict";

var ycweb = angular.module('ycweb', ['ui.router', 'ngSanitize']);

ycweb.controller('navigation', function ($scope, $location) {
  // Checks the links to see if the supplied path matches the
  // current path and returns true or false
  // This is used to show class on true through ng-class
  $scope.isActive = function (viewLocation) {
    return viewLocation === $location.path();
  };
});

ycweb.config(function($stateProvider, $httpProvider) {
  $httpProvider.interceptors.push([function() {
    return {
      'request': function(config) {
        config.headers = config.headers || {};
        //add nonce to avoid CSRF issues
        config.headers['X-WP-Nonce'] = nonce;
        return config;
      }
    };
  }]);

  $stateProvider.state('membership_account', {
    url: '/membership-account*any',
    controller: function ($rootScope){
      $rootScope.$evalAsync(function () {
        window.location.assign('/membership-account')
      });
    }
  });
  $stateProvider.state('wp_admin', {
    url: '/wp-admin*any',
    controller: function ($rootScope){
      $rootScope.$evalAsync(function () {
        window.location.assign('/wp-admin')
      });
    }
  });
  $stateProvider.state('wp_logout', {
    url: '/wp-login.php*any',
    controller: function ($rootScope){
      $rootScope.$evalAsync(function () {
        window.location.assign('/wp-login.php?action=logout')
      });
    }
  });

  $stateProvider.state('category', {
    url: '/category/*slug',
    templateUrl: themeDir + '/partials/category.html'
  });


  $stateProvider.state('home', {
    url: '/',
    templateUrl: themeDir + '/partials/home.html',
    controller: 'HomeController'
  });

  $stateProvider.state('slug', {
    url: '/*slug',
    templateProvider: function($http, $stateParams, $state) {
      return $http.get(endpoint + 'get_post_info', {params: {slug: $stateParams.slug}}).then(function(response) {
        if (response.data.status) {
          return '<div ng-include="\'' + themeDir + '/partials/' + response.data.template_name + '.html\'"></div>';
        } else {
          //$state.go('home');
        }
      });
    }
  });
});

ycweb.controller('CategoryController', function($scope, $http, $stateParams) {
  $scope.isLoading = true;
  document.title = 'Category'
  $http.get(endpoint + 'al_user_has_subscription').then(function(response) {
    $scope.al_user_has_subscription = false;
    // if al_user_has_subscription
    if (response.data.al_user_has_subscription) {
      $scope.al_user_has_subscription = true;
      $scope.isset_main_category = false;

      $http.get(endpoint + 'isset_main_category', {params: {slug: $stateParams.slug}}).then(function(response) {
        if (response.data.isset_main_category) {
          $scope.isset_main_category = true;

          // if dose_user_have_a_quiz
          $http.get(endpoint + 'does_user_have_a_quiz').then(function(response) {
            $scope.does_user_have_a_quiz = false;
            // if does_user_have_a_quiz
            if (response.data.does_user_have_a_quiz) {
              $scope.does_user_have_a_quiz = true;

              $scope.no_message_in_this_category = false;
              $scope.empty_campaigns_from_quiz = false;
              $scope.empty_camp_ids_for_today = false;
              $scope.yes = false;
              $http.get(endpoint + 'get_camp_ids', {params: {slug: $stateParams.slug}}).then(function(response) {
                //console.log(response.data.status);

                if (response.data.status == 'no_message_in_this_category') {
                  $scope.no_message_in_this_category = true;
                  $scope.isLoading = false;
                } else if (response.data.status == 'empty_campaigns_from_quiz' ) {
                  $scope.empty_campaigns_from_quiz = true;
                  $scope.isLoading = false;
                } else if (response.data.status == 'empty_camp_ids_for_today') {
                  $scope.empty_camp_ids_for_today = true;
                  $scope.isLoading = false;
                } else if (response.data.status == 'yes') {
                  $scope.yes = true;
                  $scope.data = response.data.data;
                  //console.log($scope.data);
                  $scope.isLoading = false;
                }

              });

              $scope.isLoading = false;
            } else {

              $scope.isLoading = false;
            }
          });

        } else {

          $scope.isLoading = false;
        }
      });


    } else {

      $scope.isLoading = false;
    }
  });
});








ycweb.controller('HomeController', function($scope, $http) {
  //var req = {
  //  method: 'GET',
  //  url: 'http://www.ycweb2.tk/wp-json/api/get_default_messages',
  //  headers: {
  //    Authorization: 'Basic aGFybWFuOjEyMw=='
  //  },
  //};
  //$http(req).then(function(response){
  //  console.log(response);
  //});

  $scope.isLoading = true;
  document.title = 'You Coached';
  $http.get(endpoint + 'is_user_logged_in').then(function(response) {
    $scope.is_user_logged_in = false;
    // if is_user_logged_in
    if (response.data.is_user_logged_in) {
      $scope.is_user_logged_in = true;

      $http.get(endpoint + 'al_user_has_subscription').then(function(response) {
        $scope.al_user_has_subscription = false;
        $scope.link_account_page = pages['account'];
        // if al_user_has_subscription
        if (response.data.al_user_has_subscription) {
          $scope.al_user_has_subscription = true;

          // if dose_user_have_a_quiz
          $http.get(endpoint + 'does_user_have_a_quiz').then(function(response) {
            $scope.does_user_have_a_quiz = false;
            $scope.link_quiz_start_page = pages['quiz-start'];
            // if does_user_have_a_quiz
            if (response.data.does_user_have_a_quiz) {
              $scope.does_user_have_a_quiz = true;

              // get camp_ids_for_today
              $http.get(endpoint + 'isset_camp_ids_for_today').then(function(response) {
                // if there are camps for today
                if (response.data.isset_camp_ids_for_today) {
                  $scope.isset_camp_ids_for_today = true;
                  // search new messages
                  $http.get(endpoint + 'get_users_new_messages').then(function(response) {
                    if(response.data.new_messages.length){
                      // if there are new messages
                      $scope.is_there_new_messages = true;

                      $http.get(endpoint + 'get_user_name').then(function(response) {
                        $scope.user_name = response.data.user_name;
                      });
                      $scope.new_messages = response.data.new_messages;
                      $scope.isLoading = false;
                    } else {
                      // if there are no new messages
                      $scope.is_there_new_messages = false;
                      $scope.isLoading = false;
                    }

                  });
                } else {
                  // if no camps for today
                  $scope.isset_camp_ids_for_today = false;
                  $scope.isLoading = false;
                }

              });

            } else {
              // !dose_user_have_a_quiz
              $http.get(endpoint + 'get_default_messages').then(function(response) {
                $scope.default_messages = response.data.default_messages;
              });
              $scope.isLoading = false;
            }

          });


        } else {
          $scope.isLoading = false;
        }

      });
    } else {
      $scope.isLoading = false;
    }

  });
});

ycweb.controller('PageController', function($rootScope, $http, $scope, $stateParams, $sce) {
  $http.get(endpoint + 'get_post_info', {params: {slug: $stateParams.slug}}).then(function(response) {
    if (response.data.status) {
      $http.get(endpoint + 'get_post_by_id', {params: {post_id: response.data.post_id}}).then(function(response) {
        if (response.data.status) {
          document.title = response.data.post.message_title;
          $scope.message_id = response.data.post.message_id;
          $scope.message_title = response.data.post.message_title;
          $scope.message_image = response.data.post.message_image;
          $scope.message_type = response.data.post.message_type;
          $scope.message_content = $sce.trustAsHtml(response.data.post.message_content);
          $scope.main_cat_name = response.data.post.main_cat_name;
          $scope.main_cat_color = response.data.post.main_cat_name_color;

          angular.element(document).ready(function () {
            window.addtoRead();
          });

        }
      });
    }
  });
});



ycweb.controller('SelfGoalsController', function($scope, $http) {
  $scope.isLoading = true;
  document.title = 'Self Goals';
  $http.get(endpoint + 'al_user_has_subscription').then(function(response) {
    $scope.al_user_has_subscription = false;
    // if al_user_has_subscription
    if (response.data.al_user_has_subscription) {
      $scope.al_user_has_subscription = true;

      $http.get(endpoint + 'get_self_goals').then(function(response) {
        $scope.goals = response.data.status
        if (response.data.status) {
          $scope.data = response.data.data;
          $scope.isLoading = false;
        } else {
          $scope.isLoading = false;
        }
      });
    } else {
      $scope.isLoading = false;
    }
  });
});


ycweb.controller('ChallengesController', function($scope,$http) {
  $scope.isLoading = true;
  document.title = 'Challenges';
  $scope.link_quiz_start = pages['quiz-start'];
  $http.get(endpoint + 'does_user_have_a_quiz').then(function(response) {
    $scope.does_user_have_a_quiz = false;
    if (response.data.does_user_have_a_quiz) {
      $scope.does_user_have_a_quiz = true;
      $http.get(endpoint + 'get_challenges').then(function(response) {
        $scope.data = response.data;

        $scope.isLoading = false;
      });
    }
    $scope.isLoading = false;
  });
});


ycweb.controller('SpecifChallengesController', function($scope,$http,$location) {
  var challenges = ['new-challenges', 'completed-today', 'completed'];
  var i = challenges.indexOf($location.search().category);
  var category = (i != -1 ? $location.search().category : 'new-challenges' );
  document.title = 'Challenges/' + category;


  $http.get(endpoint + 'get_specif_challenges', {params: {category: category}}).then(function(response) {
    //console.log(response.data);
    $scope.data = response.data;
    $scope.data.title = response.data.category.replace('-', ' ');
  });

//console.log(category);
});


ycweb.config(["$locationProvider", function($locationProvider) {
  $locationProvider.html5Mode(true);
}]);


ycweb.controller('SavedMessagesController', function($rootScope, $http, $scope, $stateParams, $sce) {
  document.title = 'Saved Messages';
  $scope.isLoading = true;

  $http.get(endpoint + 'al_user_has_subscription').then(function(response) {
    $scope.al_user_has_subscription = true;
    // if al_user_has_subscription
    if (response.data.al_user_has_subscription) {
      $scope.al_user_has_subscription = true;

      $http.get(endpoint + 'get_saved_messages').then(function(response) {

        $scope.status = response.data.status;
        if (response.data.status) {
          $scope.messages =  response.data.messages;
          //console.log(response.data.messages);
          $scope.isLoading = false;
        } else {
          $scope.isLoading = false;
        }

      });

      $scope.isLoading = true;
    } else {
      $scope.isLoading = false;
    }
  });

});

ycweb.controller('AccountController', function($rootScope, $http, $scope, $stateParams, $sce) {
  document.title = 'Account';
  $scope.isLoading = true;
  $http.get(endpoint + 'get_account_info').then(function(response) {
    $scope.status = response.data.status;
    if (response.data.status) {
      $scope.data = response.data;
      //console.log(response.data);
      $scope.text = (response.data.flag ? 'RETAKE QUIZ' : 'QUIZ START');
      $scope.edit_profile_url = pages['edit-profile'];


      $scope.isLoading = false;
    } else {
      $scope.isLoading = false;
    }
  });
});

ycweb.controller('QuizResultsController', function($rootScope, $http, $scope, $stateParams, $sce) {
  document.title = 'Quiz Results';
  $scope.isLoading = true;

  $http.get(endpoint + 'al_user_has_subscription').then(function(response) {
    $scope.al_user_has_subscription = false;
    // if al_user_has_subscription
    if (response.data.al_user_has_subscription) {
      $scope.al_user_has_subscription = true;


      $http.get(endpoint + 'get_quiz_results').then(function(response) {
        $scope.empty_res = response.data.empty_res;
        if (!response.data.empty_res) {
          $scope.lifestyle = response.data.lifestyle;

          $scope.show_read_more = function(read_more_class,title,content) {
            if ('' !== content) {
              jQuery.modal("<div class='read-more " + read_more_class + "'><div class='rm-title'>" + title + "</div><div class='rm-content'>"+content+"</div></div>", {});
            }
          }

          $scope.me_icon_src = themeDir + '/img/me-icon.png';
  //          $scope.post_img_src = themeDir + '/img/quiz_results/' + image + '.jpg';
          $scope.data = response.data;
          $scope.src_life_style_result = themeDir + '/img/img_results_new/lifestyle_coaching_results.png';
          //$scope.src_hair_and_makeup_chic_result = themeDir + '/img/quiz_results/hair_and_makeup_chic.jpg';

          $scope.src_sample_result = themeDir + '/img/sample.png';
          $scope.src_ferry_result = themeDir + '/img/ferry.png';
          $scope.isLoading = false;
        } else {

          $scope.isLoading = false;
        }
      });


      $scope.isLoading = false;
    } else {
      $scope.account_url = pages['account'];
      $scope.isLoading = false;
    }
  });

});
ycweb.controller('PageCustomController', function($rootScope,$http, $scope, $stateParams, $sce) {

  $scope.isLoading = true;

  $http.get(endpoint + 'get_post_info', {params: {slug: $stateParams.slug}}).then(function(response) {
    if (response.data.status) {
      document.title = response.data.post.post_title;
      $scope.post = response.data.post;
      $scope.raw_post = $sce.trustAsHtml(response.data.raw_post);
      $rootScope.bodyStyle = {background: '#fff'};
      $scope.isLoading = false;
    }
  });

});
ycweb.controller('RewardsController', function($rootScope,$http, $scope, $stateParams, $sce) {



  $http.get(endpoint + 'get_rewards').then(function(response) {
    if (response.data.status) {
      document.title = 'Rewards';
      console.log(response.data);
      $scope.posts = response.data.posts;

    }
  });

});



ycweb.controller('QuizController', function($rootScope, $http, $scope, $stateParams, $sce) {
  $scope.isLoading = true;
  document.title = 'Quiz';
  $http.get(endpoint + 'al_user_has_subscription').then(function(response) {
    $scope.al_user_has_subscription = false;
    // if al_user_has_subscription
    if (response.data.al_user_has_subscription) {
      $scope.al_user_has_subscription = true;

      $scope.theme_uri = themeDir;
      $scope.quiz_url = pages['quiz'];



      $scope.isLoading = false;
    } else {
      $scope.quiz_url = pages['quiz'];
      $scope.isLoading = false;
    }
  });

});

ycweb.controller('QuizStartController', function($rootScope, $http, $scope, $stateParams, $sce) {
  document.title = 'Quiz Start';
  $scope.isLoading = true;

  $http.get(endpoint + 'al_user_has_subscription').then(function(response) {
    $scope.al_user_has_subscription = false;
    // if al_user_has_subscription
    if (response.data.al_user_has_subscription) {
      $scope.al_user_has_subscription = true;

      $scope.theme_uri = themeDir;
      $scope.quiz_url = pages['quiz'];



      $scope.isLoading = false;
    } else {
      $scope.quiz_url = pages['quiz'];
      $scope.account_url = pages['account'];
      $scope.isLoading = false;
    }
  });

});

ycweb.controller('EditProfileController', function($rootScope, $http, $scope, $stateParams, $sce) {
  document.title = 'Edit Profile';
  $scope.isLoading = true;
  document.title = 'Edit profile'
  $http.get(endpoint + 'get_user_data').then(function(response) {
    //if (!response.data.is_user_logged_in) {
    //  $state.go('home');
    //}
    //console.log(response.data);
      $scope.theme_uri = themeDir;
      $scope.data = response.data;


    angular.element(document).ready(function () {

      window.user_avatar_refresh_image = function(img){
        jQuery('#user-avatar-display-image').html(img);
      }
      window.add_remove_avatar_link = function(){
        //jQuery('#user-avatar-display-image').html('');
        /*nothing to do here, cause we are going to use our own delete link later. This function still here because of the user avatar plugin still need this*/
      }
    });

    $scope.isLoading = false;
  });

});

ycweb.run(function($rootScope, $http, $location, $state) {
  $rootScope.$on('$stateChangeStart', function (angularEvent, next, current) {
    $http.get(endpoint + 'get_user_data').then(function(response) {
      if (!response.data.is_user_logged_in) {
        $state.go('home');
      }
    });
    $rootScope.bodyStyle = {background: '#333'};
  });
});