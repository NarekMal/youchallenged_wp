"use strict";

var ycweb = angular.module('ycweb', ['ui.router', 'ngSanitize'])
  .filter('html', function ($sce) {
    return function (input) {
      return $sce.trustAsHtml(input);
    }
  });
var showOriginal = false;
ycweb.controller('navigation', function ($scope, $location) {
  // Checks the links to see if the supplied path matches the
  // current path and returns true or false
  // This is used to show class on true through ng-class
  $scope.isActive = function (viewLocation) {
    return viewLocation === $location.path();
  };
});

// Accepts a timestamp in seconds and returns a string representing how many days have passed since that timestamp
ycweb.filter("daysPassed", function(){
  return function (timeStamp){
    var nowSeconds = Math.floor(Date.now()/1000); // Get seconds from milliseconds
    var secondsInDay = 24*60*60;
    var daysPassed = Math.floor(nowSeconds/secondsInDay) - Math.floor(timeStamp/secondsInDay);
    if(daysPassed == 0)
      return 'TODAY';
    if(daysPassed == 1)
      return 'YESTERDAY';
    return daysPassed+' DAYS AGO';
  };
});

ycweb.directive('backImg', function () {
  return function (scope, element, attrs) {
    var url = attrs.backImg;
    if(url && url != '' && url != 'false')
      element.css({
        'background-image': 'url(' + url + ')',
        'background-size': 'cover'
      });
  };
});

ycweb.run(function ($rootScope) {
  $rootScope.themeDir = themeDir;
});

ycweb.config(function ($stateProvider, $httpProvider) {

  $httpProvider.interceptors.push([function () {
    return {
      'request': function (config) {
        config.headers = config.headers || {};
        //add nonce to avoid CSRF issues
        config.headers['X-WP-Nonce'] = nonce;
        return config;
      }
    };
  }]);

  $stateProvider.state('membership_account', {
    url: '/membership-account*any',
    controller: function ($rootScope) {
      $rootScope.$evalAsync(function () {
        window.location.assign('/membership-account')
      });
    }
  });
  $stateProvider.state('wp_admin', {
    url: '/wp-admin*any',
    controller: function ($rootScope) {
      $rootScope.$evalAsync(function () {
        window.location.assign('/wp-admin')
      });
    }
  });
  $stateProvider.state('wp_lost', {
    url: '/wp-login.php?action=lostpassword',
    controller: function ($rootScope) {
      $rootScope.$evalAsync(function () {
        window.location.assign('/wp-login.php?action=lostpassword')
      });
    }
  });
  $stateProvider.state('wp_logout', {
    url: '/wp-login.php*any',
    controller: function ($rootScope) {
      $rootScope.$evalAsync(function () {
        window.location.assign('/wp-login.php?action=logout')
      });
    }
  });

  $stateProvider.state('category', {
    url: '/category/*slug',
    templateUrl: themeDir + '/partials/category.html'
  });


  $stateProvider.state('home', {
    url: '/',
    templateUrl: themeDir + '/partials/home.html',
    controller: 'HomeController'
  });

  $stateProvider.state('yChallenge', {
    params: {id: null},
    url: '/ychallenge',
    templateUrl: themeDir + '/partials/yChallenge.html',
    controller: 'yChallengeController'
  });

  $stateProvider.state('yChallengeAccepted', {
    params: {id: null, message: null},
    url: '/ychallengeaccepted',
    templateUrl: themeDir + '/partials/yChallengeAccepted.html',
    controller: 'yChallengeAcceptedController'
  });

  $stateProvider.state('yChallengeLimitReached', {
    url: '/yChallengeLimitReached',
    templateUrl: themeDir + '/partials/yChallengeLimitReached.html'
  });

  $stateProvider.state('yChallenges', {
    url: '/ychallenges',
    templateUrl: themeDir + '/partials/ychallenges.html',
    controller: 'YChallengesController'
  });

  $stateProvider.state('profile', {
    url: '/profile',
    templateUrl: themeDir + '/partials/profile.html',
    controller: 'ProfileController'
  });

  $stateProvider.state('termsOfService', {
    url: '/terms-of-service',
    templateUrl: themeDir + '/partials/termsOfService.html',
    controller: 'TermsOfServiceController'
  });

  $stateProvider.state('contactUs', {
    url: '/contact-us',
    templateUrl: themeDir + '/partials/contactUs.html',
    controller: 'ContactUsController'
  });

  $stateProvider.state('messages', {
    url: '/messages',
    templateUrl: themeDir + '/partials/messages.html',
    controller: 'MessagesController'
  });

  $stateProvider.state('slug', {
    url: '/*slug',
    templateProvider: function ($http, $stateParams, $state) {
      return $http.get(endpoint + 'get_post_info', {params: {slug: $stateParams.slug}}).then(function (response) {
        if (response.data.status) {
          return '<div ng-include="\'' + themeDir + '/partials/' + response.data.template_name + '.html\'"></div>';
        } else {
          //$state.go('home');
        }
      });
    }
  });
});

ycweb.controller('CategoryController', function ($scope, $http, $stateParams) {
  $scope.isLoading = true;
  document.title = 'Category'
  $http.get(endpoint + 'al_user_has_subscription').then(function (response) {
    $scope.al_user_has_subscription = false;
    // if al_user_has_subscription
    if (response.data.al_user_has_subscription) {
      $scope.al_user_has_subscription = true;
      $scope.isset_main_category = false;

      $http.get(endpoint + 'isset_main_category', {params: {slug: $stateParams.slug}}).then(function (response) {
        if (response.data.isset_main_category) {
          $scope.isset_main_category = true;

          // if dose_user_have_a_quiz
          $http.get(endpoint + 'does_user_have_a_quiz').then(function (response) {
            $scope.does_user_have_a_quiz = false;
            if (response.data.does_user_have_a_quiz) {
              $scope.does_user_have_a_quiz = true;
            }

            $http.get(endpoint + 'get_self_goals?category=' + $stateParams.slug.toLowerCase()).then(function (response) {
              $scope.selfGoalsStatus = response.data.status;
              $scope.selfGoals = response.data.data;
            });

            $http.get(endpoint + 'get_camp_ids', {params: {slug: $stateParams.slug}}).then(function (response) {
              // alert('click');
              if (response.data.status == 'yes') {

                $scope.data = response.data.data;
                //console.log($scope.data);
                $scope.isLoading = false;
                $scope.newIcon = themeDir + '/img/new-icon.png';
              }

            });

            $scope.isLoading = false;

          });

        } else {

          $scope.isLoading = false;
        }
      });


    } else {

      $scope.isLoading = false;
    }
  });
});

ycweb.controller('YChallengesController', function ($scope, $http) {

  $scope.isLoading = true;
  $http.get(endpoint + 'is_user_logged_in').then(function (response) {
    $scope.is_user_logged_in = false;
    if (!response.data.is_user_logged_in) {
      var landingPage = document.querySelector('.landing-page').style.display = 'block';
    }
    // if is_user_logged_in
    if (response.data.is_user_logged_in) {
      $scope.is_user_logged_in = true;

      $http.get(endpoint + 'user_has_plan').then(function (response) {
        $scope.user_has_plan = false;
        $scope.link_account_page = pages['account'];
        // if al_user_has_plan
        if (response.data.user_has_plan) {
          $scope.user_has_plan = true;
          
          $http.get(endpoint + 'get_y_challenges').then(function (response) {
              console.log(response);
              $scope.isLoading = false;
              $scope.challenges = response.data.challenges;
              $scope.challengeCategoryColor = response.data.challenge_category_color;

          });
        } else {
          $scope.isLoading = false;
        }
      });
    } else {
      $scope.isLoading = false;
    }

  });
});

ycweb.controller('HomeController', function ($scope, $http, $state) {

  $scope.acceptClick = function (challenge) {
    $scope.isLoading = true;
    $http.get(endpoint + 'accept_y_challenge', {
      params: {
        challenge_id: challenge.id
      }
    }).then(function (response) {
      if(!responseIsOk(response.data.result)){
        if (response.data.result == 'limit_reached'){
          $state.go('yChallengeLimitReached');
          return;
        }
        $scope.isLoading = false;
        alert('error');
        return;
      }
      $state.go('yChallengeAccepted', {id: challenge.id, message: response.data.accepted_message});
    });
  };

  $scope.passClick = function (challenge) {
    $scope.isLoading = true;
    $http.get(endpoint + 'pass_y_challenge', {
      params: {
        challenge_id: challenge.id
      }
    }).then(function (response) {
      if(!responseIsOk(response.data)){
        if (response.data == 'limit_reached'){
          $state.go('yChallengeLimitReached');
          return;
        }
        $scope.isLoading = false;
        alert('error');
        return;
      }
      $http.get(endpoint + 'get_suggested_y_challenge').then(function (response) {
        $scope.challenge = response.data.challenge;
        $scope.isLoading = false;
      });
    });
  };

  $scope.isLoading = true;
  document.title = 'You Coached';
  $http.get(endpoint + 'is_user_logged_in').then(function (response) {
    $scope.is_user_logged_in = false;
    if (!response.data.is_user_logged_in) {
      var landingPage = document.querySelector('.landing-page').style.display = 'block';
    }
    // if is_user_logged_in
    if (response.data.is_user_logged_in) {
      $scope.is_user_logged_in = true;

      $http.get(endpoint + 'user_has_plan').then(function (response) {
        $scope.user_has_plan = false;
        $scope.link_account_page = pages['account'];
        // if user_has_plan
        if (response.data.user_has_plan) {
          $scope.user_has_plan = true;

          // if dose_user_have_a_quiz
          $http.get(endpoint + 'does_user_have_a_quiz').then(function (response) {
            $scope.does_user_have_a_quiz = false;
            $scope.link_quiz_start_page = pages['quiz-start'];
            // if does_user_have_a_quiz
            if (response.data.does_user_have_a_quiz) {
              $scope.does_user_have_a_quiz = true;
            }

            $scope.isset_camp_ids_for_today = true;
            // search new messages
            $http.get(endpoint + 'get_suggested_y_challenge').then(function (response) {
              
              $scope.challenge = response.data.challenge;
              $scope.isLoading = false;
              if(!$scope.challenge) $scope.noChallenge = true;

            });

          });


        } else {
          $scope.isLoading = false;
        }

      });
    } else {
      $scope.isLoading = false;
    }

  });
});

ycweb.controller('ProfileController', function ($scope, $rootScope, $http, $state) {
  var imgProfile;
  $scope.isLoading = true;
  $http.get(endpoint + 'get_account_info').then(function(response){
    if(response.data.status==true){
			response.data.user_image=response.data.user_image.replace(/\"/g,"");
			response.data.user_image=response.data.user_image.replace(/\'/g,"");
			imgProfile=response.data.user_image.match(/\?src=https(.*?)\&/);
			if(imgProfile==null){
				imgProfile=response.data.user_image.match(/src=https(.*?)\&/);
			}
			response.data.user_image="https"+imgProfile[1];
			$scope.acc=response.data;

      $scope.inputModels = {
        userName: $scope.acc.user_display_name,
        userEmail: $scope.acc.user_email
      };

      // Load category info if not already loaded
      if(!$rootScope.yChallengeCategory)
        $http.get(endpoint + 'get_y_challenge_category_info').then(function(response){
          $rootScope.yChallengeCategory = response.data.category_name;
          $rootScope.yChallengesInCategory = response.data.all_challenges;
          $rootScope.complYChallengesInCategory = response.data.completed_challenges;
          $rootScope.yChallengeBadgeUrl = response.data.category_badge_url;
          $scope.isLoading = false;
        });
      else
        $scope.isLoading = false;
		}
  });

  // User name and email updating
  $scope.processInputKeyPress = function(e){
    if(e.key == 'Escape'){
      $scope.inputModels.userName = $scope.acc.user_display_name;
      $scope.inputModels.userEmail = $scope.acc.user_email;
    }
    if(e.key == 'Enter')
      $scope.updateUserNameEmail();
  }

  $scope.updateUserNameEmail = function(){
    if ($scope.inputModels.userName == $scope.acc.user_display_name && $scope.inputModels.userEmail == $scope.acc.user_email)
      return;

    $scope.isLoading = true;
    $http.get(endpoint + 'set_user_name_email', {
      params: {
        name: $scope.inputModels.userName,
        email: $scope.inputModels.userEmail
      }
    }).then(function (response) {
      $scope.isLoading = false;
      if (responseIsOk(response.data)){
        $scope.acc.user_display_name = $scope.inputModels.userName;
        $scope.acc.user_email = $scope.inputModels.userEmail;
      }
      else
        alert("Failed to update");
    });
  }

  $scope.showPlanInfo = function(){
    $scope.planInfoIsShown = true;
  }

});

ycweb.controller('TermsOfServiceController', function ($scope, $http, $state) {

  var init = function(){
    $scope.isLoading = true;
    $http.get(endpoint + 'get_accepted_tos').then(function(resp){
      $scope.accepted = resp.data;
      $scope.isLoading = false;
    });
  };
  init();

  $scope.accept = function(){
    $scope.isLoading = true;
    $http.get(endpoint + 'set_accepted_tos').then(function(resp){
      if(responseIsOk(resp.data))
        init();
      else{
        $scope.isLoading = false;
        alert('Error occurred');
      }
    });
  }

  $scope.handleContinueClick = function(){
    if($scope.accepted)
      $state.go('account');
  }

});

ycweb.controller('ContactUsController', function ($scope, $http, $state) {

  $scope.inputData = {};

  $scope.handleSendClick = function(){
    $scope.isLoading = true;
    $http.get(endpoint + 'handle_contact_form_data', {
      params: {
        name: $scope.inputData.name,
        email: $scope.inputData.email,
        text: $scope.inputData.text
      }
    }).then(function (resp) {
      $scope.isLoading = false;
      if (responseIsOk(resp.data))
        alert('Message sent');
      else
        alert('Error occurred');
    });
  };

});

ycweb.controller('MessagesController', function ($scope, $window, $http, $state) {

  $scope.scrollToTop = function() {
    $window.scrollTo(0, 0);
  };

  $scope.flagComment = function(commentId){
    if(!confirm("Flag this comment as inappropriate?")) return;

    $scope.isLoading = true;
    $http.get(endpoint + 'flag_comment', {
        params: {
          comment_id: commentId
        }
      }).then(function (response) {
      $scope.isLoading = false;
      if(responseIsOk(response.data))
        alert('Thank you! This comment will be sent to a moderator.');
      else{
        alert('Failed to flag comment');
      }
    });
  }

  $scope.isLoading = true;
  $http.get(endpoint + 'get_user_avatar').then(function(resp){
    $scope.avatarHtml = resp.data;
    $http.get(endpoint + 'get_messages').then(function(resp){
      $scope.isLoading = false;
      $scope.challengeMessages = resp.data;
    });
  });

});

ycweb.controller('yChallengeController', function ($scope, $state, $stateParams, $window, $http) {

  $scope.acceptClick = function (challenge) {
    $scope.isLoading = true;
    $http.get(endpoint + 'accept_y_challenge', {
      params: {
        challenge_id: challenge.id
      }
    }).then(function (response) {
      if(!responseIsOk(response.data.result)){
        if (response.data.result == 'limit_reached'){
          $state.go('yChallengeLimitReached');
          return;
        }
        $scope.isLoading = false;
        alert('error');
        return;
      }
      $state.go('yChallengeAccepted', {id: challenge.id, message: response.data.accepted_message});
    });
  };

  $scope.passClick = function (challenge) {
    $scope.isLoading = true;
    $http.get(endpoint + 'pass_y_challenge', {
      params: {
        challenge_id: challenge.id
      }
    }).then(function (response) {
      if(!responseIsOk(response.data)){
        if (response.data == 'limit_reached'){
          $state.go('yChallengeLimitReached');
          return;
        }
        $scope.isLoading = false;
        alert('error');
        return;
      }
      $state.go('home');
    });
  };

  $scope.completeClick = function (challenge) {
    $scope.isLoading = true;
    $http.get(endpoint + 'complete_y_challenge', {
      params: {
        challenge_id: challenge.id
      }
    }).then(function (response) {
      $scope.isLoading = false;
      if(!responseIsOk(response.data)){
        alert('error');
        return;
      }
      challenge.status = 4;
      $scope.justCompleted = true;
    });
  };

  $scope.scrollToTop = function() {
    $window.scrollTo(0, 0);
  };

  $scope.goBack = function() {
    $window.history.back();
  };

  $scope.addComment = function(parentCommentId){
    $scope.modalData = {};
    $scope.modalContext = {parentCommentId: parentCommentId};
    jQuery("#comment-modal").modal();
  };

  $scope.handleModalCancel = function(){
    jQuery.modal.close();
  }

  $scope.handleModalAdd = function(){
    jQuery.modal.close();
    $scope.isLoading = true;
    $http.get(endpoint + 'add_comment', {
        params: {
          comment: $scope.modalData.comment,
          challenge_id: $scope.challenge.id,
          parent_id: $scope.modalContext.parentCommentId
        }
      }).then(function (response) {
      if(responseIsOk(response.data))
        $http.get(endpoint + 'get_y_challenge', {
            params: {
              challenge_id: $stateParams.id
            }
          }).then(function (response) {
          $scope.isLoading = false;
          $scope.challenge = response.data.challenge;
        });
      else{
        $scope.isLoading = false;
        alert('Failed to add comment');
      }
    });
  }

  $scope.flagComment = function(commentId){
    if(!confirm("Flag this comment as inappropriate?")) return;

    $scope.isLoading = true;
    $http.get(endpoint + 'flag_comment', {
        params: {
          comment_id: commentId
        }
      }).then(function (response) {
      $scope.isLoading = false;
      if(responseIsOk(response.data))
        alert('Thank you! This comment will be sent to a moderator.');
      else{
        alert('Failed to flag comment');
      }
    });
  }

  $scope.isLoading = true;
  $http.get(endpoint + 'get_y_challenge', {
      params: {
        challenge_id: $stateParams.id
      }
    }).then(function (response) {
    $scope.isLoading = false;
    $scope.challenge = response.data.challenge;
  });
  
});

ycweb.controller('yChallengeAcceptedController', function ($scope, $state, $stateParams) {
  $scope.message = $stateParams.message;

  $scope.goBackToChallenge = function () {
    console.log($stateParams);
    $state.go('yChallenge', {id: $stateParams.id});
  }
});

ycweb.controller('PageController', function ($rootScope, $http, $scope, $stateParams, $sce) {
  $http.get(endpoint + 'get_post_info', {params: {slug: $stateParams.slug}}).then(function (response) {
    if (response.data.status) {
      $http.get(endpoint + 'get_post_by_id', {params: {post_id: response.data.post_id}}).then(function (response) {
        if (response.data.status) {
          var $_GET = getGET();
          // console.log($_GET);
          document.title = response.data.post.message_title;
          $scope.message_id = response.data.post.message_id;
          $scope.iframeUrl = $sce.trustAsResourceUrl(response.data.post.iframe_url);
          $scope.message_title = response.data.post.message_title;
          $scope.message_image = response.data.post.message_image;
          $scope.message_type = response.data.post.message_type;
          $scope.message_content = $sce.trustAsHtml(response.data.post.message_content);
          $scope.main_cat_name = response.data.post.main_cat_name;
          $scope.main_cat_color = response.data.post.main_cat_name_color;
          $scope.main_cat_link = response.data.main_cat_link;
          $scope.main_cat_link = response.data.main_cat_link;
          $scope.in_array_then_bg = response.data.post.in_array_then_bg;
          $scope.img_src = response.data.post.img_src;
          if ($_GET && $_GET.cont === 'main' && response.data.post.message_content_main) {
            $scope.message_content = $sce.trustAsHtml(response.data.post.message_content_main);
          }
          $scope.challenge_is = response.data.post.challenge_is;


          homecontroller.element(document).ready(function () {
            window.addtoRead();
          });

        }
      });
    }
  });
});


ycweb.controller('SelfGoalsController', function ($scope, $http) {
  $scope.isLoading = true;
  document.title = 'Self Goals';
  $http.get(endpoint + 'al_user_has_subscription').then(function (response) {
    $scope.al_user_has_subscription = false;
    // if al_user_has_subscription
    if (response.data.al_user_has_subscription) {
      $scope.al_user_has_subscription = true;

      $http.get(endpoint + 'get_self_goals').then(function (response) {
        $scope.goals = response.data.status
        if (response.data.status) {
          $scope.data = response.data.data;
          $scope.isLoading = false;
        } else {
          $scope.isLoading = false;
        }
      });
    } else {
      $scope.isLoading = false;
    }
  });
});


ycweb.controller('ChallengesController', function ($scope, $http) {
  $scope.isLoading = true;
  document.title = 'Challenges';
  $scope.link_quiz_start = pages['quiz-start'];

  $scope.al_user_has_subscription = false;
  $scope.link_account_page = pages['account'];
  $http.get(endpoint + 'al_user_has_subscription').then(function (response) {
    if (response.data.al_user_has_subscription) {
      $scope.al_user_has_subscription = true;
    }
  });
  $http.get(endpoint + 'does_user_have_a_quiz').then(function (response) {
    $scope.does_user_have_a_quiz = false;
    if (response.data.does_user_have_a_quiz) {
      $scope.does_user_have_a_quiz = true;
      $http.get(endpoint + 'get_challenges').then(function (response) {
        // console.log(response.data);
        $scope.data = response.data;
        $scope.isLoading = false;

        // $scope.$on('$viewContentLoaded', function() {window.radioToSlider();});

      });
    }
    $scope.isLoading = false;
  });
});


ycweb.controller('SpecifChallengesController', function ($scope, $http, $location) {
  var challenges = ['new-challenges', 'completed-today', 'completed'];
  var i = challenges.indexOf($location.search().category);
  var category = (i != -1 ? $location.search().category : 'new-challenges' );
  document.title = 'Challenges/' + category;
  $scope.isLoading = true;

  $scope.al_user_has_subscription = false;
  $scope.link_account_page = pages['account'];
  $http.get(endpoint + 'al_user_has_subscription').then(function (response) {
    if (response.data.al_user_has_subscription) {
      $scope.al_user_has_subscription = true;
    }
  });

  $http.get(endpoint + 'get_specif_challenges', {params: {category: category}}).then(function (response) {
    //console.log(response.data);
    $scope.data = response.data;
    $scope.newIcon = themeDir + '/img/new-icon.png';

    if (response.data.category){
      $scope.data.title = response.data.category.replace('-', ' ');
    }
    $scope.isLoading = false;

  });
});


ycweb.config(["$locationProvider", function ($locationProvider) {
  $locationProvider.html5Mode(true);
}]);


ycweb.controller('SavedMessagesController', function ($rootScope, $http, $scope, $stateParams, $sce) {
  document.title = 'Saved Messages';
  $scope.isLoading = true

  $scope.link_account_page = pages['account'];

  $scope.al_user_has_subscription = false;

  $http.get(endpoint + 'al_user_has_subscription').then(function (response) {

    if (response.data.al_user_has_subscription) {
      if (response.data.al_user_has_subscription) {
        $scope.al_user_has_subscription = true;
      }

      $http.get(endpoint + 'get_saved_messages').then(function (response) {

        $scope.status = response.data.status;
        if (response.data.status) {
          response.data.messages.forEach(function (val, ind) {
            // console.log(val);
            response.data.messages[ind].the_permalink = val.post_type === 'rewards' ? '/rewards?message_id=' + val.the_ID : val.the_permalink;
          });
          $scope.messages = response.data.messages;
          //console.log(response.data.messages);
          $scope.isLoading = false;
        } else {
          $scope.isLoading = false;
        }

      });

      $scope.isLoading = true;
    } else {
      $scope.isLoading = false;
    }
  });

});

ycweb.controller('AccountController', function ($rootScope, $http, $scope, $stateParams, $sce) {

  $scope.changeChallengeCategory = function () {
    $scope.isCatChanging = true;
    $scope.catProgress = 0;
    $http.get(endpoint + 'change_y_challenge_category', {params: {category_id: $scope.selectedChallengeCategory}}).then(function (response) {
        $scope.isCatChanging = false;
        $scope.catProgress = response.data.progress;
        if ($scope.catProgress == 1)
            $scope.catBadgeURL = response.data.challenge_category_badge_url;
    });
  }

  // Load Stripe js and init stripeHandler (TODO: load js file better way)
  var stripeScript = document.createElement('script');
  stripeScript.src = "https://checkout.stripe.com/checkout.js";
  var stripeHandler;
  stripeScript.onload = function () {
    stripeHandler = StripeCheckout.configure({
      key: 'pk_test_VoJBNSKrDHOkTzMgcjbTeEie',
      locale: 'auto',
      token: $scope.sendPaymentToken
    });
  };
  document.head.appendChild(stripeScript); 

  $scope.showStripeModal = function () {
    console.log('click');
    stripeHandler.open({
      name: 'Monthly Subscription',
      description: 'Monthly Subscription',
      amount: 500
    });
  }

  $scope.sendPaymentToken = function (token) {
    $scope.isProcessing = true;
    $http.get(endpoint + 'add_stripe_subscription', {params: {token_id: token.id, email: token.email}}).then(function (response) {
      $scope.isProcessing = false;
      if(responseIsOk(response)){
        $scope.justSubscribed = true;
        $scope.data.has_active_plan = true;
        $scope.data.plan_subscription_method = 'Stripe';
      }
      else
        $scope.failedToSubscribe = true;
    });
  }

  document.title = 'Account';
  $scope.isLoading = true;
  $http.get(endpoint + 'get_account_info').then(function (response) {
    $scope.status = response.data.status;
    if (response.data.status) {
        $scope.data = response.data;
        //console.log(response.data);
        $scope.text = (response.data.flag ? 'RETAKE QUIZ' : 'START QUIZ');
        $scope.edit_profile_url = pages['edit-profile'];
        $scope.selectedChallengeCategory = response.data.selected_challenge_category;
        $scope.catProgress = response.data.selected_challenge_category_progress;
        if ($scope.catProgress == 1)
            $scope.catBadgeURL = response.data.challenge_category_badge_url;

        $scope.isLoading = false;
    } else {
        $scope.isLoading = false;
    }
  });
});

ycweb.controller('QuizResultsController', function ($rootScope, $http, $scope, $stateParams, $sce) {
  document.title = 'Quiz Results';
  $scope.isLoading = true;

  $http.get(endpoint + 'al_user_has_subscription').then(function (response) {
    $scope.al_user_has_subscription = false;
    // if al_user_has_subscription
    if (response.data.al_user_has_subscription) {
      $scope.al_user_has_subscription = true;
    }
    $http.get(endpoint + 'does_user_have_a_quiz').then(function (response) {

      $scope.does_user_have_a_quiz = false;
      if (response.data.does_user_have_a_quiz) {
        $scope.does_user_have_a_quiz = true;
      }
      $http.get(endpoint + 'get_quiz_results').then(function (response) {
        $scope.empty_res = response.data.empty_res;
        if (!response.data.empty_res) {
          $scope.lifestyle = response.data.lifestyle;
          $scope.user_personality_readmore = response.data.user_personality_readmore;

          $scope.show_read_more = function (read_more_class, title, content) {
            if ('' !== content) {
              jQuery.modal("<div class='read-more " + read_more_class + "'><div class='rm-title'>" + title + "</div><div class='rm-content'>" + content + "</div></div>", {});
            }
          };

          $scope.me_icon_src = themeDir + '/img/me-icon.png';
          //          $scope.post_img_src = themeDir + '/img/quiz_results/' + image + '.jpg';
          $scope.data = response.data;
          // console.log(response.data.iframe_url);return;
          // $scope.iframe_url = response.data.iframe_url ? $sce.trustAsResourceUrl(response.data.iframe_url): false;
          $scope.data.iframe_url = $sce.trustAsResourceUrl(response.data.iframe_url);
          // console.log($scope.data.iframe_url);
          // console.log($sce.trustAsResourceUrl(response.data.iframe_url));
          // jQuery.each($scope.data.res, function(ind, val){
          //   jQuery.each($scope.data.res, function(ind, val){
          //
          //   })
          //   console.log(val);
          // })
          // $sce.trustAsResourceUrl()
          $scope.src_life_style_result = themeDir + '/img/img_results_new/lifestyle_coaching_results.png';
          //$scope.src_hair_and_makeup_chic_result = themeDir + '/img/quiz_results/hair_and_makeup_chic.jpg';

          $scope.src_sample_result = themeDir + '/img/sample.png';
          $scope.src_ferry_result = themeDir + '/img/ferry.png';
          $scope.isLoading = false;
          // console.log($scope);
        } else {

          $scope.isLoading = false;
        }
      });
    });

    $scope.isLoading = false;

  });

});
ycweb.controller('PageCustomController', function ($rootScope, $http, $scope, $stateParams, $sce) {

  $scope.isLoading = true;

  $http.get(endpoint + 'get_post_info', {params: {slug: $stateParams.slug}}).then(function (response) {
    if (response.data.status) {
      document.title = response.data.post.post_title;
      $scope.post = response.data.post;
      $scope.raw_post = $sce.trustAsHtml(response.data.raw_post);
      $rootScope.bodyStyle = {background: '#fff'};
      $scope.isLoading = false;
    }
  });

});

ycweb.controller('RewardsController', function ($rootScope, $http, $scope, $stateParams, $sce) {
  $scope.al_user_has_subscription = false;
  $scope.link_account_page = pages['account'];
  $scope.isLoading = true;
  $http.get(endpoint + 'al_user_has_subscription').then(function (response) {
    if (response.data.al_user_has_subscription) {
      $scope.al_user_has_subscription = true;
    }
  });
  $http.get(endpoint + 'get_rewards').then(function (response) {
    if (response.data.status) {
      document.title = 'Rewards';
      $scope.data = response.data;
      $scope.show_read_more = function (title, content) {
        if ('' !== content) {
          jQuery.modal("<div class='read-more'><div class='rm-title black'>" + title + "</div><div class='rm-content'>" + content + "</div></div>", {});
        }
      };


      var $_GET = getGET();

      if ($_GET.message_id && response.data.posts) {
        response.data.posts.forEach(function (val, ind) {
          if (val.ID == $_GET.message_id) {
            $scope.show_read_more(val.post_title, val.post_content);
          }
        })
      }
    }
    $scope.isLoading = false;
  });
});

function getGET() {
  var parts = window.location.search.substr(1).split("&"), $_GET = {};
  for (var i = 0; i < parts.length; i++) {
    var temp = parts[i].split("=");
    $_GET[decodeURIComponent(temp[0])] = decodeURIComponent(temp[1]);
  }
  return $_GET;
}

ycweb.controller('QuizController', function ($rootScope, $http, $scope, $stateParams, $sce) {
  $scope.isLoading = true;
  document.title = 'Quiz';
  $http.get(endpoint + 'al_user_has_subscription').then(function (response) {
    $scope.al_user_has_subscription = false;
    // if al_user_has_subscription
    if (response.data.al_user_has_subscription) {
      $scope.al_user_has_subscription = true;
    }
    $http.get(endpoint + 'get_quiz_questions').then(function (response) {
      // console.log(response);
      $scope.theme_uri = themeDir;
      $scope.quiz_url = pages['quiz'];
      $scope.questions = response.data;

      $scope.isLoading = false;

      //homecontroller.element(document).ready(function () {
      //  alert('click');
      //    window.navigationQuiz();
      //});


    });
    // } else {
    //   $scope.quiz_url = pages['quiz'];
    //   $scope.isLoading = false;
    // }
  });

});
ycweb.directive('myRepeatDirective', function () {
  return function (scope, element, attrs) {
    if (scope.$last) {

      radioToSlider();
    }
  };
});
//ycweb.directive('myMainDirective', function() {
//    return function(scope, element, attrs) {
//      radioToSlider();
//    };
//  });


ycweb.controller('QuizStartController', function ($rootScope, $http, $scope, $stateParams, $sce) {
  document.title = 'Quiz Start';
  $scope.isLoading = true;

  $http.get(endpoint + 'al_user_has_subscription').then(function (response) {
    $scope.al_user_has_subscription = false;
    // if al_user_has_subscription
    if (response.data.al_user_has_subscription) {
      $scope.al_user_has_subscription = true;

      $scope.theme_uri = themeDir;
      $scope.quiz_url = pages['quiz'];


      $scope.isLoading = false;
    } else {
      $scope.quiz_url = pages['quiz'];
      $scope.account_url = pages['account'];
      $scope.isLoading = false;
    }
  });

});

ycweb.controller('EditProfileController', function ($rootScope, $http, $scope, $stateParams, $sce) {
  document.title = 'Edit Profile';
  $scope.isLoading = true;
  document.title = 'Edit profile'
  $http.get(endpoint + 'get_user_data').then(function (response) {
    //if (!response.data.is_user_logged_in) {
    //  $state.go('home');
    //}
    //console.log(response.data);
    $scope.theme_uri = themeDir;
    $scope.data = response.data;


    angular.element(document).ready(function () {

      window.user_avatar_refresh_image = function (img) {
        jQuery('#user-avatar-display-image').html(img);
      }
      window.add_remove_avatar_link = function () {
        //jQuery('#user-avatar-display-image').html('');
        /*nothing to do here, cause we are going to use our own delete link later. This function still here because of the user avatar plugin still need this*/
      }
    });

    $scope.isLoading = false;
  });

});

ycweb.run(function ($rootScope, $http, $location, $state) {
  $rootScope.$on('$stateChangeStart', function (angularEvent, next, current) {
    $http.get(endpoint + 'get_user_data').then(function (response) {
      if (!response.data.is_user_logged_in) {
        if ($location.$$path == '/youcoached-terms-of-service/' || $location.$$path == '/giveaway-sweepstake-rules/') {
          var landingPage = document.querySelector('.landing-page').style.display = 'none';
        } else {
          $state.go('home');
        }
      }
    });
    $rootScope.bodyStyle = {background: '#333'};
  });
});


jQuery(document).ready(function () {
  (function ($) {

    function clickToggle() {
      $("#clickMenu").on("click", function () {
        $("#header-menu").toggleClass("openMMenu");
      });

      $("#header-menu #menu li").on("click", function () {
        $("#header-menu").toggleClass("openMMenu");
      });
    }

    setTimeout(clickToggle, 1000);


  })(jQuery);
})

function responseIsOk(response)
{
  return response == 'ok';
}