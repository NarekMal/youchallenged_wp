<?php
/*
Template Name: Memb Levels
*/

if(is_user_logged_in()){
  get_header();
  ?>
  <div class="container bg-white mrgn-top">
<?php
  echo do_shortcode('[pmpro_levels]');
  ?>
  </div>
  <?php
  get_footer();
}else{
  wp_redirect(home_url());
}
