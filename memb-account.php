<?php
/*

Template Name: Memb Account

*/
if(is_user_logged_in()){
  get_header();
  ?>
  <div class="container content-box">
    <div class="col-sm-12 ">
      <div class="bg-white">
        <h1 class="memb-page-title-main"><?php the_title(); ?></h1>
        <?php
        echo do_shortcode('[pmpro_account]');
        ?>
        <div class="text-center marg-bott-1">
          <a href="<?php echo get_bloginfo('url'); ?>" class="go-home-butt"> go to home page</a>
        </div>
      </div>
    </div>
  </div>
  <?php
  get_footer();
}else{
  wp_redirect(home_url());
}