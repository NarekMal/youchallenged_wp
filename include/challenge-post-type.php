<?php

// Register post type and taxonomy
add_action('init', 'al_register_challenge_post_type_taxonomy');
function al_register_challenge_post_type_taxonomy()
{

    register_taxonomy( 'challenge_category', 'challenge', array(
        'labels' => array(
            'name' => 'Challenge Categories',
            'singular_name' => 'Challenge Category'
        ),
        // Hide default taxonomy editing meta box, custom editing
        // functionality is provided to make categories exclusive
        'show_ui' => true,
        'show_in_quick_edit' => false,
        'meta_box_cb' => false
    ));

    register_post_type( 'Challenge',
        array(
            'labels' => array(
                    'name' => __( 'Challenges' ),
                    'singular_name' => __( 'Challenge' )
                ),
            'menu_position' => 10,
            'public' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'challenge'),
            'menu_icon' => 'dashicons-yes',
            'supports' => array( 'title', 'editor', 'thumbnail', 'excerpt', 'comments' ),
            'taxonomies' => array('challenge_category')
    ));

}

// Add challenge accepted meta box
add_action('add_meta_boxes', 'al_meta_for_challenge_accepted_message');
function al_meta_for_challenge_accepted_message(){
    add_meta_box( 'challenge_accepted_message', __('Challenge Accepted Message'), 'al_emit_challenge_accepted_message_input', 'challenge', 'normal');
}

function al_emit_challenge_accepted_message_input( $post ) {
    wp_nonce_field( basename( __FILE__ ), 'ai_challenge_accepted_message_nonce' );
    $currentMessage = get_post_meta( $post->ID, 'accepted_message', true); 
    ?>
    <p>Add a message that is displayed when the challenge is accepted</p>
    <p>
        <textarea name="challenge_accepted_message" style="width: 100%"><?php echo $currentMessage; ?></textarea>
    </p>
    <?php
}

// Save challenge accepted message when saving the post
add_action('save_post', 'al_save_challenge_accepted_message');
function al_save_challenge_accepted_message($post_id){
    if ( !isset( $_POST['ai_challenge_accepted_message_nonce'] ) || !wp_verify_nonce( $_POST['ai_challenge_accepted_message_nonce'], basename( __FILE__ ) ) ){
        return;
    } 
    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ){
        return;
    }
    if ( !current_user_can( 'edit_post', $post_id ) ){
        return;
    }
    if ( isset( $_REQUEST['challenge_accepted_message'] ) ) {
        update_post_meta( $post_id, 'accepted_message', sanitize_text_field( $_POST['challenge_accepted_message'] ) );
    }
}

// Add challenge category selection controls to admin
// We disable default taxonomy selection meta box and use this custom implementation to make categories exclusive
add_action('add_meta_boxes', 'al_meta_for_challenge_category');
function al_meta_for_challenge_category(){
    add_meta_box( 'challenge_category', __('Challenge Category'), 'al_emit_challenge_category_input', 'challenge' ,'side');
}

function al_emit_challenge_category_input( $post ) {
    wp_nonce_field( basename( __FILE__ ), 'ai_challenge_category_nonce' );
    $terms = get_terms( array(
        'taxonomy' => 'challenge_category',
        'hide_empty' => false
    ));
    // We assume that there is a single category
    $currentCategory = get_the_terms($post->ID, 'challenge_category')[0];
    ?>
    <p>Choose challenge category</p>
    <p>
        <?php foreach($terms as $term): ?>
        <input type="radio" name="challenge_category" id="challenge_category_<?php echo $term->term_id;?>" 
            value="<?php echo $term->term_id;?>" <?php if($term->term_id==$currentCategory->term_id) echo "checked"; ?>>
            <label for="challenge_category_<?php echo $term->term_id;?>"><?php echo $term->name; ?></label>
        </input><br/>
        <?php endforeach; ?>
    </p>
    <?php
}

// Save challenge category when saving the post
add_action('save_post', 'al_save_challenge_category');
function al_save_challenge_category($post_id){
    if ( !isset( $_POST['ai_challenge_category_nonce'] ) || !wp_verify_nonce( $_POST['ai_challenge_category_nonce'], basename( __FILE__ ) ) ){
        return;
    } 
    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ){
        return;
    }
    if ( !current_user_can( 'edit_post', $post_id ) ){
        return;
    }
    if ( isset( $_REQUEST['challenge_category'] ) ) {
        wp_set_object_terms($post_id, (int)sanitize_text_field( $_POST['challenge_category'] ), 'challenge_category');
    }
}

// Add sequence number to admin
add_action('add_meta_boxes', 'al_meta_for_challenge_position');
function al_meta_for_challenge_position()
{
    add_meta_box( 'sequence_number', __('Sequence Number'), 'al_emit_sequence_number_input', 'challenge' ,'side');
}

function al_emit_sequence_number_input( $post ) {
    wp_nonce_field( basename( __FILE__ ), 'ai_challenge_position_nonce' );
    $current_pos = get_post_meta( $post->ID, 'sequence_number', true); ?>
    <p>Enter the position at which you would like the post to appear.</p>
    <p><input type="number" name="challenge_position" value="<?php echo $current_pos; ?>" /></p>
    <?php
}

// Show info about users who accepted challenge
add_action('add_meta_boxes', 'al_challenge_acceptors');
function al_challenge_acceptors(){
    add_meta_box( 'aa', __('Users That Accepted Challenge'), 'al_emit_challenge_acceptors', 'challenge' ,'side');
}

function al_emit_challenge_acceptors( $post ) {
    $challenge_id = $post->ID;
    global $wpdb;
    $users = $wpdb->get_results(
        "select user.ID, user.display_name
        from wpqr_challenge_status as status 
        join wpqr_users as user on status.user_id = user.ID
        where (status=1 or status=4) and challenge_id={$challenge_id}");
    if(empty($users))
        echo "<h2>No users</h2>";
    else{
        echo "<ul>";
        foreach($users as $user){
            $name = $user->display_name;
            $img = get_avatar($user->ID);
            echo 
                "<li class='al_admin-avatar-list-item'>
                    <div class='al_admin-avatar-container'>{$img}</div>
                    {$name}
                </li>";
        }
        echo "</ul>";
    }
}

// Save sequence number when saving the post
add_action('save_post', 'al_save_challenge_position', 1, 2);
function al_save_challenge_position($post_id, $post){
    // Save message sequence number
    if ( !isset( $_POST['ai_challenge_position_nonce'] ) || !wp_verify_nonce( $_POST['ai_challenge_position_nonce'], basename( __FILE__ ) ) ){
        return;
    } 
    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ){
        return;
    }
    if ( !current_user_can( 'edit_post', $post_id ) ){
        return;
    }
    if ( isset( $_REQUEST['challenge_position'] ) ) {
        al_shift_sequence_numbers_rec( sanitize_text_field( $_POST['challenge_position'] ) );
        update_post_meta( $post_id, 'sequence_number', sanitize_text_field( $_POST['challenge_position'] ) );
    }
}

// Shift sequence number of challenge having the same number and do for 'neighbor' challenges recursively
function al_shift_sequence_numbers_rec($current_seq_number){
    global $wpdb;
    $conflicting_post_id = $wpdb->get_var('select post_id from wpqr_postmeta
        where meta_key="sequence_number" and meta_value = '.$current_seq_number);
    if ($conflicting_post_id){
        al_shift_sequence_numbers_rec( $current_seq_number + 1 );
        update_post_meta( $conflicting_post_id, 'sequence_number', $current_seq_number + 1 );
    }
}

// Add sequence number column to post list
add_filter('manage_posts_columns' , 'al_add_custom_post_order_column');
function al_add_custom_post_order_column( $columns ){
  return array_merge ( $columns,
    array( 'challenge_position' => 'Sequence Number', ));
}

// Display sequence number in the post list
add_action( 'manage_posts_custom_column' , 'al_custom_post_order_value' , 10 , 2 );
function al_custom_post_order_value( $column, $post_id ){
  if ($column == 'challenge_position' ){
    echo '<p>' . get_post_meta( $post_id, 'sequence_number', true) . '</p>';
  }
}

// Add category completion badge image and category color fields to challenge category edit form
add_action ( 'challenge_category_edit_form_fields', 'al_extra_category_fields');
function al_extra_category_fields( $tag ) {   
    wp_enqueue_media();
    $my_saved_attachment_post_id = get_option( 'media_selector_attachment_id', 0 );
    $badge_url = get_option( "challenge_category_badge_url_{$tag->term_id}" );  
    ?>
    <tr class="form-field">
        <th scope="row" valign="top"><label for="cat_Image_url">Category Completion Badge Image</label></th>
        <td>
            <div class='image-preview-wrapper'>
                <img id='image-preview' src='<?php echo $badge_url; ?>' width='100' height='100' style='max-height: 100px; width: 100px;'>
            </div>
            <input id="upload_image_button" type="button" class="button" value="<?php _e( 'Upload image' ); ?>" />
            <input type='hidden' name='image_attachment_url' id='image_attachment_url' value='<?php echo $badge_url; ?>'>
        </td>
    </tr>
    <tr class="form-field">
        <th scope="row" valign="top"><label for="cat_Image_url">Category Color</label></th>
        <td id="category_color_container">
            <input type="text" name="category_color" value="<?php echo get_option( "challenge_category_color_{$tag->term_id}" ); ?>">
        </td>
    </tr>

    <script type='text/javascript'>
        jQuery( document ).ready( function( $ ) {
            // Init color picker control
            jQuery('#category_color_container input').wpColorPicker();

            // Uploading files
            var file_frame;
            var wp_media_post_id = wp.media.model.settings.post.id; // Store the old id
            var set_to_post_id = <?php echo $my_saved_attachment_post_id; ?>; // Set this
            jQuery('#upload_image_button').on('click', function( event ){
                event.preventDefault();
                // If the media frame already exists, reopen it.
                if ( file_frame ) {
                    // Set the post ID to what we want
                    file_frame.uploader.uploader.param( 'post_id', set_to_post_id );
                    // Open frame
                    file_frame.open();
                    return;
                } else {
                    // Set the wp.media post id so the uploader grabs the ID we want when initialised
                    wp.media.model.settings.post.id = set_to_post_id;
                }
                // Create the media frame.
                file_frame = wp.media.frames.file_frame = wp.media({
                    title: 'Select an image to upload',
                    button: {
                        text: 'Use this image',
                    },
                    multiple: false	// Set to true to allow multiple files to be selected
                });
                // When an image is selected, run a callback.
                file_frame.on( 'select', function() {
                    // We set multiple to false so only get one image from the uploader
                    attachment = file_frame.state().get('selection').first().toJSON();
                    // Do something with attachment.id and/or attachment.url here
                    $( '#image-preview' ).attr( 'src', attachment.url ).css( 'width', 'auto' );
                    $( '#image_attachment_url' ).val( attachment.url );
                    // Restore the main post ID
                    wp.media.model.settings.post.id = wp_media_post_id;
                });
                    // Finally, open the modal
                    file_frame.open();
            });
            // Restore the main ID when the add media button is pressed
            jQuery( 'a.add_media' ).on( 'click', function() {
                wp.media.model.settings.post.id = wp_media_post_id;
            });
        });
    </script>
    <?php

}

// Save changes made on the "challenge category" taxonomy
add_action( 'edited_challenge_category', 'ai_save_taxonomy_custom_fields', 10, 2 );  
function ai_save_taxonomy_custom_fields( $term_id ) { 
    if ( isset( $_POST['image_attachment_url'] ) ) {  
        update_option( "challenge_category_badge_url_{$term_id}", $_POST['image_attachment_url'] );  
    }  
    if ( isset( $_POST['category_color'] ) ) {  
        update_option( "challenge_category_color_{$term_id}", $_POST['category_color'] );  
    }  
}

// Enqueue color picker CSS and JS when in challenge category edit page
add_action('admin_enqueue_scripts', 'al_admin_enqueue_scripts_challenge_category');
function al_admin_enqueue_scripts_challenge_category() {
    if ('challenge_category' == get_current_screen()->taxonomy) {
        wp_enqueue_style('wp-color-picker');
        wp_enqueue_script('wp-color-picker');
    }
}


?>