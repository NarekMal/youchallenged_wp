<?php 

function al_get_comments_thread ($postId){
    $commentsArray = get_comments(array('post_id' => $postId, 'order' => 'ASC'));
    $commentsThread = array();
    foreach($commentsArray as $comment){
        if($comment->comment_parent == 0)
            al_add_comment_to_thread_rec($comment, 0, $commentsThread, $commentsArray);
    }
    return $commentsThread;
}

function al_add_comment_to_thread_rec($comment, $depth, &$commentsThread, $commentsArray){
    $augmentedComment = (array)$comment;
    $augmentedComment['comment_depth'] = $depth;
    array_push($commentsThread, $augmentedComment);
    foreach($commentsArray as $potentialChild){
        if($potentialChild->comment_parent == $comment->comment_ID)
            al_add_comment_to_thread_rec($potentialChild, $depth + 1, $commentsThread, $commentsArray);
    }
}

?>