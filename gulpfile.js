var gulp = require('gulp');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var gulpRename = require('gulp-rename');

gulp.task('styles', function () {
    gulp.src('assets/css/scss/main.scss')
        .pipe(sourcemaps.init())
        .pipe(sass({
            outputStyle: 'compact'
        }).on('error', sass.logError))
        .pipe(sourcemaps.write())
        .pipe(gulpRename('scss-output.css'))
        .pipe(gulp.dest('./assets/css/'));
});

gulp.task('watch', function () {
    gulp.watch('assets/css/scss/*.scss', ['styles']);
});

gulp.task('default', ['watch'], function () {
    console.log("runned default gulp task");
});