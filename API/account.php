<?php

add_action('rest_api_init', function () {
    register_rest_route('api', '/get_account_info', array(
        'methods' => 'GET',
        'callback' => 'al_api_get_account_info',
    ));
});
function al_api_get_account_info( WP_REST_Request $request ) {
  if ( is_user_logged_in() ) {
    $data = array();

    $user = wp_get_current_user();
    $last_login = get_user_meta($user->ID, 'last_login', true);
    $becoming_awesome = date("F j, Y", strtotime($user->user_registered));

    $user_info = get_userdata($user->ID);
    $first_last = $user_info->first_name . ' ' . $user_info->last_name;
    $user_name = $first_last !== ' ' ? $first_last : $user->user_nicename;


    // user meta completed challenges
    $compl_chall_all_data = get_user_meta($user->ID, 'completed_challenges', true);
    $compl_chall_count = 0;
    if($compl_chall_all_data){
      $compl_chall_all_data = unserialize($compl_chall_all_data);
      $compl_chall_count = count($compl_chall_all_data);
    }



    $quiz = get_user_meta($user->ID, 'quiz', true);
    if($quiz) {
      $quiz = unserialize($quiz);
      $new_challenges = get_users_new_challenges($user->ID);
      $new_challenges_count = $new_challenges ? count($new_challenges): 0;
    }
    $data['new_challenges_count'] = $new_challenges_count;
    $data['complete_challenges_count'] = $compl_chall_count;

    $data['status'] = true;
    $data['last_login'] = $last_login;
    $data['becoming_awesome'] = $becoming_awesome;
    $data['user_name'] = $user_name;
    $data['user_display_name'] = $user->display_name;
    $data['user_email'] = $user->user_email;
    $data['user_id'] = $user->ID;
    $data['user_image'] = user_avatar_get_avatar( $user->ID, 150);
//    $data['delete_user_avatar_nonce'] = wp_create_nonce('delete_user_avatar');
    $data['is_subscribed'] = al_user_has_subscription();
    $data['is_admin'] = current_user_can('manage_options');
    $data['subscribe_link'] = get_permalink_by_cf( 'payment' );
    $data['admin_ajax'] = admin_url( 'admin-ajax.php' );

    $data['img_review'] =  get_template_directory_uri() . '/img/review.png';
    $data['img_check'] =  get_template_directory_uri() . '/img/check.png';
    $data['img_subscribe'] =  get_template_directory_uri() . '/img/subscribe.png';
    $data['img_email'] =  get_template_directory_uri() . '/img/email.png';

    $data['user_personality'] = isset( $quiz['user_personality'] );
    $data['flag'] = isset( $quiz['user_personality'] );

    $data['specif_challenges'] = get_permalink_by_cf( 'specif-challenges' );
    $data['self_goals'] = get_permalink_by_cf( 'self-goals' );
    $data['quiz_start'] = get_permalink_by_cf( 'quiz-start' );
    $data['quiz_results'] = get_permalink_by_cf( 'quiz-results' );
    // wp-memberships level page
    $data['link_member_levels'] = pmpro_url("levels");
    $data['link_unsubscribe'] = pmpro_url("account");
    $data['trial'] = false;
    $data['billing_amount'] = '';
    $data['expiration_period'] = '';


    $subscription_memb = pmpro_getMembershipLevelForUser($user->ID);
//    var_dump($subscription_memb);
    if(is_null($subscription_memb)){
      $trial_time = get_option('al_trial_time');
      $var = array(
        'no_trial' => '',
        'week' => '+1 week',
        'month' => '+1 month',
      );
      if(time() > strtotime($var[$trial_time], strtotime($user->user_registered)) ){
        $data['trial'] = false;
      }else{
        $data['trial'] = true;
      }
    }else{
      $data['billing_amount'] = $subscription_memb->billing_amount;
      $data['expiration_period'] = $subscription_memb->expiration_period;
    }
    $data['link_logout'] = wp_logout_url( );

    // Get count of accepted or completed challenges
    global $wpdb;
    $acceptedCompletedChallengesCount = $wpdb -> get_var(
        "select count(*) from wpqr_challenge_status
        where user_id={$user->ID} and (status = 1 or status = 4)");
    $data['acc_comp_challenges_count'] = $acceptedCompletedChallengesCount;

    // Get category completion badge urls
    $completionBadgeUrls = Array();
    $challengeCategories = get_terms('challenge_category');
    foreach ($challengeCategories as $challengeCategory){
        $categoryStats = get_user_stats_on_challenge_category($user->ID, $challengeCategory->term_id);
        if ($categoryStats['progress'] == 1)
            array_push($completionBadgeUrls, get_option( "challenge_category_badge_url_{$challengeCategory->term_id}" ));
    }
    $data['completion_badge_urls'] = $completionBadgeUrls;

    // $categoryId = get_user_meta($user->ID, 'challenge_category', true);
    // $data['challenge_categories'] = get_terms('challenge_category');
    // $data['selected_challenge_category'] = $categoryId;
    // // Getting progress on category
    // $categoryStats = get_user_stats_on_challenge_category($user->ID, $categoryId);
    // $data['selected_challenge_category_progress'] = $categoryStats['progress'];
    // // If category is completed, send badge image url
    // if ($categoryStats['progress'] == 1)
    //     $data['challenge_category_badge_url'] = get_option( "challenge_category_badge_url_{$categoryId}" ); 

    // Get subscription plan info
    $data['has_active_plan'] = get_user_meta( $user->ID, 'has_active_plan', true );
    // $data['plan_subscription_method'] = get_user_meta( $user->ID, 'plan_subscription_method', true );

    
  } else {
    $data['status'] = false;
  }
  __json( $data );
}


add_action('rest_api_init', function () {
    register_rest_route('api', '/get_user_avatar', array(
        'methods' => 'GET',
        'callback' => 'al_api_get_user_avatar',
    ));
});
function al_api_get_user_avatar( WP_REST_Request $request ) {
    if (is_user_logged_in()) {
        $user = wp_get_current_user();

        __json(get_avatar( $user->ID));

    } else {
        __json('not logged');
    }
}


add_action('rest_api_init', function () {
    register_rest_route('api', '/set_user_name_email', array(
        'methods' => 'GET',
        'callback' => 'al_api_set_user_name_email',
    ));
});
function al_api_set_user_name_email( WP_REST_Request $request ) {
    if (is_user_logged_in()) {
        $user = wp_get_current_user();

        $email = $request->get_param('email');
        if (!empty($email)) {
            if (!is_email(esc_attr($email))) {
                __json('incorrect email');
            } elseif (email_exists(esc_attr($email)) && $user->ID !== email_exists(esc_attr($email))) {
                __json('email is used by another user');
            } else {
                //wp_update_user(array('ID' => $user->ID, 'user_email' => esc_attr($email)));
            }
        }

        $name = $request->get_param('name');
        if (!empty($name))
            wp_update_user(array('ID' => $user->ID, 'display_name' => esc_attr($name)));

        __json('ok');

    } else {
        __json('not logged');
    }
}


?>