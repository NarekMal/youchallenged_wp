<?php

add_action('rest_api_init', function () {
    register_rest_route('api', '/get_messages', array(
        'methods' => 'GET',
        'callback' => 'al_api_get_messages',
    ));
});
function al_api_get_messages( WP_REST_Request $request ) {
    if (is_user_logged_in()) {
        $user = wp_get_current_user();
        $userAvatarHtml = get_avatar($user->ID);
        $comments = get_comments(array('author_in' => Array($user->ID), 'parent' => null));
        $returnSet = Array();

        foreach ($comments as $comment){
            $postTitle = get_post($comment->comment_post_ID)->post_title;
            $replyCommentObjects = get_comments(array('parent' => $comment->comment_ID));
            $replyComments = Array();
            foreach($replyCommentObjects as $replyCommentObject)
                $replyComments[] = Array(
                'comment_ID' => $replyCommentObject->comment_ID,
                'comment_content' => $replyCommentObject->comment_content,
                'comment_author' => $replyCommentObject->comment_author,
                'author_avatar_html' => get_avatar($replyCommentObject->user_id),
                'comment_timestamp' => strtotime($replyCommentObject->comment_date),
            );
            $returnSet[$postTitle][] = Array(
                'comment_ID' => $comment->comment_ID,
                'comment_content' => $comment->comment_content,
                'comment_author' => $comment->comment_author,
                'author_avatar_html' => $userAvatarHtml,
                'comment_timestamp' => strtotime($comment->comment_date),
                'replies' => $replyComments
            );
        }

        __json($returnSet);

    } else {
        __json('not logged');
    }
}

?>