<?php 

add_action('rest_api_init', function () {
    register_rest_route('api', '/get_y_challenges', array(
        'methods' => 'GET',
        'callback' => 'al_api_get_y_challenges',
    ));
});
function al_api_get_y_challenges(WP_REST_Request $request)
{
    if (is_user_logged_in()) {
        $args = array(
            'post_type' => 'challenge',
            'meta_key' => 'sequence_number',
            'orderby' => 'meta_value_num',
            'order' => 'ASC',
            'posts_per_page' => -1,
        );

        global $wpdb;

        $query = new WP_Query($args);
        $returnSet = array();
        while ($query->have_posts()) {
            $query->the_post();
            global $wpdb;
            $user_id = wp_get_current_user()->ID;
            $challenge_id = get_the_id();
            // Our assumption is that challenge can have at most one category
            $categoryId = get_the_terms($challenge_id, 'challenge_category')[0]->term_id;
            $statusRow = $wpdb->get_row(
                "select status, UNIX_TIMESTAMP(last_updated) as last_updated from wpqr_challenge_status 
                where user_id={$user_id} and challenge_id={$challenge_id}", "ARRAY_A");

            $challenge = array(
                'title' => get_the_title(),
                'id' => $challenge_id,
                'status' => $statusRow['status'],
                'status_updated' => $statusRow['last_updated'],
                'thumbnail_url' => get_the_post_thumbnail_url()
            );
            // If challenge has category, add category color to accepted challenges and badge urls to completed challenges
            if($categoryId){
                if($challenge['status'] == 1)
                    $challenge['category_color'] = get_option( "challenge_category_color_{$categoryId}");
                if($challenge['status'] == 4)
                    $challenge['category_badge_url'] = get_option( "challenge_category_badge_url_{$categoryId}");
            }
            array_push ($returnSet, $challenge);
        }
        wp_reset_postdata();
        __json(array(
            'challenges' => $returnSet
        ));

    } else {
        __json('not logged');
    }
}

add_action('rest_api_init', function () {
    register_rest_route('api', '/get_suggested_y_challenge', array(
        'methods' => 'GET',
        'callback' => 'al_api_get_suggested_y_challenge',
    ));
});
function al_api_get_suggested_y_challenge(WP_REST_Request $request)
{
    if (is_user_logged_in()) {

        $userId = wp_get_current_user()->ID;
        global $wpdb;
        $challengeCategory = $wpdb->get_var("select meta_value from wpqr_usermeta where user_id={$userId} and meta_key='challenge_category'");
        $suggestedChallenge = $wpdb->get_row(
            "select posts.ID as id, posts.post_title as title, posts.post_excerpt as excerpt
            from wpqr_posts as posts 
            left join wpqr_challenge_status as statuses on statuses.challenge_id = posts.ID and statuses.user_id = {$userId}
            left join wpqr_postmeta as meta on posts.ID = meta.post_id and meta.meta_key = 'sequence_number'
            join wpqr_term_relationships as tr on posts.ID = tr.object_id
            join wpqr_term_taxonomy as tt on tt.term_taxonomy_id = tr.term_taxonomy_id
            where posts.post_type='challenge' and tt.taxonomy='challenge_category' "
            .($challengeCategory ? " and tt.term_id={$challengeCategory}" : "").
            " and statuses.id is null
            order by cast(meta.meta_value as unsigned) asc
            limit 1", 
            "ARRAY_A");
        // If no suggested challenge, return null
        if(!$suggestedChallenge)
            __json(array('challenge' => null));
        
        $suggestedChallengeId = $suggestedChallenge['id'];
        $acceptedUsers = $wpdb->get_results(
            "select user.ID as id, user.display_name as name
            from wpqr_challenge_status as status 
            join wpqr_users as user on status.user_id = user.ID
            where (status=1 or status=4) and challenge_id={$suggestedChallengeId}", 
            "ARRAY_A");
        // Add avatar html to accepted users
        for ($i=0; $i<count($acceptedUsers); $i++)
            $acceptedUsers[$i]['avatarHtml'] = get_avatar($acceptedUsers[$i]['id']);

        $suggestedChallenge['accepted_users'] = $acceptedUsers;
        $suggestedChallenge['thumbnail_url'] = get_the_post_thumbnail_url($suggestedChallengeId);

        __json(array('challenge' => $suggestedChallenge));

    } else {
        __json('not logged');
    }
}



add_action('rest_api_init', function () {
    register_rest_route('api', '/get_y_challenge', array(
        'methods' => 'GET',
        'callback' => 'al_api_get_y_challenge',
    ));
});
function al_api_get_y_challenge(WP_REST_Request $request)
{
    if (is_user_logged_in()) {
        $yChallenge = get_post( $request->get_param('challenge_id') );
        $userId = wp_get_current_user()->ID;
        global $wpdb;
        $status = $wpdb->get_var(
            "select status from wpqr_challenge_status where user_id={$userId} and challenge_id={$yChallenge->ID}");
        $acceptedUsers = $wpdb->get_results(
            "select user.ID as id, user.display_name as name
            from wpqr_challenge_status as status 
            join wpqr_users as user on status.user_id = user.ID
            where (status=1 or status=4) and challenge_id={$yChallenge->ID}", "ARRAY_A");
        // Add avatar html to accepted users
        for ($i=0; $i<count($acceptedUsers); $i++)
            $acceptedUsers[$i]['avatarHtml'] = get_avatar($acceptedUsers[$i]['id']);
        require_once(get_template_directory().'/include/get-comments-thread.php');
        $comments = al_get_comments_thread($yChallenge->ID);
        // Add author avatar html to comments
        for ($i=0; $i<count($comments); $i++){
            $comments[$i]['author_avatar_html'] = get_avatar($comments[$i]['user_id']);
            $comments[$i]['comment_timestamp'] = strtotime($comments[$i]['comment_date']);
        }

        $returnVal = array(
            'id' => $yChallenge->ID,
            'title' => $yChallenge->post_title,
            'thumbnail_url' => get_the_post_thumbnail_url($yChallenge->ID),
            'content' => $yChallenge->post_content,
            'status' => $status,
            'accepted_users' => $acceptedUsers,
            'comments' => $comments
        );

        __json(array('challenge' => $returnVal));

    } else {
        __json('not logged');
    }
}

add_action('rest_api_init', function () {
    register_rest_route('api', '/add_comment', array(
        'methods' => 'GET',
        'callback' => 'al_api_add_comment',
    ));
});
function al_api_add_comment(WP_REST_Request $request)
{
    if (is_user_logged_in()) {
        $currentUser = wp_get_current_user();
        $result = wp_insert_comment(array(
            'comment_post_ID' => $request->get_param('challenge_id'),
            'comment_content' => $request->get_param('comment'),
            'comment_parent' => $request->get_param('parent_id'),
            'user_id' => $currentUser->ID,
            'comment_author' => $currentUser->display_name
        ));

        if($result)
            __json('ok');
        else
            __json('error');
    } else {
        __json('not logged');
    }
}

add_action('rest_api_init', function () {
    register_rest_route('api', '/flag_comment', array(
        'methods' => 'GET',
        'callback' => 'al_api_flag_comment',
    ));
});
function al_api_flag_comment(WP_REST_Request $request)
{
    if (is_user_logged_in()) {
        $currentUser = wp_get_current_user();
        $comment = get_comment($request->get_param('comment_id'));
        $post = get_post($comment->comment_post_ID);
        $adminEmail = get_option('admin_email');

        $result = wp_mail($adminEmail, "YouChallenged: User Flagged a Comment", 
            "<i>User {$currentUser->display_name} has flagged the following comment:</i><br/> 
            <b>{$comment->comment_content}</b><br/>
            <i>Challenge name: </i><b>{$post->post_title}</b>");

        if($result)
            __json('ok');
        else
            __json('error');
    } else {
        __json('not logged');
    }
}


add_action('rest_api_init', function () {
    register_rest_route('api', '/accept_y_challenge', array(
        'methods' => 'GET',
        'callback' => 'al_api_accept_y_challenge',
    ));
});
function al_api_accept_y_challenge(WP_REST_Request $request)
{
    if (is_user_logged_in()) {
        $userId = wp_get_current_user() -> ID;
        $challengeId = $request->get_param( 'challenge_id' );

        $challengeLimitNotReached = al_update_challenge_limit($userId);
        if (!$challengeLimitNotReached)
            __json(Array(
                'result' => 'limit_reached'
            ));
        
        global $wpdb;

        $updateQuery = "update wpqr_challenge_status SET status = 1 WHERE user_id={$userId} and challenge_id={$challengeId}";

        // If status updated, return success
        if($wpdb->query($updateQuery))
            __json('ok');
        
        // If status record didn't exist, insert new one
        $insertQuery = "insert into wpqr_challenge_status (user_id, challenge_id, status) values({$userId}, {$challengeId}, 1)";

        if($wpdb->query($insertQuery)){
            __json(Array(
                'result' => 'ok',
                'accepted_message' => get_post_meta( $challengeId, 'accepted_message', true)
            ));
        }

        __json(Array(
            'result' => 'error'
        ));

    } else {
        __json(Array(
            'result' => 'not logged'
        ));
    }
}

add_action('rest_api_init', function () {
    register_rest_route('api', '/pass_y_challenge', array(
        'methods' => 'GET',
        'callback' => 'al_api_pass_y_challenge',
    ));
});
function al_api_pass_y_challenge(WP_REST_Request $request)
{
    if (is_user_logged_in()) {
        $userId = wp_get_current_user() -> ID;
        $challengeId = $request->get_param( 'challenge_id' );

        $challengeLimitNotReached = al_update_challenge_limit($userId);
        if (!$challengeLimitNotReached)
            __json('limit_reached');
        
        global $wpdb;

        $updateQuery = "update wpqr_challenge_status SET status = 3 WHERE user_id={$userId} and challenge_id={$challengeId}";

        // If status updated, return success
        if($wpdb->query($updateQuery))
            __json('ok');
        
        // If status record didn't exist, insert new one
        $insertQuery = "insert into wpqr_challenge_status (user_id, challenge_id, status) values({$userId}, {$challengeId}, 3)";

        if($wpdb->query($insertQuery))
            __json('ok');

        __json('error');

    } else {
        __json('not logged');
    }
}

// Update daily challenge limit (decrease by one) if limit not reached, otherwise return false
function al_update_challenge_limit($userId){
    $limit = get_user_meta($userId, 'challenge_limit', true);
    $now = time();
    $setNewChallengeLimit = function($userId){
        update_user_meta($userId, 'challenge_limit', array(
            'last_updated' => time(),
            'limit' => 4
        ));
    };
    if(!empty($limit)){
        $lastUpdated = $limit['last_updated'];
        $secondsInDay = 24*60*60;
        if( floor($now/$secondsInDay) > floor($lastUpdated/$secondsInDay) ){
            // If the day has changed since last update, set new challenge limit
            $setNewChallengeLimit($userId);
            return true;
        }
        else{
            if($limit['limit'] == 0)
                // Limit is reached, return false
                return false;
            else{
                // Limit is not reached, decrease by one and store it
                $limit['limit'] = $limit['limit'] - 1;
                $limit['last_updated'] = time();
                update_user_meta($userId, 'challenge_limit', $limit);
                return true;
            }
        }
    }
    else{
        // Limit is not set, set it
        $setNewChallengeLimit($userId);
        return true;
    }
}

add_action('rest_api_init', function () {
    register_rest_route('api', '/complete_y_challenge', array(
        'methods' => 'GET',
        'callback' => 'al_api_complete_y_challenge',
    ));
});
function al_api_complete_y_challenge(WP_REST_Request $request)
{
    if (is_user_logged_in()) {


        $userId = wp_get_current_user() -> ID;
        $challengeId = $request->get_param( 'challenge_id' );

        global $wpdb;

        $updateQuery = "update wpqr_challenge_status SET status = 4 WHERE user_id={$userId} and challenge_id={$challengeId}";

        // If status updated, return success
        if($wpdb->query($updateQuery))
            __json('ok');
        
        // If status record didn't exist, insert new one
        $insertQuery = "insert into wpqr_challenge_status (user_id, challenge_id, status) values({$userId}, {$challengeId}, 4)";

        if($wpdb->query($insertQuery))
            __json('ok');

        __json('error');
        
    } else {
        __json('not logged');
    }
}


add_action('rest_api_init', function () {
    register_rest_route('api', '/change_y_challenge_category', array(
        'methods' => 'GET',
        'callback' => 'al_api_change_y_challenge_category',
    ));
});
function al_api_change_y_challenge_category(WP_REST_Request $request)
{
    if (is_user_logged_in()) {
        $user_id = wp_get_current_user()->ID;
        $category_id = $request->get_param( 'category_id' );
        
        global $wpdb;
        $checkQuery = "select * from wpqr_usermeta where user_id={$user_id} and meta_key='challenge_category'";

        if ($wpdb->get_var($checkQuery))
            $query = "update wpqr_usermeta set meta_value = $category_id where user_id=$user_id and meta_key='challenge_category'";
        else
            $query = "insert into wpqr_usermeta (user_id, meta_key, meta_value) values($user_id, 'challenge_category', $category_id)";
        
        $wpdb->query($query);

        $categoryStats = get_user_stats_on_challenge_category($user_id, $category_id);
        $response = Array('progress' => $categoryStats['progress']);
        if ($categoryStats['progress'] == 1)
            $response['challenge_category_badge_url'] = get_option( "challenge_category_badge_url_{$category_id}" );  

        __json($response);

    } else {
        __json('not logged');
    }
}


add_action('rest_api_init', function () {
    register_rest_route('api', '/get_y_challenge_category_info', array(
        'methods' => 'GET',
        'callback' => 'al_api_get_y_challenge_category_info',
    ));
});
function al_api_get_y_challenge_category_info(WP_REST_Request $request)
{
    if (is_user_logged_in()) {
        $userId = wp_get_current_user()->ID;
        $categoryId = get_user_meta($userId, 'challenge_category', true);

        $categoryStats = get_user_stats_on_challenge_category($userId, $categoryId);

        __json(Array(
            'category_id' => $categoryId,
            'category_name' => get_term_by('id', $categoryId, 'challenge_category')->name,
            'category_badge_url' => get_option( "challenge_category_badge_url_{$categoryId}" ),
            'all_challenges' => $categoryStats['all_challenges'],
            'completed_challenges' => $categoryStats['completed_challenges']
        ));

    } else {
        __json('not logged');
    }
}

?>