<?php 

add_action('rest_api_init', function () {
    register_rest_route('api', '/add_stripe_subscription', array(
        'methods' => 'GET',
        'callback' => 'al_api_add_stripe_subscription',
    ));
});
function al_api_add_stripe_subscription(WP_REST_Request $request)
{
    if (is_user_logged_in()) {
        $user_id = wp_get_current_user()->ID;
        
        require_once('include/stripe-php-5.1.1/init.php');

        \Stripe\Stripe::setApiKey("sk_test_IT8WSDYE4qk75VCx9DiDFxTi");

        try{
            $customer = \Stripe\Customer::create(array(
                'email' => $request->get_param( 'email' ),
                'source' => $request->get_param( 'token_id' ),
                'plan' => 'testplan'
            ));

            update_user_meta( $user_id, 'has_active_plan', '1' );
            update_user_meta( $user_id, 'plan_subscription_method', 'Stripe' );
            __json('ok');
        }
        catch(Exception $e){
            __json($e->getMessage());
        }

    } else {
        __json('not logged');
    }
}


add_action('rest_api_init', function () {
    register_rest_route('api', '/register_plan_subscription', array(
        'methods' => 'GET',
        'callback' => 'al_api_register_plan_subscription',
    ));
});
function al_api_register_plan_subscription(WP_REST_Request $request)
{
    if (is_user_logged_in()) {
        $user_id = wp_get_current_user()->ID;
        update_user_meta( $user_id, 'has_active_plan', '1' );
        update_user_meta( $user_id, 'plan_subscription_method', $request->get_param( 'subscription_method' ) );
        __json('ok');
    } else {
        __json('not logged');
    }
}


add_action('rest_api_init', function () {
    register_rest_route('api', '/user_has_plan', array(
        'methods' => 'GET',
        'callback' => 'al_api_user_has_plan',
    ));
});
function al_api_user_has_plan(WP_REST_Request $request)
{
    if (is_user_logged_in()) {
        // if admin
        if (current_user_can('manage_options'))
            $userHasPlan = true;
        else
            $userHasPlan = ((int)get_user_meta(wp_get_current_user()->ID, 'has_active_plan')) == 1;
    }
    else
        $userHasPlan = false;

    __json(array('user_has_plan' => $userHasPlan));
}

?>