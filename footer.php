<!--<footer>&copy; --><?//= date('Y') . ' - ' . get_bloginfo('name'); ?><!--</footer>-->
<?php //wp_footer(); ?>
<!--</body>-->
<!--</html>-->

<div class="row footer">
  <div class="container">
    <div class="col-sm-2 col-xs-3 footer-left-side">
      <?php if ( is_active_sidebar( 'footer_copies_sidebar' ) ) : ?>
        <?php dynamic_sidebar( 'footer_copies_sidebar' ); ?>
      <?php endif; ?>

      <!--      <p>YouCoached&copy;2016</p>-->
      <!--      <a href="#" class="links">Terms of use</a><br/>-->
      <!--      <a href="#" class="links">Provacy Policy</a>-->
    </div>

    <div class="col-sm-2 col-xs-3 social">
      <a href="https://www.instagram.com/youcoached/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/instagram.png"  alt="instagram" class="social-connects" /></a>
      <a href="https://www.twitter.com/youcoached" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/twitter.png" alt="twitter" class="social-connects" /></a>
      <a href="https://www.facebook.com/YouCoached/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/facebook.png" alt="facebook" class="social-connects" /></a>
    </div>
  </div>
</div>

<?php
?>
<div id="modal-contact" class="modal fade" role="dialog">
  <?php $is_logged = +is_user_logged_in(); ?>
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h2 class="welcome">Contact us</h2>
      </div>
      <div class="modal-body">
        <div class="login-form-box">
          <div class="login-form-box">
            <form action="">
              <input type="hidden" name="is-logged-in" value="<?php echo $is_logged; ?>">
              <?php if($is_logged === 0): ?>
                <input type="text" name="user-email" placeholder="enter your email">
                <input type="text" name="user-name" placeholder="enter your name">
              <?php endif; ?>
              <textarea type="text" name="message" rows="10" placeholder="enter your message"></textarea>
              <input class="login-sub-btn" id="send-contact-email" type="submit" value="Send">
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<?php wp_footer();

if(!is_user_logged_in()){
  ?>
  <script>
    jQuery(document).ready(function(){
      (function($){
        var ajax_url = '<?php echo admin_url('admin-ajax.php'); ?>';
        var adminUrl = '<?php echo admin_url(); ?> '
        jssor_1_slider_init = function() {

          var jssor_1_options = {
            $AutoPlay: true,
            $AutoPlaySteps: 3,
            $SlideDuration: 160,
            $SlideWidth: 320,
            $SlideSpacing: 20,
            $Cols: 3,
            $ArrowNavigatorOptions: {
              $Class: $JssorArrowNavigator$,
              $Steps: 4
            },
            $BulletNavigatorOptions: {
              $Class: $JssorBulletNavigator$,
              $SpacingX: 1,
              $SpacingY: 1
            }
          };

          var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

          //responsive code begin
          //you can remove responsive code if you don't want the slider scales while window resizing
          function ScaleSlider() {

            window.setTimeout(ScaleSlider, 30);

          }
          ScaleSlider();
          $Jssor$.$AddEvent(window, "load", ScaleSlider);
          $Jssor$.$AddEvent(window, "resize", ScaleSlider);
          $Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
          //responsive code end
        };
        setTimeout(function(){
          jssor_1_slider_init()
        }, 2000)
        ;
        $('#menu').slicknav({
          prependTo:'#landing-menu'
        });
        function m_close(){
          $('#menu').slicknav('close');
        }



        $( '#al-login-front-end' ).on( 'click', function(e) {
          e.preventDefault();
          var thisForm = $(this).parent(),
            name = thisForm.find('input[name="login_name"]').val(),
            pass = thisForm.find('input[name="login_password"]').val();

          var allData = {
            action: 'al_login_check',
            login_name: name,
            login_password: pass
          };
          $.ajax({
            type: "POST",
            url: ajax_url,
            data: allData,
            complete: function(r) {
//              console.log(r);
              if(r.responseText == 'error') {
                alert('incorrect user or password');
              }else if(r.responseText == 'exist'){
                location.reload();
              }else if(r.responseText == 'man'){
                alert('sorry, we are coaching only women.')
                jQuery.modal.close();
                return false;
              }
            }
          });
        });

      })(jQuery)
    })
  </script>
  <?php
}
?>
</div>
</body>
</html>
