<?php
/*
Template Name: Memb Billing
*/

if(is_user_logged_in()){
  get_header();
  ?>
  <div class="container bg-white mrgn-top memb-checkout-container">
    <?php
  echo do_shortcode('[pmpro_billing]');
  ?>
  </div>
  <?php
  get_footer();
}else{
  wp_redirect(home_url());
}

