<?php
class Calendar {

  /**
   * Constructor
   */
  public function __construct(){
    $this->naviHref = htmlentities($_SERVER['REQUEST_URI']);
  }

  /********************* PROPERTY ********************/
  private $dayLabels = array("Mon","Tue","Wed","Thu","Fri","Sat","Sun");

  private $currentYear=0;

  private $currentMonth=0;

  private $currentDay=0;

  private $currentDate=null;

  private $daysInMonth=0;

  private $naviHref= null;

  /********************* PUBLIC **********************/

  /**
   * print out the calendar
   */
  public function show() {
    $year  = null;

    $month = null;

    if(null==$year && isset($_GET['year'])){

      $year = $_GET['year'];

    }else if(null==$year){

      $year = date("Y",time());

    }

    if(null==$month&&isset($_GET['month'])){

      $month = $_GET['month'];

    }else if(null==$month){

      $month = date("m",time());

    }

    $this->currentYear=$year;

    $this->currentMonth=$month;

    $this->daysInMonth=$this->_daysInMonth($month,$year);

    $content='<div id="calendar">'.
      '<div class="box">'.
      $this->_createNavi().
      '</div>'.
      '<div class="box-content">'.
      '<ul class="label">'.$this->_createLabels().'</ul>';
    $content.='<div class="clear"></div>';
    $content.='<ul class="dates">';


    // it's old categories, used when plugin was creating. shows categories of single post
    $categories = get_categories(array(
      'type'         => 'messages',
      'child_of'     => 0,
      'parent'  => '',
      'hide_empty'   => 0,
      'childless'     => true,
    ));
    $categories =  get_terms( array(
      'taxonomy' => 'campaign',
      'hide_empty' => false,
    ) );

//    $args = array('taxonomy' => 'campaign', 'hide_empty'   => true);
//    $categories = get_terms($args);
//    $year =  date("Y",time());
//    $month = date("m",time());
;
    $start_val = strtotime($this->currentYear . '-' . $this->currentMonth);
    if($this->currentMonth < 12){
      $end_val = strtotime($this->currentYear . '-' . $this->currentMonth + 1 . '-01');
    }else{
      $end_val = strtotime($this->currentYear + 1 . '-01-01');
    }

    // get all categories for this month
    global $wpdb;
    $table = $wpdb->prefix . 'al_postscat_by_date';
    $sql = "SELECT * FROM $table WHERE date BETWEEN $start_val AND $end_val";
    $selected_categories = $wpdb->get_results($sql, ARRAY_A);
    $selected_categories_date = array();
    $selected_categories_name = array();
    foreach ($selected_categories as $selected_category) {
      $selected_categories_date[] = $selected_category['date'];
      $selected_categories_name[$selected_category['date']] = explode(',',$selected_category['category']);
    }

    $weeksInMonth = $this->_weeksInMonth($month,$year);
    // Create weeks in a month
    for( $i=0; $i<$weeksInMonth; $i++ ){
      //Create days in a week
      for($j=1;$j<=7;$j++){
        $day_number = $i*7+$j;
        $content.=$this->_showDay($day_number, $selected_categories_name,$selected_categories_date,$categories);
      }
    }

    $content.='</ul>';

    $content.='<div class="clear"></div>';

    $content.='</div><button class="button button-primary button-large save-all">Save</button>';

    $content.='</div>';
    return $content;
  }

  /********************* PRIVATE **********************/
  /**
   * create the li element for ul
   */
  private function _showDay($cellNumber, $selected_categories_name,$selected_categories_date,$categories){

    if($this->currentDay==0){

      $firstDayOfTheWeek = date('N',strtotime($this->currentYear.'-'.$this->currentMonth.'-01'));

      if(intval($cellNumber) == intval($firstDayOfTheWeek)){

        $this->currentDay=1;

      }
    }
    $p_class = '';

    if( ($this->currentDay!=0)&&($this->currentDay<=$this->daysInMonth) ){

      $this->currentDate = date('Y-m-d',strtotime($this->currentYear.'-'.$this->currentMonth.'-'.($this->currentDay)));

      if(($this->currentDay!=0)&&($this->currentDay<=$this->daysInMonth)){
        $date = strtotime(date("Y").'-'.date("m").'-'.date("d"));
        if(strtotime($this->currentDate) >= $date){
          $p_class = 'future';
        }

        $select = '<select class="select-in-li" data-placeholder="Select campaign name" multiple>';
        foreach ($categories as $category) {
          $selected = '';
          $this_time = strtotime($this->currentYear . '-' . $this->currentMonth . '-'  . $this->currentDay );


          if(in_array($this_time, $selected_categories_date) && in_array($category->term_id, $selected_categories_name[$this_time]) ){
            $selected = 'selected';
          }
          $select .= '<option  class="option" value="' . $category->term_id . '" ' . $selected . ' >'.$category->name . '</option>';
          $category->name;
        }
        $select .= '<select>';
      }
      $cellContent = '<p class="' . $p_class . '">' . $this->currentDay. '</p>';

      // add select-box only for today and future days
      if(strtotime($this->currentDate) >= $date){
        $cellContent .= $select;
      }

      $this->currentDay++;

    }else{

      $this->currentDate =null;

      $cellContent=null;
    }


    return '<li data-date="' . $this->currentDate . '" class="li-item ' . $p_class . ($cellNumber%7==1?' start ':($cellNumber%7==0?' end ':'')).
    ($cellContent==null?'mask':'').'">'.$cellContent.'</li>';
  }

  /**
   * create navigation
   */
  private function _createNavi(){

    $nextMonth = $this->currentMonth==12?1:intval($this->currentMonth)+1;

    $nextYear = $this->currentMonth==12?intval($this->currentYear)+1:$this->currentYear;

    $preMonth = $this->currentMonth==1?12:intval($this->currentMonth)-1;

    $preYear = $this->currentMonth==1?intval($this->currentYear)-1:$this->currentYear;

    return
      '<div class="header">'.
      '<a class="prev" href="'.$this->naviHref.'&month='.sprintf('%02d',$preMonth).'&year='.$preYear.'">Prev</a>'.
      '<span class="title">'.date('Y M',strtotime($this->currentYear.'-'.$this->currentMonth.'-1')).'</span>'.
      '<a class="next" href="'.$this->naviHref.'&month='.sprintf("%02d", $nextMonth).'&year='.$nextYear.'">Next</a>'.
      '</div>';
  }

  /**
   * create calendar week labels
   */
  private function _createLabels(){

    $content='';

    foreach($this->dayLabels as $index=>$label){

      $content.='<li class="'.($label==6?'end title':'start title').' title">'.$label.'</li>';

    }

    return $content;
  }



  /**
   * calculate number of weeks in a particular month
   */
  private function _weeksInMonth($month=null,$year=null){

    if( null==($year) ) {
      $year =  date("Y",time());
    }

    if(null==($month)) {
      $month = date("m",time());
    }

    // find number of days in this month
    $daysInMonths = $this->_daysInMonth($month,$year);

    $numOfweeks = ($daysInMonths%7==0?0:1) + intval($daysInMonths/7);

    $monthEndingDay= date('N',strtotime($year.'-'.$month.'-'.$daysInMonths));

    $monthStartDay = date('N',strtotime($year.'-'.$month.'-01'));

    if($monthEndingDay<$monthStartDay){

      $numOfweeks++;

    }

    return $numOfweeks;
  }

  /**
   * calculate number of days in a particular month
   */
  private function _daysInMonth($month=null,$year=null){

    if(null==($year))
      $year =  date("Y",time());

    if(null==($month))
      $month = date("m",time());

    return date('t',strtotime($year.'-'.$month.'-01'));
  }

}