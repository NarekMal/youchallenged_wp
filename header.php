<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
  <base href="/">
  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta http-equiv="X-UA-COMPATIBLE" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, maximum-scale=1.0">
  <!--<meta name="viewport" content="width=device-width, initial-scale=1">-->
  <title><?php bloginfo( 'name' ); wp_title(); ?></title>
  <?php wp_head(); ?>
</head>

<body <?php body_class(); echo ( is_page_template() ) ? '' : 'ng-app="ycweb" ng-style="bodyStyle"'; ?>>
<div class="main-wrapper">
  <?php
  if(!is_user_logged_in()){
    ?>

    <div class="header">
      <div class="container">

        <div class="logoBl">
          <a href="<?php echo get_bloginfo('url'); ?>" class="logo-link">
            <img src="<?php echo get_template_directory_uri(); ?>/img/new-design/you-challenged.png" alt="" class="logo img-responsive" />

          </a>
        </div>
        <div class="col-md-1 col-sm-2 col-xs-2">
          <button class="login-btn pull-right open-login-form" name="login-btn">LOG IN</button>
        </div>


        <div id="myModal" class="modal fade" role="dialog">
          <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <h2 class="welcome">WELCOME BACK!</h2>
                <div class="forgot-pass"><a href="#">Forgot Password?</a></div>
              </div>
              <div class="modal-body">
                <div class="login-form-box">
                  <?php echo do_shortcode('[cflrf_custom_login_form]') ?>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div id="myModal2" class="modal fade" role="dialog">
          <div class="modal-dialog">
            <!-- Modal content-->

            <div class="modal-body">
              <div class="modal-content">
                <div class="modal-header">
                  <h2 class="welcome">SUBSCRIBE NOW</h2>
                </div>
                <div class="login-form-box">
                  <form method="post" action="/" class="ng-pristine ng-valid">
                    <input name="username" type="text" class="" value="" placeholder="Username" >
                    <input name="email" type="text" class="" value="" placeholder="Email" >
                    <input name="pass1" type="password" class="" value="" placeholder="Password" >
                    <input name="pass2" type="password" class="" value="" placeholder="Confirm Password" >
                    <div class="gender-select">
                      <p class="gender-title"><label>Gender</label></p>
                      <p class="genders-man"><label for="man"><input name="gender" id="man" type="radio" value="man">MALE</label></p>
                      <p class="gender-woman"><label for="woman"><input name="gender" id="woman" type="radio" value="woman">FEMALE</label></p>
                    </div>
                    <input class="login-sub-btn" id="login-sub-btn" type="submit" value="SIGN UP">
                    <a href="" class="signin-link">SIGN IN</a>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div id="myModal3" class="modal fade" role="dialog">
          <div class="modal-dialog">
            <!-- Modal content-->

            <div class="modal-body">
              <div class="modal-content">
                <div class="modal-header">
                  <h2 class="welcome">RESET PASSWORD</h2>
                </div>
                <div class="login-form-box reset-form-box">
                  <iframe class="lost-pass-iframe" src="/wp-login.php?action=lostpassword" scrolling="no"></iframe>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div ng-if="!is_user_logged_in" class="bg-white landing-page">
      <div class="landBl1">
        <!--Landing page for new users-->

        <!--         <div class="row container">
                    <div class="col-sm-12 main-heading">
                        Become Better Every Day<br/> With You Coached
                    </div>
                </div> -->
        <div class="row container">
          <div class="center_content2 video_content">
            <!-- <iframe src="https://player.vimeo.com/video/192381904" width="640" height="273" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe> -->
            <!-- <iframe src="https://player.vimeo.com/video/192381904" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe> -->
            <!--<iframe src="https://player.vimeo.com/video/192381904?loop=1&title=0&byline=0" width="640" height="273" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe> -->
            <iframe src="https://player.vimeo.com/video/192381904" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
            <!-- <iframe src="https://player.vimeo.com/video/192381904?loop=1&title=0&byline=0" width="WIDTH" height="HEIGHT" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe> -->
          </div>
          <div class="btBl">
            <a href="" class="quiz-btn open-login-form" >Take quiz</a>
            <button class="subscribe-btn open-login-form" name="subscribe-btn">Subscribe</button>
          </div>
        </div>
        <!--         <div class="row container">
                    <div class="col-md-12 head">WHAT WE DO</div>
                </div> -->
        <div class="row container">
          <div class="col-sm-12 paragraph">
            YouCoached sends you personalized messages that motivate, educate, and challenge for only $9.99 a month. No commitment. Cancel anytime. It’s time to unleash your awesome.
          </div>
        </div>
        <!--         <div class="row container">
                    <div class="col-sm-12 sub-par">
                        Why? Because we believe your whole life matters.
                    </div>
                </div> -->
      </div>
      <div class="row area-box landBl2">
        <div class="col-md-12">
          <h1 class="heading">Areas we coach</h1>
          <div class="row container">
            <div class="col-md-3 col-sm-3 col-xs-6 box full open-login-form landing-box">
              <div class="sub-box">
                <button class="button fitness-btn home-fashion-btn" name="">FITNESS</button>
                <div class="image" style="background-image: url('wp-content/themes/al_youcoached/img/main_categories/land-Muscular.png')"></div>
                <div class="desc-box msg-desc-box">
                  <p class="msg-desc msg-descs">Weekly workouts and meal plans to help you reach your goals.</p>
                </div>
              </div>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-6 box full open-login-form landing-box">
              <div class="sub-box">
                <button class="button hair-btn home-fashion-btn" name="">HAIR AND MAKEUP</button>
                <div class="image" style="background-image: url('wp-content/themes/al_youcoached/img/main_categories/land-Step_9_.png')"></div>
                <div class="desc-box msg-desc-box">
                  <p class="msg-desc msg-descs">Step by step guides to take your hair and makeup to the next level.</p>
                </div>
              </div>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-6 box full open-login-form landing-box">
              <div class="sub-box">
                <button class="button relation-btn home-relation-btn" name="">RELATIONSHIPS</button>
                <div class="image" style="background-image: url('wp-content/themes/al_youcoached/img/main_categories/land-fashionable-.png')"></div>
                <div class="desc-box msg-desc-box">
                  <p class="msg-desc msg-descs">Advice and exercises for each phase of a relationship.</p>
                </div>
              </div>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-6 box full open-login-form landing-box">
              <div class="sub-box">
                <button class="button fashion-btn home-fashion-btn" name="">INTERIOR</button>
                <div class="image" style="background-image: url('wp-content/themes/al_youcoached/img/main_categories/land_Rendering.png')"></div>
                <div class="desc-box msg-desc-box">
                  <p class="msg-desc msg-descs">How to think and act like a designer so you can make your own space amazing.</p>
                </div>
              </div>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-6 box full open-login-form landing-box">
              <div class="sub-box">
                <button class="button fashion-btn home-fashion-btn" name="">FASHION</button>
                <div class="image" style="background-image: url('wp-content/themes/al_youcoached/img/main_categories/land-Fashion.png')"></div>
                <div class="desc-box msg-desc-box">
                  <p class="msg-desc msg-descs">How to dress for your body type, establish your own look, and where to get it all. </p>
                </div>
              </div>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-6 box full open-login-form landing-box">
              <div class="sub-box">
                <button class="button lifestyle-btn home-fashion-btn" name="">LIFE COACHING</button>
                <div class="image" style="background-image: url('wp-content/themes/al_youcoached/img/main_categories/land-Planning.png')"></div>
                <div class="desc-box msg-desc-box">
                  <p class="msg-desc msg-descs">Figure out who you are, what you want, and how to get there. </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row landBl3">
        <div class="col-sm-12 col-xs-12">
          <h2 class="heading">HOW IT WORKS</h2>
          <div class="container">
            <div class=" BlQu">
              <h4 class="sml-headings">QUIZ</h4>
              <div class="lanImg"></div>
              <div class="desc">
                Take the Evaluation Quiz to help us get to know you a little better
              </div>
            </div>
            <div class=" BlMe">
              <h4 class="sml-headings">MESSAGES</h4>
              <div class="lanImg"></div>
              <div class="desc">
                Receive Messages from coaches that Motivate, Educate, and Challenge
              </div>
            </div>
            <div class=" BlCh">
              <h4 class="sml-headings">POINTS</h4>
              <div class="lanImg"></div>
              <div class="desc">
                Complete challenges to earn points toward giveaways
              </div>
            </div>
          </div>
          <h2 class="heading">WHAT'S IN A MESSAGE ?</h2>
          <div class="container utp-block">
            <div class="col-md-4 utp-item-wrapper">
              <div class="utp-item">
                <div class="utp--inner-wrapper">
                  <div class="utp-item--heading">Motivate</div>
                  <img src="wp-content/themes/al_youcoached/img/utp-img-1.png" alt="" class="utp-item--img">
                  <div class="utp-item--description">Let’s be honest, if you’re not feeling it, you’re not doing it. Don’t worry, our coaches have your back. Every message helps to motivate you toward your goals.</div>
                </div>
              </div>
            </div>
            <div class="col-md-4 utp-item-wrapper">
              <div class="utp-item">
                <div class="utp--inner-wrapper">
                  <div class="utp-item--heading">Educate</div>
                  <img src="wp-content/themes/al_youcoached/img/utp-img-2.png" alt="" class="utp-item--img">
                  <div class="utp-item--description">Because we want you to know the why, messages have step-by-step guides, exercises, where to get what you need, and how to create real change in your life.</div>
                </div>
              </div>
            </div>
            <div class="col-md-4 utp-item-wrapper">
              <div class="utp-item">
                <div class="utp--inner-wrapper">
                  <div class="utp-item--heading">Challenge</div>
                  <img src="wp-content/themes/al_youcoached/img/utp-img-3.png" alt="" class="utp-item--img">
                  <div class="utp-item--description">Our coaches want you to <i>actually</i> take your life to the next level, so they ask you to accept challenges. And the more you complete challenges, the more points you earn toward giveaways.</div>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>
            <div class="utp-slogan"><div class="utp-slogan--inner open-login-form">Start 30 Day Free Trial</div></div>
            <div class="utp-pay">Then only $9.99 a month.</div>
          </div>
          <div class="btBl">
            <a href="" class="quiz-btn open-login-form" >Take quiz</a>
            <button class="subscribe-btn open-login-form" name="subscribe-btn">Subscribe</button>
          </div>
        </div>
      </div>
      <!-- <div class="b-divdr"></div> -->
      <div class=" landBl4">
        <div id="jssor_1" style="position: relative; margin: 0 auto; top: 0px; left: 0px; width: 1000px; height: 451px; overflow: visible; visibility: hidden;">
          <!-- Loading Screen -->
          <!--<div data-u="loading" style="position: absolute; top: 0px; left: 0px;">
            <div style="filter: alpha(opacity=70); opacity: 0.7; position: absolute; display: block; top: 0px; left: 0px; width: 100%; height: 100%;"></div>
            <div style="position:absolute;display:block;background:url('img/loading.gif') no-repeat center center;top:0px;left:0px;width:100%;height:100%;"></div>
          </div>-->

          <div data-u="slides" style="cursor: default; position: relative; top: 0px; left: 0px; width: 1000px; height: 451px; overflow: hidden;" class="slides">
            <div style="display: none;" class="resp_imgg">
              <div data-u="image" class="slImg" style="background-image: url('wp-content/themes/al_youcoached/img/slider-image1.png')"></div>
            </div>
            <div style="display: none;" class="resp_imgg">
              <div data-u="image" class="slImg" style="background-image: url('wp-content/themes/al_youcoached/img/slider-image2.png')"></div>
            </div>
            <div style="display: none;" class="resp_imgg">
              <div data-u="image" class="slImg" style="background-image: url('wp-content/themes/al_youcoached/img/slider-image3.png')"></div>
            </div>
            <div style="display: none;" class="resp_imgg">
              <div data-u="image" class="slImg" style="background-image: url('wp-content/themes/al_youcoached/img/slider-image1.png')"></div>
            </div>
            <div style="display: none;" class="resp_imgg">
              <div data-u="image" class="slImg" style="background-image: url('wp-content/themes/al_youcoached/img/slider-image2.png')"></div>
            </div>
            <div style="display: none;" class="resp_imgg">
              <div data-u="image" class="slImg" style="background-image: url('wp-content/themes/al_youcoached/img/slider-image3.png')"></div>
            </div>
            <a data-u="ad" href="http://www.jssor.com" style="display:none">Bootstrap Slider</a>
          </div>

          <span data-u="arrowleft" class="jssora03l" style="top:0px;left:-80px;width:55px;height:55px;" data-autocenter="2"><img src="/wp-content/themes/al_youcoached/img/arrow-left.png" /></span>
          <span data-u="arrowright" class="jssora03r" style="top:0px;right:-50px;width:55px;height:55px;" data-autocenter="2"><img src="/wp-content/themes/al_youcoached/img/arrow-right.png" /></span>
        </div>
      </div>
      <div class="row get-app-box container landBl5">
        <div class="col-sm-6 col-xs-12 col-md-6 lft-side">
          <img src="/wp-content/themes/al_youcoached/img/phone.png" alt="" class="phone" />
        </div>
        <div class="col-sm-6 right-side col-md-6 col-xs-12 ">
          <div class="btBl">
            <a href="" class="quiz-btn open-login-form" >Take quiz</a>
            <button class="subscribe-btn open-login-form" name="subscribe-btn">Subscribe</button>
          </div>
          <h2 class="headings">GET THE APP</h2>
          <div class="app-content">
            (Coming Soon) Get free iOS or Android app<br/> and receive coaching anytime, anywhere
          </div>
          <img src="/wp-content/themes/al_youcoached/img/app-store.png" alt="" class="app-store-img" /><br/><br>
          <img src="/wp-content/themes/al_youcoached/img/google-play.png" alt="" class="google-play-img" />
        </div>
      </div>
      <div class="row container">

      </div>

    </div>
    <?php
  }else{
    ?>

    <div class="header">
      <div class="container">
        <div class="logoBl">
          <a href="<?php echo get_bloginfo('url'); ?>" class="logo-link">
            <img src="<?php echo get_template_directory_uri(); ?>/img/new-design/you-challenged.png" alt="" class="logo img-responsive" />
          </a>
        </div>
        <div class="col-md-8 col-sm-8 col-xs-8 menu-list">
          <?php if ( has_nav_menu( 'header-menu' ) ) : ?>
            <nav ng-controller="navigation" id="header-menu" class="menu-list" role="navigation" aria-label="<?php esc_attr_e( 'Header Manu', 'al_youcoached_theme' ); ?>">
              <?php
              //
              class Walker_Quickstart_Menu extends Walker {

                var $db_fields = array(
                  'parent' => 'menu_item_parent',
                  'id'     => 'db_id'
                );
                function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
                  $path = parse_url( $item->url, PHP_URL_PATH );
                  if(empty($path)) $path="/";
                  $class = trim($path, '/');
                  if ( empty( $class ) ) {
                    $class = 'home';
                  }
                  $output .= sprintf( "\n<li ng-class=\"{ active: isActive('$path')}\" class='%s'><a href='%s'>%s</a></li>\n",
                    $class,
                    $item->url,
                    $item->title
                  );
                }
              }
              //
              wp_nav_menu( array(
                'theme_location' => 'header-menu',
                'depth'          => 1,
                'menu_id' => 'menu',
                'walker'  => new Walker_Quickstart_Menu()
              ) );
              ?>
              <!-- <div id="clickMenu"></div> -->
              <button type="button" id="clickMenu" class="tcon tcon-menu--xcross" aria-label="toggle menu">
                <span class="tcon-menu__lines" aria-hidden="true"></span>
                <span class="tcon-visuallyhidden"></span>
              </button>
            </nav><!-- .header-menu -->
          <?php endif; ?>
        </div>
      </div>
    </div>

  <?php } ?>
