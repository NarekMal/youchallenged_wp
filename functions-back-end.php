<?php

// Add post thumbnail support
add_theme_support('post-thumbnails');


// todo add deactivation hook and action

add_action("after_switch_theme", "al_you_coached_theme_actiovation");

function al_you_coached_theme_actiovation()
{

  global $wpdb;

  $table = $wpdb->prefix . 'al_postscat_by_date';

  $myrows = $wpdb->get_results("CREATE TABLE `$table` (

																	`id` int(11) NOT NULL AUTO_INCREMENT,

																	`date` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,

																	`category` text COLLATE utf8mb4_unicode_ci NOT NULL,

																	PRIMARY KEY (`id`)

																) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci");


  update_option('al_trial_time', 'month', false);
  update_option('challenge_value', 50, false);
  update_option('selfgoal_value', 20, false);

}


add_action("after_switch_theme", "al_add_coach_role");

function al_add_coach_role()
{

  $capabilities = array(

    'read' => true,

    'edit_message' => true,

    'publish_message' => true,

    'read_message' => true,

    'delete_message' => true,

    'upload_files' => true,

    'edit_published_message' => true,

    'delete_others_message' => false,

    'edit_others_message' => true

  );

  remove_role('al_coach');

  add_role('al_coach', __('Coach', 'al_cust_tax'), $capabilities);


}


// display settings page

add_action('admin_menu', 'al_calendar_menu');

function al_calendar_menu()
{

  //create new top-level menu

  add_menu_page('Post display settings', 'Messages Calendar', 'administrator', 'al_calendar_page', 'la_calendar_page', 'dashicons-calendar-alt', 38);

}

function la_calendar_page()
{

  include_once plugin_dir_path(__FILE__) . 'class/Calendar.php';

  ?>

  <div class="wrap full-height">

    <h2>Posts Categories Calendar</h2>

    <p>Select category (categories), you want to be shown, on selected day</p>

    <p>After you set all categories don't forget to save changes - pressing button "Save"!</p>


    <?php

    $calendar = new Calendar();


    echo $calendar->show(); ?>

  </div>


  <div id="load-main">

    <div id='load'>

      <div id='block_1' class='load_block'></div>

      <div id='block_2' class='load_block'></div>

      <div id='block_3' class='load_block'></div>

    </div>

  </div>

<?php }


// ajax function add category to selected date

add_action('wp_ajax_al_ajax_add_cat', 'al_ajax_add_cat');

function al_ajax_add_cat()
{

  $data = $_POST['data'];

  global $wpdb;

  $table = $wpdb->prefix . 'al_postscat_by_date';

  $result = '';


  foreach ($data as $k => $v) {

    $date_str = strtotime($k);

    $count = $wpdb->get_var("SELECT id FROM $table WHERE date = $date_str");


    if (!is_null($count)) {

      // if isset - update

      $category_val = $v ? implode(',', $v) : '';

      $result = $wpdb->update($table, array('category' => $category_val), array('date' => $date_str));


    } else {

      // if !isset - insert

      $category_val = $v ? implode(',', $v) : '';

      $result = $wpdb->insert($table, array('date' => $date_str, 'category' => $category_val), array('%s', '%s'));

    }

  }

  if ($result == 1 || $result == '') {

    echo 'done';

  } else {

    echo 'error';

  }

  die;

}

// calendar end


// custom post type message

add_action('init', 'al_add_capabilities_to_administrator');

function al_add_capabilities_to_administrator()
{

  $role = get_role('administrator');

  foreach (array('publish', 'delete', 'delete_others', 'read', 'delete_private', 'delete_published', 'edit', 'edit_others', 'edit_private', 'edit_published', 'read_private') as $cap) {

    $role->add_cap("{$cap}_message");

  }

  foreach (array('publish', 'delete', 'delete_others', 'read', 'delete_private', 'delete_published', 'edit', 'edit_others', 'edit_private', 'edit_published', 'read_private') as $cap) {

    $role->add_cap("{$cap}_default_message");

  }

}


add_action('init', 'al_custom_post_message');

function al_custom_post_message()
{


  $labels = array(

    'name' => __('Messages', 'al_cust_tax'),

    'all_items' => __('All Messages', 'al_cust_tax'),

    'singular_name' => __('message', 'al_cust_tax'),

    'add_new' => __('New Message', 'al_cust_tax'),

    'add_new_item' => __('Add New message', 'al_cust_tax'),

    'edit_item' => __('Edit message', 'al_cust_tax'),

    'new_item' => __('New message', 'al_cust_tax'),

    'view_item' => __('View message', 'al_cust_tax'),

    'search_items' => __('Search messages', 'al_cust_tax'),

    'not_found' => __('No Messages Found', 'al_cust_tax'),

    'not_found_in_trash' => __('No messages found in Trash', 'al_cust_tax'),

  );

  $args = array(

    'labels' => $labels,
    'public' => true,
    'has_archive' => true,
    'supports' => array(
      'title',
      'editor',
      'excerpt',
      'thumbnail',
      'page-attributes',
      'revisions'
    ),

    'capabilities' => array(

      'edit_post' => 'edit_message',

      'edit_posts' => 'edit_message',

      'edit_others_posts' => 'edit_others_message',

      'edit_published_posts' => 'edit_published_message',

      'publish_posts' => 'publish_message',

      'read_post' => 'read_message',

      'read_private_posts' => 'read_private_message',

      'delete_post' => 'delete_message',

      'delete_posts' => 'delete_message',

      'delete_others_posts' => 'delete_others_message'

    ),

    'menu_icon' => 'dashicons-format-status',

    'register_meta_box_cb' => 'al_meta_is_challenge',

  );

  register_post_type('message', $args);

//  remove_role('al_coach');

  $labels = array(

    'name' => __('Default Message', 'al_cust_tax'),

    'all_items' => __('All Default Messages', 'al_cust_tax'),

    'singular_name' => __('message', 'al_cust_tax'),

    'add_new' => __('New Default Message', 'al_cust_tax'),

    'add_new_item' => __('Add New Default message', 'al_cust_tax'),

    'edit_item' => __('Edit Default message', 'al_cust_tax'),

    'new_item' => __('New Default message', 'al_cust_tax'),

    'view_item' => __('View Default message', 'al_cust_tax'),

    'search_items' => __('Search Default messages', 'al_cust_tax'),

    'not_found' => __('No Default Messages Found', 'al_cust_tax'),

    'not_found_in_trash' => __('No default messages found in Trash', 'al_cust_tax'),

  );

  $args = array(

    'labels' => $labels,

    'public' => true,

    'has_archive' => true,

    'supports' => array(

      'title',

      'editor',

      'excerpt',

      'thumbnail',

      'page-attributes'

    ),

    'capabilities' => array(

      'edit_post' => 'edit_default_message',

      'edit_posts' => 'edit_default_message',

      'edit_others_posts' => 'edit_other_default_message',

      'edit_published_posts' => 'edit_published_message',

      'publish_posts' => 'publish_default_message',

      'read_post' => 'read_default_message',

      'read_private_posts' => 'read_private_default_message',

      'delete_post' => 'delete_default_message',

      'delete_posts' => 'delete_default_message',

      'delete_others_posts' => 'delete_others_default_message'

    ),

    'menu_icon' => 'dashicons-format-status',

    'register_meta_box_cb' => 'al_def_messages_metaboxes',

  );

  register_post_type('default_message', $args);

//  remove_role('al_coach');

}


// adding metabox to message

add_action('add_meta_boxes', 'al_meta_is_challenge');


function al_meta_is_challenge()
{

  add_meta_box('al_meta_challenge', __('Challenge'), 'al_meta_challenge', 'message', 'side');

  add_meta_box('book-image', __('Message Image'), 'book_image_meta_box', 'message', 'normal', 'high');

  add_meta_box('challenges_description', __('Description of accepted challenge'), 'challenges_description_meta_box', 'message', 'normal');

  add_meta_box('challenges_points', __('Points'), 'challenges_points', 'message', 'side');

//  add_meta_box( 'message-priority', __( 'Order Priority' ), 'order_priority', 'message', 'side', 'high' );

}

// adding metabox to default message

add_action('add_meta_boxes', 'al_def_messages_metaboxes');


function al_def_messages_metaboxes()
{

  add_meta_box('al_meta_main_category', __('Main Category'), 'al_meta_main_category', 'default_message', 'side');

  add_meta_box('book-image', __('Message Image'), 'book_image_meta_box', 'default_message', 'normal', 'high');


}


function al_meta_main_category()
{

  global $post;

  $main_cat = get_terms('main_category', 'hide_empty=0');

  $current_main_cat = wp_get_object_terms($post->ID, 'main_category');

  ?>

  <select name='def_message_category' id='def_message_category'>

    <!-- Display themes as options -->

    <?php

    ?>

    <option class='theme-option' value=''

      <?php if (!count($current_main_cat)) echo "selected"; ?>>None
    </option>

    <?php

    foreach ($main_cat as $theme) {

      if (!is_wp_error($current_main_cat) && !empty($current_main_cat) && !strcmp($theme->slug, $current_main_cat[0]->slug))

        echo "<option class='theme-option' value='" . $theme->slug . "' selected>" . $theme->name . "</option>\n";

      else

        echo "<option class='theme-option' value='" . $theme->slug . "'>" . $theme->name . "</option>\n";

    }

    ?>

  </select>

  <?php

}


function al_meta_challenge()
{

  global $post;


  echo '<input type="hidden" name="is_challange" id="is_challange" value="' .

    wp_create_nonce(plugin_basename(__FILE__)) . '" />';


  $challenge = get_post_meta($post->ID, 'challenge', true);

  ?>

  <p><input type="checkbox" id="challenge" name="challenge" value="yes"
            class="widefat js-acc-chall" <?php checked($challenge, 'yes') ?>/> is a challenge</p>

  <?php

}


function book_image_meta_box()
{

  global $post;


  $image_src = '';


  $image_id = get_post_meta($post->ID, '_image_id', true);

  $image_src = wp_get_attachment_url($image_id);


  ?>

  <img id="book_image" src="<?php echo $image_src ?>" style="max-width:100%;"/>

  <input type="hidden" name="upload_image_id" id="upload_image_id" value="<?php echo $image_id; ?>"/>

  <p>

    <a title="<?php esc_attr_e('Set message image') ?>" href="#"
       id="set-book-image"><?php _e('Set message image') ?></a>

    <a title="<?php esc_attr_e('Remove message image') ?>" href="#" id="remove-book-image"
       style="<?php echo(!$image_id ? 'display:none;' : ''); ?>"><?php _e('Remove message image') ?></a>

  </p>


  <script type="text/javascript">

    jQuery(document).ready(function ($) {



      // save the send_to_editor handler function

      window.send_to_editor_default = window.send_to_editor;


      $('#set-book-image').click(function () {



        // replace the default send_to_editor handler function with our own

        window.send_to_editor = window.attach_image;

        tb_show('', 'media-upload.php?post_id=<?php echo $post->ID ?>&amp;type=image&amp;TB_iframe=true');


        return false;

      });


      $('#remove-book-image').click(function () {


        $('#upload_image_id').val('');

        $('img').attr('src', '');

        $(this).hide();


        return false;

      });


      // handler function which is invoked after the user selects an image from the gallery popup.

      // this function displays the image and sets the id so it can be persisted to the post meta

      window.attach_image = function (html) {



        // turn the returned image html into a hidden image element so we can easily pull the relevant attributes we need

        $('body').append('<div id="temp_image">' + html + '</div>');


        var img = $('#temp_image').find('img');


        imgurl = img.attr('src');

        imgclass = img.attr('class');

        imgid = parseInt(imgclass.replace(/\D/g, ''), 10);


        $('#upload_image_id').val(imgid);

        $('#remove-book-image').show();


        $('img#book_image').attr('src', imgurl);

        try {
          tb_remove();
        } catch (e) {
        }
        ;

        $('#temp_image').remove();


        // restore the send_to_editor handler function

        window.send_to_editor = window.send_to_editor_default;


      }


    });

  </script>

  <?php

}


function challenges_description_meta_box()
{

  global $post;

  $challenge = get_post_meta($post->ID, 'challenge', true);

  $value = get_post_meta($post->ID, 'accept-chall-descr', true);

  ?>

  <div class="<?php echo $challenge !== 'yes' ? 'hide' : ''; ?> chall-acc-toggle">

    <?php wp_editor($value, 'acc-chall-id', array('textarea_name' => 'accept-chall-descr', 'textarea_rows' => 20)) ?>

  </div>

  <?php

}


function challenges_points()
{

  global $post;

  $challenge = get_post_meta($post->ID, 'challenge', true);

  $value = get_post_meta($post->ID, 'points', true);
  $value = intval($value);
  $value_default = get_option('challenge_value');
  $value = $value ? $value : $value_default

  ?>

  <div class="<?php echo $challenge !== 'yes' ? 'hide' : ''; ?> chall-acc-toggle">

    <label for=""><input type="number" class="widefat" value="<?php echo $value; ?>" name="points"></label>

  </div>


  <?php

}


// register taxonomies

add_action('init', 'al_register_taxonomy');

function al_register_taxonomy()
{

  $labels = array('name' => __('Main Category', 'al_cust_tax'), 'singular_name' => __('Main category', 'al_cust_tax'), 'all_items' => __('All main categories', 'al_cust_tax'), 'parent_item' => __('Parent Item', 'al_cust_tax'), 'parent_item_colon' => __('Parent Item', 'al_cust_tax'), 'edit_item' => __('Edit main category', 'al_cust_tax'), 'update_item' => __('Update main category', 'al_cust_tax'), 'add_new_item' => __('Add new main category', 'al_cust_tax'), 'new_item_name' => __('New main category', 'al_cust_tax'), 'separate_items_with_commas' => __('Separate categories with commas', 'al_cust_tax'), 'menu_name' => __('Main Categories', 'al_cust_tax'));

  register_taxonomy('main_category', array('message', 'default_message'), array('heirarchical' => false, 'capabilities' => array('manage_terms'), 'labels' => $labels, 'query_var' => true, 'rewrite' => true, 'show_admin_column' => true));


  $labels = array('name' => __('Campaign', 'al_cust_tax'), 'singular_name' => __('Campaign', 'al_cust_tax'), 'all_items' => __('All campaigns', 'al_cust_tax'), 'parent_item' => __('Parent Item', 'al_cust_tax'), 'parent_item_colon' => __('Parent Item', 'al_cust_tax'), 'edit_item' => __('Edit campaign', 'al_cust_tax'), 'update_item' => __('Update campaign', 'al_cust_tax'), 'add_new_item' => __('Add new campaign', 'al_cust_tax'), 'new_item_name' => __('New campaign', 'al_cust_tax'), 'separate_items_with_commas' => __('Separate categories with commas', 'al_cust_tax'), 'menu_name' => __('Campaigns', 'al_cust_tax'), 'popular_items' => NULL);

  register_taxonomy('campaign', 'message', array('heirarchical' => false, 'capabilities' => array('manage_terms'), 'labels' => $labels, 'query_var' => true, 'rewrite' => true, 'show_admin_column' => true, 'meta_box_cb' => 'al_select_view_campaign'));

}


add_action('init', 'al_register_meta');

function al_register_meta()
{

  register_meta('term', 'color', 'al_sanitize_hex');

  register_meta('term', 'parent', 'al_sanitize_parent_id');

  register_meta('term', 'relations', 'al_sanitize_relations');

}


function al_sanitize_hex($color)
{

  $color = ltrim($color, '#');

  return preg_match('/([A-Fa-f0-9]{3}){1,2}$/', $color) ? $color : '';

}


function al_sanitize_parent_id($parent)
{

  return (int)$parent;

}

function al_sanitize_relations($relations)
{

  return $relations;

}


function al_get_term_color($term_id, $hash = false)
{

  $color = get_term_meta($term_id, 'color', true);

  $color = al_sanitize_hex($color);


  return $hash && $color ? "#{$color}" : $color;

}


// color selector for add main cat page

add_action('main_category_add_form_fields', 'al_new_term_color_field');

function al_new_term_color_field()
{


  wp_nonce_field(basename(__FILE__), 'al_term_color_nonce'); ?>


  <div class="form-field al-term-color-wrap">

    <label for="al-term-color"><?php _e('Color', 'al_term_parent_nonce'); ?></label>

    <input type="text" name="al_term_color" id="al-term-color" value="" class="al-color-field"

           data-default-color="#ffffff"/>

  </div>

<?php }


// color selector for edit main cat page

add_action('main_category_edit_form_fields', 'al_edit_term_color_field');

function al_edit_term_color_field($term)
{


  $default = '#ffffff';

  $color = al_get_term_color($term->term_id, true);


  if (!$color) $color = $default; ?>


  <tr class="form-field al-term-color-wrap">

    <th scope="row"><label for="al-term-color"><?php _e('Color', 'al_term_parent_nonce'); ?></label></th>

    <td>

      <?php wp_nonce_field(basename(__FILE__), 'al_term_color_nonce'); ?>

      <input type="text" name="al_term_color" id="al-term-color" value="<?php echo esc_attr($color); ?>"

             class="al-color-field" data-default-color="<?php echo esc_attr($default); ?>"/>

    </td>

  </tr>

<?php }


// saving main cat color

add_action('edit_main_category', 'al_save_term_color');

add_action('create_main_category', 'al_save_term_color');

function al_save_term_color($term_id)
{


  if (!isset($_POST['al_term_color_nonce']) || !wp_verify_nonce($_POST['al_term_color_nonce'], basename(__FILE__))) return;


  $old_color = al_get_term_color($term_id);

  $new_color = isset($_POST['al_term_color']) ? al_sanitize_hex($_POST['al_term_color']) : '';


  if ($old_color && '' === $new_color) delete_term_meta($term_id, 'color');


  else if ($old_color !== $new_color) update_term_meta($term_id, 'color', $new_color);

}


// displaying main cat color in main table

add_filter('manage_edit-main_category_columns', 'al_edit_term_columns', 10, 1);

function al_edit_term_columns($columns)
{

  $columns['camp'] = __('Campaigns', 'al_term_parent_nonce');

  $columns['color'] = __('Color', 'al_term_parent_nonce');

  return $columns;

}

// displaying main cat color in main table

add_filter('manage_main_category_custom_column', 'al_manage_term_custom_column', 10, 3);

function al_manage_term_custom_column($out, $column, $term_id)
{

  if ('color' === $column) {

    $color = al_get_term_color($term_id, true);

    if (!$color) $color = '#ffffff';

    $out = sprintf('<span class="color-block" style="background:%s;">&nbsp;</span>', esc_attr($color));

  }

  if ('camp' === $column) {

    $args = array('hide_empty' => false, 'meta_query' => array(array('taxonomy' => 'campaign', 'key' => 'parent', 'value' => $term_id, 'compare' => '=',),), 'suppress_filters' => false,);

    $terms = get_terms($args);

    $count = count($terms);

    $out = '<span>' . $count . '</span>';

  }

  return $out;

}


// adding color scripts on main cat page / sort script camp page

add_action('admin_enqueue_scripts', 'al_admin_enqueue_scripts');

function al_admin_enqueue_scripts($hook_suffix)
{

  if ('main_category' == get_current_screen()->taxonomy) {

    wp_enqueue_style('wp-color-picker');

    wp_enqueue_script('wp-color-picker');

    add_action('admin_footer', 'al_term_colors_print_scripts');

  } else if ('edit-main_category' == get_current_screen()->id) {


    add_action('admin_footer', 'al_sort_table_print_scripts');

  }

}

function al_term_colors_print_scripts()
{ ?>

  <script type="text/javascript">

    jQuery(document).ready(function ($) {

      $('.al-color-field').wpColorPicker();

    });

  </script>

<?php }


function al_sort_table_print_scripts()
{ ?>

  <script type="text/javascript">

    jQuery(document).ready(function ($) {

      $('.wp-list-table.widefat.fixed.striped.tags').tablesorter();

    });

  </script>

<?php }


// Campaign settings

// add parent select

add_action('campaign_add_form_fields', 'parent_select_campaign');

function parent_select_campaign()
{

  $args = array('taxonomy' => 'main_category', 'hide_empty' => false);

  $taxonomies = get_terms($args);

  ?>

  <div class="form-field al-term-color-wrap">

    <label for="parent-select">Parent Main Category</label>

    <?php wp_nonce_field(basename(__FILE__), 'al_term_parent_nonce'); ?>

    <select name="parent" id="parent-select" class="main-select">

      <?php foreach ($taxonomies as $taxonomy) {

        echo '<option value="' . $taxonomy->term_id . '">' . $taxonomy->name . '</option>';

      } ?>

    </select>

  </div>

  <?php

}


add_action('campaign_edit_form_fields', 'al_edit_term_parent_field');

function al_edit_term_parent_field($term)
{

  $args = array('taxonomy' => 'main_category', 'hide_empty' => false);

  $taxonomies = get_terms($args);


  $parent = get_term_meta($term->term_id, 'parent', true);

  ?>


  <tr class="form-field al-term-color-wrap">

    <th scope="row"><label for="al-term-color"><?php _e('Parent', 'al_term_parent_nonce'); ?></label></th>

    <td>

      <?php wp_nonce_field(basename(__FILE__), 'al_term_parent_nonce'); ?>

      <select name="parent" id="parent-select" class="main-select">

        <?php foreach ($taxonomies as $taxonomy) {

          $selected = $parent == $taxonomy->term_id ? 'selected' : '';

          echo '<option value="' . $taxonomy->term_id . '" ' . $selected . '>' . $taxonomy->name . '</option>';

        } ?>

      </select>

    </td>

  </tr>

<?php }


add_action('edit_campaign', 'al_save_term_campaign');

add_action('create_campaign', 'al_save_term_campaign');

function al_save_term_campaign($term_id)
{

// todo can be changed to this


  if (!isset($_POST['al_term_parent_nonce']) || !wp_verify_nonce($_POST['al_term_parent_nonce'], basename(__FILE__))) return;

  if (!isset($_POST['al_term_coach_nonce']) || !wp_verify_nonce($_POST['al_term_coach_nonce'], basename(__FILE__))) return;

  if (!isset($_POST['al_term_relations_nonce']) || !wp_verify_nonce($_POST['al_term_relations_nonce'], basename(__FILE__))) return;


  $old_val = get_term_meta($term_id, 'parent', true);

  $new_val = isset($_POST['parent']) ? $_POST['parent'] : '';

  if ($old_val && '' === $new_val) delete_term_meta($term_id, 'parent');

  else if ($old_val !== $new_val) update_term_meta($term_id, 'parent', $new_val);


  $old_val = get_term_meta($term_id, 'coach', true);

  $new_val = isset($_POST['coach']) ? $_POST['coach'] : '';

  if ($old_val && '' === $new_val) delete_term_meta($term_id, 'coach');

  else if ($old_val !== $new_val) update_term_meta($term_id, 'coach', $new_val);


  $old_val = get_term_meta($term_id, 'relations', true);

  $new_val = isset($_POST['relations']) ? implode(',', $_POST['relations']) : '';

  if ($old_val && '' === $new_val) delete_term_meta($term_id, 'relations');

  else if ($old_val !== $new_val) update_term_meta($term_id, 'relations', $new_val);


}

// sortable columns

add_filter('manage_edit-campaign_columns', 'al_edit_term_columns_parent', 10, 1);

function al_edit_term_columns_parent($columns)
{

  $columns['parent'] = __('Parent', 'al_term_parent_nonce');

  $columns['coach'] = __('Coach', 'al_term_parent_nonce');

  return $columns;

}


add_filter('manage_edit-campaign_sortable_columns', 'register_date_column_for_issues_sortable', 10, 1);

function register_date_column_for_issues_sortable($columns)
{

  $columns['parent'] = 'parent';

  $columns['coach'] = 'coach';

  return $columns;

}


// adding parent column to camp page

add_filter('manage_campaign_custom_column', 'al_manage_term_custom_column_parent', 10, 3);

function al_manage_term_custom_column_parent($out, $column, $term_id)
{

  if ('parent' === $column) {

    $parent = get_term_meta($term_id, 'parent', true);

    if (!$parent) {

      $val = '';

    } else {

      $parent_name = get_term($parent);

      $val = esc_attr($parent_name->name);

    }

  }

  if ('coach' === $column) {

    $user = get_term_meta($term_id, 'coach', true);

    if ($user) {

      $user = get_userdata($user);

      $val = $user ? esc_attr($user->user_nicename) : '';

    }

  }

  $out = sprintf('<span class="">%s</span>', $val);

  return $out;

}


// coach select

add_action('campaign_add_form_fields', 'coach_select_campaign');

function coach_select_campaign()
{

  $args = array('role' => 'al_coach');

  $users = get_users($args);

  ?>

  <div class="form-field al-term-color-wrap">

    <label for="parent-select">Coach</label>

    <?php wp_nonce_field(basename(__FILE__), 'al_term_coach_nonce'); ?>

    <select name="coach" id="coach-select" class="main-select">

      <?php foreach ($users as $user) {

        echo '<option value="' . $user->ID . '">' . $user->user_nicename . '</option>';

      } ?>

    </select>

  </div>

  <?php

}


add_action('campaign_edit_form_fields', 'al_edit_term_coach_field');

function al_edit_term_coach_field($term)
{

  $args = array('role' => 'al_coach');

  $users = get_users($args);

  $coach = get_term_meta($term->term_id, 'coach', true);

  ?>


  <tr class="form-field al-term-color-wrap">

    <th scope="row"><label for="al-term-color"><?php _e('Coach', 'al_term_parent_nonce'); ?></label></th>

    <td>

      <?php wp_nonce_field(basename(__FILE__), 'al_term_coach_nonce'); ?>

      <select name="coach" id="coach-select" class="main-select">

        <?php foreach ($users as $user) {

          echo '<option value="' . $user->ID . '" ' . selected($coach, $user->ID, false) . '>' . $user->user_nicename . '</option>';

        } ?>

      </select>

    </td>

  </tr>

<?php }


// reltions select

add_action('campaign_add_form_fields', 'relations_select_campaign');

function relations_select_campaign()
{


  $result_clean = al_get_answers_custom_fileds();

  if (!empty($result_clean)) {

    ?>

    <div class="form-field al-term-relations-wrap">

      <label for="parent-select">Related to:</label>

      <?php wp_nonce_field(basename(__FILE__), 'al_term_relations_nonce');

      echo '<select name="relations[]" id="relations-select" class="main-select" multiple>';


      foreach ($result_clean as $k => $v) {

        echo '<optgroup label="' . $k . '">';

        foreach ($v as $k => $v) {


          echo '<option value="' . $k . '">' . $v . '</option>';

        }

        echo '</optgroup>';

      }

      echo '</select>';

      ?>

    </div>

    <?php

  } else {

    ?>

    <div class="form-field al-term-color-wrap">

      <label for="parent-select">Related to:</label>

      <p>No relations</p>

    </div>

    <?php

  }

}


add_action('campaign_edit_form_fields', 'al_edit_term_relations_field');

function al_edit_term_relations_field($term)
{

  $result_clean = al_get_answers_custom_fileds();

  $relations = get_term_meta($term->term_id, 'relations', true);

  $relations_arr = explode(',', $relations);

  ?>


  <tr class="form-field al-term-color-wrap">

    <th scope="row"><label for="al-term-color"><?php _e('Related to', 'al_term_parent_nonce'); ?></label></th>

    <td>

      <?php

      if (!empty($result_clean)) {


        wp_nonce_field(basename(__FILE__), 'al_term_relations_nonce');

        echo '<select name="relations[]" id="relations-select" class="main-select" multiple>';

        foreach ($result_clean as $k => $v) {

          echo '<optgroup label="' . $k . '">';

          foreach ($v as $k => $v) {

            $selected = in_array($k, $relations_arr) ? 'selected="selected"' : '';

            echo '<option value="' . $k . '" ' . $selected . '>' . $v . '</option>';

          }

          echo '</optgroup>';

        }

        echo '</select>';
      } else {

        echo '<p>No relations</p>';

      } ?>

    </td>

  </tr>

<?php }


function al_get_answers_custom_fileds()
{

  $all_questions = array(

    'gender' => array('man' => 'Man', 'woman' => 'Woman'),

    'fashion' => array('fash_chic' => 'Chic', 'fash_bohemian' => 'Bohemian', 'fash_classic' => 'Classic', 'fash_tomboy' => 'Tomboy', 'fash_rocker' => 'Rocker'),

    'body_type' => array('inverted-triangle' => 'Inverted Triangle', 'rectangle' => 'Rectangle', 'round' => 'Round', 'hourglass' => 'Hourglass', 'pear' => 'Pear'),

    'relationships' => array('single' => 'Single', 'new-relationship' => 'New Relationship', 'long-term-relationship' => 'Long Term Relationship', 'loss-of-relationship' => 'Starting Over in Relationships'),

    'fitness' => array('weight-loss' => 'Weight Loss', 'lean-and-tone' => 'Lean and Tone', 'athlete' => 'Athlete'),

    'interior_design' => array('des_bohemian' => 'Bohemian', 'des_modern' => 'Modern', 'des_rustic_farmhouse' => 'Rustic Farmhouse', 'des_industrial_urban' => 'Industrial Urban'),

    'skin_tone' => array('dark' => 'Dark', 'medium' => 'Medium', 'fair' => 'Fair'),

    'hair' => array('defined-curly' => 'Defined Curly', 'spiral-curl' => 'Spiral Curl', 'straight' => 'Straight', 'wavy' => 'Wavy'),

    'hair_type' => array('qual-fine' => 'Fine', 'qual-medium' => 'Medium', 'qual-coarse' => 'Coarse'),

  );

  return $all_questions;

}


// taxonomies order in admin menu

// - Main Categories

// - Campaigns

function al_change_displaying_order_menu()
{

  // todo add checking

  if (current_user_can('manage_options')) {

    global $submenu;

    $found = FALSE;

    $before = $after = array();

    $tax_slug = array('main_category', 'campaign');


    foreach ($submenu['edit.php'] as $item) {

      if (!$found || $item[2] === 'edit-tags.php?taxonomy=' . $tax_slug[0] || $item[2] === 'edit-tags.php?taxonomy=' . $tax_slug[1]) {

        $before[] = $item;

      } else {

        $after[] = $item;

      }

      if ($item[2] === 'edit-tags.php?taxonomy=main_category') $found = TRUE;

    }

    $submenu['edit.php'] = array_values(array_merge($before, $after));

  }


  remove_submenu_page('edit.php?post_type=default_message', 'edit-tags.php?taxonomy=main_category&amp;post_type=default_message');

//  remove_submenu_page( 'edit.php?post_type=portfolio', 'edit-tags.php?taxonomy=featured&amp;post_type=portfolio' );

}


;

add_action('admin_menu', 'al_change_displaying_order_menu');


function al_remove_meta_boxes()
{

  remove_meta_box('tagsdiv-main_category', 'message', 'side');

  remove_meta_box('members-cp', 'message', 'normal');

//  remove_meta_box('pageparentdiv', 'message', 'side');

}


add_action('do_meta_boxes', 'al_remove_meta_boxes', 100);


// callback displaying campaign metabox

function al_select_view_campaign($post)
{

  $post = get_post();

  // getting camp for current post

  $term_id = '';

  $campaign = wp_get_object_terms($post->ID, 'campaign', array('orderby' => 'term_id', 'order' => 'ASC'));

  if (!is_wp_error($campaign)) {

    if (isset($campaign[0]) && isset($campaign[0]->term_id))

      // id of the campaign for current message

      $term_id = $campaign[0]->term_id;

  }

  if (current_user_can('manage_options')) {

    // administrator

    $terms = get_terms(array('taxonomy' => 'campaign', 'hide_empty' => false));

  } else if (current_user_can('edit_others_message') && current_user_can('edit_message')) {

    // coach

    $coach_id = get_current_user_id();

    $args = array('hide_empty' => false, 'meta_query' => array(array('taxonomy' => 'campaign', 'key' => 'coach', 'value' => $coach_id, 'compare' => '=',),), 'suppress_filters' => false,);

    $terms = get_terms($args); ?>

    <?php // todo change val to ID and change save function

  }

  $terms = add_parent_to_camp($terms);
//  var_dump($terms);
  $options = '';

  $all_terms = array();

  // terms are

  // - if admin - all existing terms

  // - if a coach - terms related to current coach

  $flag = true;
  $options = '';
  if (!empty($terms)) {

    foreach ($terms as $k => $v) {
//      var_dump($v);echo '<br>';
      $options .= '<optgroup label="' . $k . '">';
      foreach ($v as $camp) {
        $all_terms[] = $camp[0];
        $options .= '<option value=" ' . esc_attr__($camp[0]) . ' " ' . selected($camp[0], $term_id, false) . ' >' . esc_attr__($camp[1]) . ' </option>';
      }
      $options .= '</optgroup>';
//      $options .= '<option value=" ' . esc_attr__($term->term_id) . ' " ' .  selected($term->term_id, $term_id, false) .' >' . esc_attr__($term->name) . ' </option>';
    }

  } else {

    $flag = false;

  }


  global $pagenow;
  $screen = get_current_screen();

  $disabled = '';

//  var_dump($pagenow);

  if ($pagenow != 'post-new.php' && $screen->id == 'message') {


    if (!current_user_can('edit_others_message') && !in_array($term_id, $all_terms)) {

      $disabled = 'disabled="disabled"';

    }

  }

  if ($flag) {

    ?>

    <select name="campaign" <?php echo $disabled; ?> class="full-width" id="">

      <?php echo $options; ?>

    </select>

    <?php

  } else {

    ?>

    <p>No campaigns are assigned to you yet</p>

    <?php


  }

}

function add_parent_to_camp($campaigns)
{
  if (!$campaigns) {
    return false;
  }

  $main_cats = get_terms('main_category', 'hide_empty=0');
  $main_cat_child = array();

  if (!$main_cats) {
    return false;
  }

  $output = array();
  foreach ($campaigns as $campaign) {
    $parent = get_term_meta($campaign->term_id, 'parent', true);

    foreach ($main_cats as $main_cat) {
      if ($parent == $main_cat->term_id) {
        $output[$main_cat->name][] = array($campaign->term_id, $campaign->name);
      }
    }
  }
  return $output;
}

add_action('save_post', 'save_post_camp_meta', 1, 2);

function save_post_camp_meta($post_id, $post)
{

  if (!empty($_POST)) {

    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) return;

    if (!current_user_can('edit_post', $post_id)) return;

    if ($post->post_type != 'message') return;


    if (isset($_POST['campaign'])) {

      $campaign_id = (int)$_POST['campaign'];


      // A valid rating is required, so don't let this get published without one

      if (empty($campaign_id)) {

        // unhook this function so it doesn't loop infinitely

        remove_action('save_post', 'save_post_camp_meta');

        $postdata = array('ID' => $post_id, 'post_status' => 'draft',);

        wp_update_post($postdata);

      } else {

        wp_set_object_terms($post_id, $campaign_id, 'campaign', false);

      }

    }


    if (isset($_POST['challenge']) && $_POST['challenge'] == 'yes') {

      if (!wp_verify_nonce($_POST['is_challange'], plugin_basename(__FILE__))) {
        return;
      }

      update_post_meta($post_id, 'challenge', 'yes');
      if (isset($_POST['accept-chall-descr'])) {
        update_post_meta($post_id, 'accept-chall-descr', $_POST['accept-chall-descr']);

      }


      if (isset($_POST['points'])) {
        $points_default = get_option('challenge_value', true);

        $points = intval($_POST['points']);
        $points = $points ? $points : $points_default;

        if ($points < 0) $points = 1;
        if ($points > 100) $points = 100;

        update_post_meta($post_id, 'points', (int)$_POST['points']);

      }


    } else {
      update_post_meta($post_id, 'challenge', 'no');
    }
    if (isset($_POST['upload_image_id'])) {
      update_post_meta($post_id, '_image_id', $_POST['upload_image_id']);
    }
  }

}


add_action('save_post', 'save_default_message_meta', 1, 2);

function save_default_message_meta($post_id, $post)
{

  if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) return;

  if (!current_user_can('edit_post', $post_id)) return;

  if ($post->post_type != 'default_message') return;


  if (isset($_POST['def_message_category'])) {


    wp_set_object_terms($post_id, $_POST['def_message_category'], 'main_category');

  }


  if (isset($_POST['upload_image_id'])) {

    update_post_meta($post_id, '_image_id', $_POST['upload_image_id']);

  }

}


// unset unused columns

add_filter('manage_edit-message_columns', 'al_columns_filter_message', 10, 1);

function al_columns_filter_message($columns)
{

  unset($columns['taxonomy-main_category']);

  return $columns;

}

add_filter('manage_edit-main_category_columns', 'al_columns_filter_main_cat', 10, 1);

function al_columns_filter_main_cat($columns)
{

  unset($columns['posts']);

  return $columns;

}


add_filter('main_category_row_actions', 'change_delete_button_main_category', 10, 2);

function change_delete_button_main_category($action, $tag)
{


  $action['delete'] = str_replace('delete-tag', 'delete-tag-al-custom', $action['delete']);

  return $action;

}


// ajax function add category to selected date

add_action('wp_ajax_al_check_if_has_no_children', 'al_check_if_has_no_children');

function al_check_if_has_no_children()
{


  $term_id = isset($_POST['tax_id']) ? str_replace('tag-', '', $_POST['tax_id']) : '';

  $args = array('hide_empty' => false, 'meta_query' => array(array('taxonomy' => 'campaign', 'key' => 'parent', 'value' => $term_id, 'compare' => '=',),), 'suppress_filters' => false,);

  $terms = get_terms($args);

  echo empty($terms) ? 'nothing' : 'exist';

  die;

}


add_action('admin_menu', 'remove_artist_meta');

function remove_artist_meta()
{

  remove_meta_box('main_category', 'post', 'normal');

}


// might be not needed
//add_filter('pre_get_posts', 'al_posts_for_current_author', 10, 1);

//function al_posts_for_current_author($query) {
//
//  if(is_admin()){
//
//    $screen = get_current_screen();
//
//
//
//    if ($query->is_admin && (!empty($screen) && $screen->id == 'edit-message') && !current_user_can('edit_others_message') && current_user_can('edit_message')) {
//
//      global $user_ID;
//
//      $query->set('author', $user_ID);
//
//    }
//
//  }
//
//  return $query;
//
//}

add_filter('bulk_actions-edit-main_category', 'al_delete_bulk_main_cat');

function al_delete_bulk_main_cat($actions)
{

  unset($actions['delete']);

  return $actions;

}


add_action('admin_init', 'al_quiz_options_registration');

function al_quiz_options_registration()
{

  register_setting('al_quiz_options', 'al_quiz_personality', 'al_validate_settings');

  add_settings_section('al_main_section', 'QUIZ Settings', 'al_main_section_cb', __FILE__);

  add_settings_field('al_banner_heading', 'Description for personality types ', 'al_banner_heading_settings', __FILE__, 'al_main_section');

}


function al_validate_settings($options)
{

//var_dump($options);

//die;

  $clean_arr = array();

  foreach ($options as $k => $v) {

    $clean_arr[$k] = htmlspecialchars($v);

  }

  return $clean_arr;

}


function al_main_section_cb()
{

  //optional

}

add_action('admin_menu', 'al_quiz_menu');

function al_quiz_menu()
{

  add_menu_page('QUIZ', 'Quiz results', 'administrator', 'al_quiz_page', 'la_quiz_page', 'dashicons-clipboard', 35);

  add_submenu_page('al_quiz_page', 'Read More', 'Read More', 'manage_options', 'answers_custom_fields', 'answers_custom_fields');

}

function answers_custom_fields()
{

  $answers_custom_fields = al_get_answers_custom_fileds();

  $personality_types = get_personality_types();

  $personality_types_add = array();
  foreach ($personality_types as $personality_type) {
    $personality_types_add[$personality_type] = $personality_type;
  }

  // Add lifestyle

  $answers_custom_fields = array_merge($answers_custom_fields, array(

    'lifestyle' => array(

      'lifestyle' => 'Lifestyle coaching'

    ),
    'Personality types' => $personality_types_add


  ));

  $current_tab = 'gender';

  if (!empty($_GET['tab']) && array_key_exists($_GET['tab'], $answers_custom_fields)) {

    $current_tab = $_GET['tab'];

  }


  if (isset($_POST['answers_custom_fields']) && isset($_POST['save_changes']) && wp_verify_nonce($_POST['save_changes'], __FILE__)) {

    $old = get_option('al_answers_custom_fields', array());
    $old = $old == '' ? array() : $old;

    $new = array_merge($old, $_POST['answers_custom_fields']);

    update_option('al_answers_custom_fields', $new);

  }


  $options_answers_custom_fields = get_option('al_answers_custom_fields');

  ?>

  <div class="wrap">

    <h1>Answers custom fields</h1>


    <h2 class="nav-tab-wrapper">

      <?php foreach ($answers_custom_fields as $tab => $value): $class = ($tab == $current_tab) ? ' nav-tab-active' : ''; ?>

        <?php ?>

        <a class='nav-tab<?php echo $class; ?>'

           href='?page=answers_custom_fields&tab=<?php echo $tab; ?>'><?php echo ucfirst(str_replace('_', ' ', $tab)); ?></a>

      <?php endforeach; ?>

    </h2>

    <form action="" method="POST" enctype="multipart/form-data">

      <?php wp_nonce_field(__FILE__, 'save_changes'); ?>

      <?php foreach ($answers_custom_fields[$current_tab] as $name => $title): ?>

        <?php if ('man' === $name) continue; ?>

        <h3><?php echo $title; ?></h3>

        <div id="wp-content-editor-tools" class="wp-editor-tools hide-if-no-js">

          <?php

          $editor_content = '';

          if (!empty($options_answers_custom_fields) && isset($options_answers_custom_fields[$name])) {

            $editor_content = $options_answers_custom_fields[$name];

          }

          wp_editor(wp_unslash($editor_content), 'answers_custom_fields_' . $name, array('textarea_name' => 'answers_custom_fields[' . $name . ']', 'textarea_rows' => 20));

          ?>

        </div>

      <?php endforeach; ?>

      <p class="submit">

        <input type="submit" name="submit" class="button button-primary" value="Save Changes">

      </p>

    </form>

  </div>

  <?php

}

function la_quiz_page()
{

  ?>

  <div class="wrap">

    <form action="options.php" method="POST" enctype="multipart/form-data">

      <?php settings_fields('al_quiz_options'); ?>

      <?php do_settings_sections(__FILE__); ?>

      <p class="submit">

        <input type="submit" name="submit" class="button button-primary" value="Save Changes">

      </p>

    </form>

  </div>

  <?php

}

function al_banner_heading_settings()
{ ?>


  <?php


  $quiz_personality = get_option('al_quiz_personality');

  $personalities = get_personality_types();

  foreach ($personalities as $personality) {

    ?>

    <div class="">

      <label for="<?php echo strtolower($personality); ?>"
             class="label-quiz-admin"><?php echo $personality; ?></label>

            <textarea name="al_quiz_personality[<?php echo strtolower($personality); ?>]"
                      id="<?php echo strtolower($personality); ?>" rows="5"
                      cols="100"><?php echo $quiz_personality[strtolower($personality)] ? $quiz_personality[strtolower($personality)] : ''; ?></textarea>


    </div>

    <?php


  } ?>

  <?php

}


function get_personality_types()
{
  return array('ISFJ', 'ISFP', 'ISTJ', 'ISTP', 'INFJ', 'INFP', 'INTJ', 'INTP', 'ESFJ', 'ESFP', 'ESTJ', 'ESTP', 'ENFJ', 'ENFP', 'ENTJ', 'ENTP');
}


// Custom frontend registration

class cflrf_registration_form

{


  // form properties

  private $username;

  private $email;

  private $password;


  function __construct()

  {

    add_shortcode('cflrf_registration_form', array($this, 'shortcode'));

  }

  function shortcode()

  {


    ob_start();


    if (isset($_REQUEST['reg_submit']) && $_REQUEST['reg_submit']) {

      $this->username = $_REQUEST['reg_name'];

      $this->email = $_REQUEST['reg_email'];

      $this->password = $_REQUEST['reg_password'];

      $this->validation();

      $this->registration();

    }


    $this->registration_form();

    return ob_get_clean();

  }

  function validation()

  {


    if (empty($this->username) || empty($this->password) || empty($this->email)) {

      return new WP_Error('field', 'Required form field is missing');

    }


    if (strlen($this->username) < 4) {

      return new WP_Error('username_length', 'Username too short. At least 4 characters is required');

    }


    if (strlen($this->password) < 5) {

      return new WP_Error('password', 'Password length must be greater than 5');

    }


    if (!is_email($this->email)) {

      return new WP_Error('email_invalid', 'Email is not valid');

    }


    if (email_exists($this->email)) {

      return new WP_Error('email', 'Email Already in use');

    }

  }

  function registration()

  {


    $userdata = array(

      'user_login' => esc_attr($this->username),

      'user_email' => esc_attr($this->email),

      'user_pass' => esc_attr($this->password)

    );


    if (is_wp_error($this->validation())) {

      echo '<div style="margin-bottom: 6px" class="btn btn-block btn-lg btn-danger">';

      echo '<strong>' . $this->validation()->get_error_message() . '</strong>';

      echo '</div>';

    } else {

      $register_user = wp_insert_user($userdata);

      if (!is_wp_error($register_user)) {


        echo '<div style="margin-bottom: 6px" class="btn btn-block btn-lg btn-danger">';

        echo '<strong>Registration complete.</strong>';

        echo '</div>';

      } else {

        echo '<div style="margin-bottom: 6px" class="btn btn-block btn-lg btn-danger">';

        echo '<strong>' . $register_user->get_error_message() . '</strong>';

        echo '</div>';

      }

    }


  }

  public function registration_form()
  {

    ?>

    <form method="post" action="<?php echo esc_url($_SERVER['REQUEST_URI']); ?>">

      <input name="reg_name" type="text" class=""

             value="<?php echo(isset($_REQUEST['reg_name']) ? $_REQUEST['reg_name'] : null); ?>"

             placeholder="USERNAME" id="reg-name">

      <input name="reg_email" type="email" class=""

             value="<?php echo(isset($_REQUEST['reg_email']) ? $_REQUEST['reg_email'] : null); ?>"

             placeholder="EMAIL" id="reg-email">

      <input name="reg_password" type="password" class=""

             value="<?php echo(isset($_REQUEST['reg_password']) ? $_REQUEST['reg_password'] : null); ?>"

             placeholder="PASSWORD" id="reg-pass">


      <input class="subscribe-sub-btn" type="submit" name="reg_submit" value="SUBSCRIBE"/>

    </form>

    <?php

  }

}

new cflrf_registration_form;


// custom frontend login form

function cflrf_login_form()
{

  ?>


  <form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>" class="">

    <input name="login_name" type="text" class="" value="" placeholder="Username" id="login-name"/>

    <input name="login_password" type="password" class="" value="" placeholder="Password" id="login-pass"/>

    <input class="login-sub-btn" id="al-login-front-end" type="submit" name="login_submit" value="Log in"/>

    <a href="" class="signup-link">SIGNUP</a>

  </form>

  <?php

}

function login_check($username, $password)
{

  global $user;

  $creds = array();

  $creds['user_login'] = $username;

  $creds['user_password'] = $password;

  $creds['remember'] = true;

  $user = wp_signon($creds, false);

  if (is_wp_error($user)) {

    echo $user->get_error_message();

  }

  if (!is_wp_error($user)) {

    wp_redirect(home_url('wp-admin'));

  }

}

function login_check_process()
{

  if (isset($_POST['login_submit'])) {

    login_check($_POST['login_name'], $_POST['login_password']);

  }


  cflrf_login_form();

}

function cflrf_login_shortcode()
{

  ob_start();

  login_check_process();

  return ob_get_clean();

}


add_shortcode('cflrf_custom_login_form', 'cflrf_login_shortcode');

// Custom Reset Pssword Form

function cflrf_reset_form()
{

  ?>


  <form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>" class="">

    <input name="login_name" type="text" class="" value="" placeholder="Username" id="login-name"/>

    <input class="login-sub-btn" id="al-login-front-end" type="submit" name="login_submit" value="Log in"/>

    <a href="" class="signup-link">SIGNUP</a>

  </form>

  <?php

}


add_action('wp_ajax_al_login_check', 'al_login_check');

add_action('wp_ajax_nopriv_al_login_check', 'al_login_check');

function al_login_check()
{

  global $user;
  $creds = array();
  $creds['user_login'] = $_POST['login_name'];
  $creds['user_password'] = $_POST['login_password'];
  $creds['remember'] = true;
  $user = wp_signon($creds, false);
  if (is_wp_error($user)) {
    echo 'error';
  } else {


    $sex = get_user_meta($user->ID, 'sex', true);

    if ($sex == 'man') {
      wp_logout();
      echo 'man';
      die;
    }

//    if(in_array('al_coach',$user->roles)){
//      echo 'coach';
//      die;
//    }
    echo 'exist';
  }
  die;
}


add_action('wp_footer', 'update_last_action_time');

function update_last_action_time()
{

  $user = wp_get_current_user();

  if ($user->ID !== 0) {

    update_user_meta($user->ID, 'last_login', date('Y-m-d'));

  }

}


add_filter('the_excerpt', 'al_excerpt_function');
function al_excerpt_function($excerpt)
{
  $length = strlen($excerpt);
  return $length > 150 ? substr($excerpt, 0, 150) . '...' : $excerpt;
}


add_action('wp_ajax_al_user_quiz', 'al_user_quiz');

function al_user_quiz()
{
  if (isset($_POST)) {
    $key = $_POST['quest'];
    $val = strtolower(str_replace(' ', '_', $_POST['answer']));

    $user = wp_get_current_user();
    if ($user->ID !== 0) {
      $res = get_user_meta($user->ID, 'quiz', true);
      if ($res) {
        $old_val = unserialize($res);
        if ($old_val[$key] == $val) {
          echo 'done';
          die;
        }
        $old_val[$key] = $val;
        $new_val = serialize($old_val);
      } else {
        $new_val = serialize(array($key => $val));
      }
      $res = update_user_meta($user->ID, 'quiz', $new_val);
      echo $res ? 'done' : 'error';

    }
  }
  die;
}


add_action('wp_ajax_al_personality_quiz', 'al_personality_quiz');
//add_action('wp_ajax_nopriv_al_personality_quiz', 'al_personality_quiz');

function al_personality_quiz()
{

  if (isset($_POST)) {

    $user = wp_get_current_user();

    if ($user->ID !== 0) {

      $answ = $_POST['data'];

//      echo '<pre>';
//      print_r($answ);
//      echo '</pre>';;
//      $IE = 30 - $answ['Q3'] - $answ['Q7'] - $answ['Q11'] + $answ['Q15'] - $answ['Q19'] + $answ['Q23'] + $answ['Q27'] - $answ['Q31'];
//      $SN = 12 + $answ['Q4'] + $answ['Q8'] + $answ['Q12'] + $answ['Q16'] + $answ['Q20'] - $answ['Q24'] - $answ['Q28'] + $answ['Q32'];
//      $FT = 30 - $answ['Q2'] + $answ['Q6'] + $answ['Q10'] - $answ['Q14'] - $answ['Q18'] + $answ['Q22'] - $answ['Q26'] - $answ['Q30'];
//      $JP = 18 + $answ['Q1'] + $answ['Q5'] - $answ['Q9'] + $answ['Q13'] - $answ['Q17'] + $answ['Q21'] - $answ['Q25'] + $answ['Q29'];
//      var_dump($IE, $SN, $FT, $JP);
//      die;

      $IE = 30 - $answ['Q3'] - $answ['Q7'] - $answ['Q11'] + $answ['Q15'] - $answ['Q19'] + $answ['Q23'] + $answ['Q27'] - $answ['Q31'] > 24 ? 'E' : 'I';

      $SN = 12 + $answ['Q4'] + $answ['Q8'] + $answ['Q12'] + $answ['Q16'] + $answ['Q20'] - $answ['Q24'] - $answ['Q28'] + $answ['Q32'] > 24 ? 'N' : 'S';

      $FT = 30 - $answ['Q2'] + $answ['Q6'] + $answ['Q10'] - $answ['Q14'] - $answ['Q18'] + $answ['Q22'] - $answ['Q26'] - $answ['Q30'] > 24 ? 'T' : 'F';

      $JP = 18 + $answ['Q1'] + $answ['Q5'] - $answ['Q9'] + $answ['Q13'] - $answ['Q17'] + $answ['Q21'] - $answ['Q25'] + $answ['Q29'] > 24 ? 'P' : 'J';

      $personality_res = $IE . $SN . $FT . $JP;

      $res = get_user_meta($user->ID, 'quiz', true);

      if ($res) {

        $old_val = unserialize($res);

        if ($old_val['user_personality'] == $personality_res) {
          add_date_to_quiz($user->ID);
          add_new_messages_into_user_inbox($user->ID, true);
          al_create_selfie_badge($user->ID);
          echo 'done';
          die;
        }

        $old_val['user_personality'] = $personality_res;
        $new_val = serialize($old_val);

      } else {
        $new_val = serialize(array('user_personality' => $personality_res));
      }

      $update = update_user_meta($user->ID, 'quiz', $new_val);
      add_date_to_quiz($user->ID);
      add_new_messages_into_user_inbox($user->ID, true);
      al_create_selfie_badge($user->ID);

      echo $update ? 'done' : 'error';

    }

    die;

  }

}

function al_create_selfie_badge($user_id)
{
  if (!$user_id) return false;

  $user = get_user_by('id', $user_id);

  if ($user === false) return false;

  $user_quiz = get_user_meta($user_id, 'quiz', true);

  if (!$user_quiz) return false;


  $user_quiz = unserialize($user_quiz);
  if (!isset($user_quiz['date'])) return false;
  $image = imagecreatetruecolor(800, 800);
  $destination_path = wp_upload_dir();
  $destination_path = $destination_path['basedir'] . '/users_selfie_badges/' . $user_id;

  if (!file_exists($destination_path)) {
    mkdir($destination_path, 0777, true);
  }
  $files = glob($destination_path . '/*');
  foreach ($files as $file) {
    if (is_file($file))
      unlink($file);
  }

  foreach ($user_quiz as $k => $v) {
    $k = lower_underline($k);
    $v = lower_underline($v);
    $path = __DIR__ . "/img/selfie_badge_source/$k" . "_" . "$v.png";
    if (file_exists($path)) {
      $img_doorStyle = imagecreatefrompng($path);
      switch ($k) {
        case 'fashion':
          imagecopyresampled($image, $img_doorStyle, 0, 240, 0, 0, 270, 240, 270, 240);
          break;
        case 'body_type':
          imagecopyresampled($image, $img_doorStyle, 270, 240, 0, 0, 160, 310, 160, 310);
          break;
        case 'relationships':
          imagecopyresampled($image, $img_doorStyle, 270, 550, 0, 0, 210, 250, 210, 250);
          break;
        case 'fitness':
          imagecopyresampled($image, $img_doorStyle, 0, 480, 0, 0, 270, 320, 270, 320);
          break;
        case 'interior_design':
          imagecopyresampled($image, $img_doorStyle, 480, 550, 0, 0, 320, 250, 320, 250);
          break;
        case 'hair_type':
          imagecopyresampled($image, $img_doorStyle, 430, 240, 0, 0, 370, 175, 370, 175);
          break;
        case 'user_personality':
          imagecopyresampled($image, $img_doorStyle, 0, 0, 0, 0, 800, 240, 800, 240);
          break;
        case 'skin_tone':
          imagecopyresampled($image, $img_doorStyle, 430, 415, 0, 0, 370, 135, 370, 135);
          break;
        default:

          break;
      }
    }
  }
//  $res = imagecopyresampled($image, $img_doorStyle,0,0,  0,0,  800,240,800,240);

//  imagealphablending($image,true);
  return imagepng($image, $destination_path . '/' . time() . ".png", 1);
//  if(imagepng($image, $destination_path. '/' . time() . ".png", 1)){
//     true;
//  }
//die;
}

function lower_underline($str)
{
  return str_replace('-', '_', strtolower($str));
}

add_action('wp_ajax_al_save_to_favourite', 'al_save_to_favourite');

function al_save_to_favourite()
{

  if (isset($_POST)) {

    $message_id = (int)$_POST['message_id'];

    if (!$message_id)
      __json('no ID');

    $user = wp_get_current_user();
//    $delete = delete_user_meta($user->ID, 'saved_messages', false);
//    var_dump($delete);
//    die;

    if ($user->ID !== 0) {

      $res = get_user_meta($user->ID, 'saved_messages', true);

      if ($res) {

        if (in_array($message_id, $res)) {

          $new_val = array_diff($res, array($message_id));

        } else {

          $res[] = $message_id;

          $new_val = $res;

        }

      } else {


        $new_val = array($message_id);

      }

      $update = update_user_meta($user->ID, 'saved_messages', $new_val);

      echo $update ? 'done' : 'error';

    }

    die;

  }

}


add_action('wp_ajax_al_accept_challenge', 'al_accept_challenge');

function al_accept_challenge()
{

  if (isset($_POST)) {

    $message_id = (int)$_POST['message_id'];
    if (!$message_id)
      __json('no ID');
    $user = wp_get_current_user();

//    delete_user_meta($user->ID, 'accepted_challenges', false);


    if ($user->ID !== 0) {

      $date_added = strtotime(date("Y") . '-' . date("m") . '-' . date("d"));

      $old_val = get_user_meta($user->ID, 'accepted_challenges', true);

      $action = '';


      if ($old_val) {

        $old_val = unserialize($old_val);

        if (array_key_exists($message_id, $old_val)) {

          $action = 'deleted';

          unset($old_val[$message_id]);

          $new_val = $old_val;

        } else {

          $action = 'added';

          $new_val = $old_val;

          $new_val[$message_id] = $date_added;

          al_add_viewed_challenge($user->ID, $message_id);

        }

      } else {

        $action = 'added';

        $new_val = array($message_id => $date_added);

      }

      $update = update_user_meta($user->ID, 'accepted_challenges', serialize($new_val));

      echo $update ? $action : 'error';


    }

    die;

  }

}


add_action('wp_ajax_al_complete_challenge', 'al_complete_challenge');

function al_complete_challenge()
{

  if (isset($_POST)) {
    $message_id = (int)$_POST['message_id'];
    $user = wp_get_current_user();
    $points_challenge = get_post_meta($message_id, 'points', true);

    $accepted_cahllenges = get_user_meta($user->ID, 'accepted_challenges', true);
    $accepted_cahllenges = $accepted_cahllenges ? array_keys(unserialize($accepted_cahllenges)) : array();

    if (!in_array($message_id, $accepted_cahllenges)) {
      echo 'not accepted';
      die;
    }

    if ($user->ID !== 0) {
      $date_added = strtotime(date("Y") . '-' . date("m") . '-' . date("d"));
      $old_val = get_user_meta($user->ID, 'completed_challenges', true);
      $action = '';

      if ($old_val) {
        $old_val = unserialize($old_val);
        if (array_key_exists($message_id, $old_val)) {
          // no deleting completed challenge (29.09.2016)
//          $action = 'deleted';
//          unset($old_val[$message_id]);
//          $new_val = $old_val;
//          al_del_viewed_challenge($user->ID,$message_id);
          echo 'already';
          die;
        } else {
          al_add_viewed_challenge($user->ID, $message_id);
          al_add_points_user($user->ID, $points_challenge);
          $action = 'added';
          $new_val = $old_val;
          $new_val[$message_id] = $date_added;
        }
      } else {
        al_add_points_user($user->ID, $points_challenge);
        $action = 'added';
        $new_val = array($message_id => $date_added);
      }

      $update = update_user_meta($user->ID, 'completed_challenges', serialize($new_val));

      if ($update) {
        add_delete_accepted_challenge($user->ID, $message_id);
        echo $action;
      } else {
        echo 'error';
      }

    }

    die;

  }
}

function al_add_points_user($user_id, $points)
{

  if (!$points) {
    return false;
  }
  if (!$user_id) {
    return false;
  }
  $user = get_userdata($user_id);
  if ($user === false) {
    return false;
  }

  $points_user = get_user_meta($user_id, 'points', true);
  $entries_user = get_user_meta($user_id, 'entries', true);

//var_dump($points_user, $entries_user);
//  die;
  $summ = intval($points_user) + $points;

  if ($summ >= 100) {
    $entries_user = intval($entries_user) + 10;
    $summ -= 100;
  }

  $res1 = update_user_meta($user_id, 'points', $summ);
  $res2 = update_user_meta($user_id, 'entries', $entries_user);

  return $res1 && $res2;
}

function get_user_points_array()
{

  $user = wp_get_current_user();


  $points_user = get_user_meta($user->ID, 'points', true);
  $entries_user = get_user_meta($user->ID, 'entries', true);
  $percentage = get_challenge_percentage($user->ID);
  $percentage = $percentage ? $percentage: 0;

  return array('points' => $points_user, 'entries' => $entries_user, 'percentage' => $percentage);
}


add_action('wp_ajax_al_get_user_points', 'al_get_user_points');

function al_get_user_points()
{
  $arr = get_user_points_array();
  if ($arr) {
    echo json_encode($arr);
    die;
  }
  echo json_encode('error');
  die;
}

function decrease_entry_user($user_id)
{
  $entry = get_user_meta($user_id, 'entries', true);

  if (!$entry)
    return false;

  if ($entry < 1)
    return false;

  $entry = intval($entry);

  $new_val = $entry - 1;
  $res = update_user_meta($user_id, 'entries', $new_val);
  return $res ? $new_val : 'error';
}

//al_add_points_user(1,1);

function add_delete_accepted_challenge($user_id, $challenge_id)
{

  $accepted_challenges_old = get_user_meta($user_id, 'accepted_challenges', true);
  if ($accepted_challenges_old) {
    $accepted_challenges_old = unserialize($accepted_challenges_old);
    $date_added = strtotime(date("Y") . '-' . date("m") . '-' . date("d"));

//var_dump($accepted_challenges_old);
    if (array_key_exists($challenge_id, $accepted_challenges_old)) {
      unset($accepted_challenges_old[$challenge_id]);
      $new_val = $accepted_challenges_old;
    } else {

      $new_val = $accepted_challenges_old;
      $new_val[$challenge_id] = $date_added;
    }
    update_user_meta($user_id, 'accepted_challenges', serialize($new_val));
  }

}


add_action('wp_ajax_al_add_self_goal', 'al_add_self_goal');

function al_add_self_goal()
{

  if (isset($_POST)) {
    if (!$_POST['goal']) {
      echo json_encode(array('status' => 'no goal'));
      die;
    }
    $category = strtolower($_POST['category']);
    if (!$category) {
      echo json_encode(array('status' => 'no category'));
      die;
    }
    $goal = strip_tags($_POST['goal']);

    $update = $goal_id = false;
    if (isset($_POST['update']) && $_POST['update'] = 'update') {
      $update = 'update';
    }


    if (isset($_POST['id']) && intval($_POST['id']) != 0) {
      $goal_id = intval($_POST['id']);
    }

    add_self_goal($goal, $category, $update);
  }
}

function add_self_goal($goal, $category, $update = false)
{
  $user = wp_get_current_user();
  if ($user->ID !== 0) {
//    delete_user_meta($user->ID, 'self_goals');
    $old_val = get_user_meta($user->ID, 'self_goals', true);

    if (!$old_val) {
      $old_val = array();
    } else {
      $old_val = unserialize($old_val);
    }

    $new_val = $old_val;
    $text = trim($goal);

    foreach ($new_val as $k => $v) {
      if (isset($v['category']) && $v['category'] == $category) {
        if (!$update) {
          echo json_encode(array('status' => 'goal already exist'));
          die;
        } else {
          $new_val[$k]['text'] = $text;

          $res = update_user_meta($user->ID, 'self_goals', serialize($new_val));

          echo $res ? json_encode(array('status' => 'done', 'ID' => $k)) : json_encode(array('status' => false));
          die;
        }
        break;
      }
    }


    $new_val[] = array('category' => $category, 'text' => $text, 'status' => '0');

    end($new_val);
    $added_last_key = key($new_val);
    $res = update_user_meta($user->ID, 'self_goals', serialize($new_val));

    echo $res ? json_encode(array('status' => 'done', 'ID' => $added_last_key)) : json_encode(array('status' => false));
    die;
  }
}

// change status of self-goals

add_action('wp_ajax_al_status_self_goal', 'al_status_self_goal');

function al_status_self_goal()
{

  if (isset($_POST)) {
    $key = (int)$_POST['id'];

    echo change_self_goal_status($key);
    die;
  }
}

function change_self_goal_status($sg_id)
{
  $user = wp_get_current_user();
  if ($user->ID === 0) {
    return 'incorrect user';
  }
  if ($sg_id === false) {
    return 'no self goal id';
  }

  $key = $sg_id;
  $old_val = get_user_meta($user->ID, 'self_goals', true);

  if (!$old_val) {
    return 'no self goals yet';
  } else {
    $old_val = unserialize($old_val);
    if (isset($old_val[$key])) {

      unset($old_val[$key]);
//      if($old_val[$key]['status'] == 0){
//        $old_val[$key]['status'] = 1;
//        $action = 'completed';
//      }else{
//        return 'self goal is already completed';
//      }
      $points_self_goal = get_option('selfgoal_value', true);
      $points_self_goal = $points_self_goal === true ? 50 : $points_self_goal;
      al_add_points_user($user->ID, $points_self_goal);

      $res = update_user_meta($user->ID, 'self_goals', serialize($old_val));


      return $res ? 'completed' : 'error';
    }
  }
}

// delete self-goals

add_action('wp_ajax_al_delete_self_goal', 'al_delete_self_goal');

function al_delete_self_goal()
{

  if (isset($_POST)) {

    $user = wp_get_current_user();

    $key = (int)$_POST['id'];

    if ($user->ID !== 0) {

      $old_val = get_user_meta($user->ID, 'self_goals', true);


      if (!$old_val) {

        die;

      } else {

        $old_val = unserialize($old_val);

        if (isset($old_val[$key])) {

          unset($old_val[$key]);

        }


        $res = update_user_meta($user->ID, 'self_goals', serialize($old_val));

        echo $res ? 'done' : 'error';

        die;

      }

    }

  }

}


add_action('wp_ajax_al_save_viewed_history', 'al_save_viewed_history');

function al_save_viewed_history()
{

  if (isset($_POST)) {

    $message_id = (int)$_POST['message_id'];


    if ($_POST['type'] == 'challenge' || $_POST['type'] == 'message') {

      $message_type = $_POST['type'];

    } else {

      return false;

    }


    $user = wp_get_current_user();


    if ($user->ID !== 0) {

// $delete = delete_user_meta($user->ID, 'viewed_challenges', false);

//    var_dump($delete);

//    die;

//      if($message_type == 'challenge'){
//
//        // it's a challenge
//
//        al_add_viewed_challenge($user->ID,$message_id);
//
//      }else{

      // it's a message

      $old_val = get_user_meta($user->ID, 'viewed_messages', true);

      if (!empty($old_val)) {

        if (!in_array($message_id, $old_val)) {

          $new_val = $old_val;

          $new_val[] = $message_id;

        } else {

          die;

        }

      } else {

        $new_val = array($message_id);

      }

      update_user_meta($user->ID, 'viewed_messages', $new_val);

//      }

    }

    die;

  }

}


function al_add_viewed_challenge($user_id, $challenge_id)
{

  $old_val = get_user_meta($user_id, 'viewed_challenges', true);

//  echo 'adding! Old value = ';

//  var_dump($old_val);

  if (!empty($old_val)) {

    if (!in_array($challenge_id, $old_val)) {

      $new_val = $old_val;

      $new_val[] = $challenge_id;

    }

  } else {

    $new_val = array($challenge_id);

  }

//  echo 'Want to add - ';

//  var_dump($new_val);

  update_user_meta($user_id, 'viewed_challenges', $new_val);


//echo 'efter adding = ';

//var_dump(get_user_meta($user_id, 'viewed_challenges', true));

}


function al_del_viewed_challenge($user_id, $challenge_id)
{

  $old_val = get_user_meta($user_id, 'viewed_challenges', true);

  if (!empty($old_val)) {

    if (in_array($challenge_id, $old_val)) {

      $new_val = array_diff($old_val, array($challenge_id));

    }

  }

  update_user_meta($user_id, 'viewed_challenges', $new_val);

}


// need to add these action for ajax

add_action('wp_ajax_nopriv_delete_avatar', 'delete_avatar');

add_action('wp_ajax_delete_avatar', 'delete_avatar');

function delete_avatar()
{

  $uid = $_POST['uid'];

  $nonce = $_POST['nonce'];

  $current_user = wp_get_current_user();


  // If user clicks the remove avatar button, in URL deleter_avatar=true

  if (isset($uid) && wp_verify_nonce($nonce, 'delete_user_avatar') && $uid == $current_user->id) {

    user_avatar_delete_files($uid);//here I using user avatar own plugin delete photo function.


  }


  echo '1';//sending back to the js script to notify that the photo is deleted.

  exit;

}


// STRIPE

//require 'library/stripe/init.php';

//

//add_action( 'wp_ajax_al_stripe_payment', 'al_stripe_payment' );

//function al_stripe_payment(){

//

//  \Stripe\Stripe::setApiKey("sk_test_kr0tcvgpffIEiI6jKcIU5eSj");

//  $customer = \Stripe\Customer::create(array("email" => $_POST['email'], "description" => "Subscription Customer", "source" => $_POST['stripeToken'], "metadata" => array("user_id" => $_POST['user_id'])));

//

//

//  $subscription = \Stripe\Subscription::create(array("customer" => $customer->id, "plan" => "ycweb_day", 'metadata' => array('user_id' => $_POST['user_id'])));

//

//  echo 'done';

//  die;


//  Stripe::setApiKey("sk_test_kr0tcvgpffIEiI6jKcIU5eSj");

//  Stripe_Customer::create(array(

//    "source" => $_POST['stripeToken'],

//    "plan" => "ycweb_day",

//    "email" => $_POST['email'],

//    "metadata" => array('user_id' => $_POST['user_id'])

//    ));

//echo 'done';

//  die;

//}


//add_action( 'wp_ajax_al_cancel_subscription', 'al_cancel_subscription' );

//function al_cancel_subscription(){

// $user = wp_get_current_user();

//  $subscription_id = get_user_meta($user->ID, 'subscription_stripe', true);

//  $subscription_customer = get_user_meta($user->ID, 'subscription_customer_stripe', true);

//

//  \Stripe\Stripe::setApiKey("sk_test_kr0tcvgpffIEiI6jKcIU5eSj");

//  $sub = \Stripe\Subscription::retrieve($subscription_id);

//  var_dump($sub);echo '<br>';

//  $res = $sub->cancel();

//  var_dump($res);

//if($res){

//  update_user_meta($user->ID, 'subscribe_canceled', 0);

//

//echo 'done';

//}

//die;

//}


//add_action( 'rest_api_init', function () {

//register_rest_route( 'stripe', '/webhook', array(

//  'methods' => 'POST',

//  'callback' => 'takes',

//  ));

//});

//

//

//if (!function_exists('http_response_code')) {

//  function http_response_code($code = NULL) {

//      if ($code !== NULL) {

//          switch ($code) {

//

//              case 200: $text = 'OK'; break;

//              case 201: $text = 'Created'; break;

//              case 202: $text = 'Accepted'; break;

//              case 203: $text = 'Non-Authoritative Information'; break;

//              case 204: $text = 'No Content'; break;

//              case 205: $text = 'Reset Content'; break;

//              case 206: $text = 'Partial Content'; break;

//              case 300: $text = 'Multiple Choices'; break;

//              case 301: $text = 'Moved Permanently'; break;

//              case 302: $text = 'Moved Temporarily'; break;

//              case 303: $text = 'See Other'; break;

//              case 304: $text = 'Not Modified'; break;

//              case 305: $text = 'Use Proxy'; break;

//              case 400: $text = 'Bad Request'; break;

//              case 401: $text = 'Unauthorized'; break;

//              case 402: $text = 'Payment Required'; break;

//              case 403: $text = 'Forbidden'; break;

//              case 404: $text = 'Not Found'; break;

//              case 405: $text = 'Method Not Allowed'; break;

//              case 406: $text = 'Not Acceptable'; break;

//              case 407: $text = 'Proxy Authentication Required'; break;

//              case 408: $text = 'Request Time-out'; break;

//              case 409: $text = 'Conflict'; break;

//              case 410: $text = 'Gone'; break;

//              case 411: $text = 'Length Required'; break;

//              case 412: $text = 'Precondition Failed'; break;

//              case 413: $text = 'Request Entity Too Large'; break;

//              case 414: $text = 'Request-URI Too Large'; break;

//              case 415: $text = 'Unsupported Media Type'; break;

//              case 500: $text = 'Internal Server Error'; break;

//              case 501: $text = 'Not Implemented'; break;

//              case 502: $text = 'Bad Gateway'; break;

//              case 503: $text = 'Service Unavailable'; break;

//              case 504: $text = 'Gateway Time-out'; break;

//              case 505: $text = 'HTTP Version not supported'; break;

//              default:

//                  exit('Unknown http status code "' . htmlentities($code) . '"');

//              break;

//          }

//

//          $protocol = (isset($_SERVER['SERVER_PROTOCOL']) ? $_SERVER['SERVER_PROTOCOL'] : 'HTTP/1.0');

//

//          header($protocol . ' ' . $code . ' ' . $text);

//

//          $GLOBALS['http_response_code'] = $code;

//

//      } else {

//

//          $code = (isset($GLOBALS['http_response_code']) ? $GLOBALS['http_response_code'] : 200);

//

//      }

//

//      return $code;

//

//  }

//}

//

//

//function takes( WP_REST_Request $request ){

//

//  http_response_code(200);

//  $input = file_get_contents("php://input");

//  $event_json = json_decode($input, true);

//

//  ob_start();

//  var_dump($event_json['type']);echo '<br>';

//  if($event_json['type'] === 'customer.subscription.created'){

//    $user_id = $event_json['data']['object']['metadata']['user_id'];

//

//    $subscripition_id = $event_json['data']['object']['id'];

//    $current_period_end = $event_json['data']['object']['current_period_end'];

//    $subscripition_customer = $event_json['data']['object']['customer'];

//    echo 'Creating subs.'; echo '<br>';

//    var_dump($user_id);echo '<br>';

//    var_dump($subscripition_id);echo '<br>';

//    var_dump($current_period_end);echo '<br>';

//    var_dump($subscripition_customer);echo '<br>';

//

//

//    update_user_meta($user_id, 'subscription_stripe', $subscripition_id);

//    update_user_meta($user_id, 'current_period_end', $current_period_end);

//    update_user_meta($user_id, 'subscription_customer_stripe', $subscripition_customer);

//    update_user_meta($user_id, 'subscribe_canceled', 1);

//

//  //  echo 'subscription_stripe'; var_dump( $subscripition_id );

//  //  echo 'current_period_end'; var_dump( $current_period_end );

//  //  echo 'subscription_customer_stripe'; var_dump( $subscripition_customer );

//

//

//  }elseif($event_json['type'] === 'customer.subscription.updated'){

//    // update subscription

//    $user_id = $event_json['data']['object']['metadata']['user_id'];

//    $current_period_end = $event_json['data']['object']['current_period_end'];

//    echo 'User id'; echo '<br>';

//    var_dump($user_id); echo '<br>';echo '<br>';

//    echo 'Period end = '; echo '<br>';

//    var_dump($current_period_end);

//

//    update_user_meta($user_id, 'current_period_end', $current_period_end);

//  }

//

//  $res = ob_get_contents();

//  file_put_contents(__DIR__ .'/new_stripe_responce.txt', $res);

//  die;

//

//}


function al_user_has_subscription()
{
//return false;
  if (is_user_logged_in()) {
    // if admin
    if (current_user_can('manage_options'))
      return true;

    $user = wp_get_current_user();
    // if no subscription at all
//    var_dump(pmpro_getMembershipLevelForUser($user->ID));
    $user_level = pmpro_getMembershipLevelForUser($user->ID);
    if (!$user_level) {
      
      $trial_time = get_option('al_trial_time');
      $var = array(
        'no_trial' => '',
        'week' => '+1 week',
        'month' => '+1 month',
      );
      if (time() > strtotime($var[$trial_time], strtotime($user->user_registered))) {
        return false;
      } else {
        return true;
      }

    }else{
      return true;
    }
  }
  return false;
}


function al_dashboard_subscription()
{
  if (current_user_can('manage_options'))
    wp_add_dashboard_widget('al_dashboard_subscription', 'Subscription settings', 'al_dashboard_subscription_widget');
}


function al_dashboard_subscription_widget()
{ ?>

  Select trial time

  <form action="" id="al-trial-time-form">

    <?php $trial_time = get_option('al_trial_time'); ?>
    <p><input type="radio" name="trial_time" <?php checked('no_trial', $trial_time, true) ?> value="no_trial"> no
      trial</p>
    <p><input type="radio" name="trial_time" <?php checked('week', $trial_time, true) ?> value="week"> week</p>
    <p><input type="radio" name="trial_time" <?php checked('month', $trial_time, true) ?> value="month"> month</p>

    <button class="button button-primary" id="al-trial-time-save">Update</button>
  </form>
<?php }

add_action('wp_dashboard_setup', 'al_dashboard_subscription');


// ajax function add category to selected date

add_action('wp_ajax_al_update_trial_time', 'al_update_trial_time');

function al_update_trial_time()
{

  if (isset($_POST)) {

    if ($_POST['trial_time'] == 'no_trial' || $_POST['trial_time'] == 'week' || $_POST['trial_time'] == 'month') {

      $trial_time = $_POST['trial_time'];

      $res = update_option('al_trial_time', $trial_time);

      echo $res ? 'done' : 'error';

      die;

    } else {

      wp_die('cheating?)');

    }

  }

  die;

}

// ajax saving profile

add_action('wp_ajax_save_profile_front', 'save_profile_front');

function save_profile_front()
{

  if (isset($_POST)) {

    $user = wp_get_current_user();


    /* Update user password. */

    if (!empty($_POST['pass1']) && !empty($_POST['pass2'])) {

      if ($_POST['pass1'] == $_POST['pass2']) {

        wp_update_user(array('ID' => $user->ID, 'user_pass' => esc_attr($_POST['pass1'])));

      } else {

        echo 'Passwords do not match';

      }

    }


    /* Update user information. */

    if (!empty($_POST['email'])) {

      if (!is_email(esc_attr($_POST['email']))) {

        echo 'incorrect email';

        die;

      } elseif (email_exists(esc_attr($_POST['email'])) && $user->ID !== email_exists(esc_attr($_POST['email']))) {

        echo 'email is used by another user';

        die;

      } else {

        wp_update_user(array('ID' => $user->ID, 'user_email' => esc_attr($_POST['email'])));

      }

    }


    if (!empty($_POST['first_name']))

      update_user_meta($user->ID, 'first_name', esc_attr($_POST['first_name']));

    if (!empty($_POST['last_name']))

      update_user_meta($user->ID, 'last_name', esc_attr($_POST['last_name']));


    echo 'done';

    die;

  }

}

// ajax saving profile

add_action('wp_ajax_nopriv_al_registration_front', 'al_registration_front');

function al_registration_front()
{

  if (isset($_POST)):


    if (empty($_POST['user_name'])) {

      echo 'Please enter name.';
      die;

    }

    if (empty($_POST['email'])) {

      echo 'Please enter a email.';
      die;

    }

    if (empty($_POST['pass1'])) {

      echo 'Please enter a password.';
      die;

    }

    if (empty($_POST['pass2'])) {

      echo 'Please enter a confirm password.';
      die;

    }

    if ((!empty($_POST['pass2']) && !empty($_POST['pass1'])) && ($_POST['pass1'] != $_POST['pass2'])) {

      echo 'Entered password did not match.';
      die;

    }


    $user_login = esc_attr($_POST['user_name']);

    $user_email = esc_attr($_POST['email']);

    $user_pass = esc_attr($_POST['pass1']);


    $sanitized_user_login = sanitize_user($user_login);

    $user_email = apply_filters('user_registration_email', $user_email);


    if (!is_email($user_email)) {

      echo 'Invalid e-mail.';
      die;

    } elseif (email_exists($user_email)) {

      echo 'This email is already registered.';
      die;

    }


    if (empty($sanitized_user_login) || !validate_username($user_login)) {

      echo 'Invalid user name.';
      die;

    } elseif (username_exists($sanitized_user_login)) {

      echo 'User name already exists.';
      die;

    }


    $user_id = wp_create_user($sanitized_user_login, $user_pass, $user_email);


    if (!$user_id) {

      echo 'Registration failed';
      die;

    } else {

      update_user_option($user_id, 'default_password_nag', true, true);

      wp_new_user_notification($user_id, $user_pass);

      wp_cache_delete($user_id, 'users');

      wp_cache_delete($user_login, 'userlogins');

      do_action('user_register', $user_id);

      $user_data = get_userdata($user_id);

      if ($user_data !== false) {

        wp_clear_auth_cookie();

        wp_set_auth_cookie($user_data->ID, true);

        do_action('wp_login', $user_data->user_login, $user_data);

        // Redirect user.

        echo 'done';

        exit();

      }

    }


  endif;

  die;

}


if (!function_exists('array_orderby')) {

  function array_orderby()
  {

    $args = func_get_args();

    $data = array_shift($args);

    if (!is_array($data)) {

      return array();

    }

    $multisort_params = array();

    foreach ($args as $n => $field) {

      if (is_string($field)) {

        $tmp = array();

        foreach ($data as $row) {

          $tmp[] = $row[$field];

        }

        $args[$n] = $tmp;

      }

      $multisort_params[] = &$args[$n];

    }

    $multisort_params[] = &$data;

    call_user_func_array('array_multisort', $multisort_params);

    return end($multisort_params);

  }

}


//  add_filter('pmpro_login_redirect_url', 'redirect_if_no_subscription', 15, 3);

function redirect_if_no_subscription($redirect_to, $request, $user)
{


  return bloginfo('url');

}


if (!current_user_can('manage_options')) {

  show_admin_bar(false);

}


add_filter('wp_revisions_to_keep', 'al_revisions_to_keep', 10, 2);


function al_revisions_to_keep($num, $post)
{

  if ('message' == $post->post_type) {

    $num = 5;

  }

  return $num;

}


add_filter('pre_get_posts', 'posts_for_current_author');

function posts_for_current_author($query)
{

  if ($query->is_admin && !current_user_can('manage_options')) {
    if (isset($_GET['post_type']) && $_GET['post_type'] != 'message') {
      return $query;
    }


    if (isset($_POST['action']) && $_POST['action'] == 'query-attachments') {
      return $query;
    }

    if (isset($_GET['type']) && $_GET['type'] == 'image') {
      return $query;
    }

    global $user_ID;
    if (isset($_GET['author']) && $_GET['author'] == $user_ID) {
      return $query;
    }


    $args = array('hide_empty' => false, 'meta_query' => array(array('taxonomy' => 'campaign', 'key' => 'coach', 'value' => $user_ID, 'compare' => '=',),), 'suppress_filters' => false,);
    $terms = get_terms($args);

    $all_terms_ids = array();

    if (!empty($terms)) {
      foreach ($terms as $term) {
        $all_terms_ids[] = $term->term_id;
      }
    }


    $query->set('tax_query', array(
      array(
        'taxonomy' => 'campaign',
        'field' => 'id',
        'terms' => $all_terms_ids,
        'operator' => 'IN'
      )));
  }


  return $query;

}

//add_action('save_post', function(){

//  if(!current_user_can('manage_options') && isset($_GET['post'])){

//    $post = intval($_GET['post']);

//

//    $isset_post  = get_post($post);

//    if(is_null($isset_post))

//      return true;

//    global $user_ID;

//    $args = array('hide_empty' => false, 'meta_query' => array(array('taxonomy' => 'campaign', 'key' => 'coach', 'value' => $user_ID, 'compare' => '=',),), 'suppress_filters' => false,);

//    $terms = get_terms($args);

//

//    $all_terms_ids = array();

//

//    if(!empty($terms)){

//      foreach ($terms as $term) {

//        $all_terms_ids[] = $term->term_id;

//      }

//    }

////var_dump($all_terms_ids);

////    echo '<br>';

////    echo '<br>';

//    $args = array(

//      'post_type' => 'message',

//      'posts_per_page' => -1,

//      'order' => 'ASC',

//      'tax_query' => array(

//        array(

//          'taxonomy' => 'campaign',

//          'field' => 'id',

//          'terms' => $all_terms_ids,

//          'operator' => 'IN'

//        ),

//      )

//    );

//

//    $query = new WP_Query($args);

//    $this_caoach_messages = array();

//    if ($query->have_posts()) {

//      while ( $query->have_posts() ) {

//        $query->the_post();

//        $this_caoach_messages[] = get_the_ID();

//      }

//    }

////var_dump($this_caoach_messages);

//    if(!in_array($post, $this_caoach_messages)){

//      wp_die('cheating?)');

//    }

//  }

//});


// *** Rewards ***

// Add custom post type

add_action('init', 'create_post_type_rewards');

function create_post_type_rewards()
{

  register_post_type('rewards',

    array(
      'labels' => array(
        'name' => __('Rewards'),
        'singular_name' => __('Rewards'),
      ),
      'supports' => array(
        'title',
        'editor',
        'excerpt',
        'thumbnail',
        'page-attributes',
        'revisions'
      ),
      'public' => false,  // it's not public, it shouldn't have it's own permalink, and so on
      'publicly_queriable' => true,  // you should be able to query it
      'show_ui' => true,  // you should be able to edit it in wp-admin
      'exclude_from_search' => true,  // you should exclude it from search results
      'show_in_nav_menus' => false,  // you shouldn't be able to add it to menus
      'has_archive' => false,  // it shouldn't have archive page
      'rewrite' => false,  // it shouldn't have rewrite rules
      'menu_icon' => 'dashicons-smiley',
    )
  );
}

// Add column

//function add_members_column( $columns ) {
//  return array_merge( $columns,
//    array(
//      'members' => __('Members')
//    )
//  );
//}
//
//add_filter( 'manage_rewards_posts_columns' ,'add_members_column' );


//function custom_columns( $column, $post_id ){
//  if ( $column === 'members' ) {
//    $members = get_post_meta( $post_id, 'members', true );
//    echo ( ! empty( $members ) ) ? $members : 0;
//  }
//}
//
//add_action( 'manage_rewards_posts_custom_column', 'custom_columns', 10, 2 );
// Add meta box to set image

function add_reward_image_meta_box()
{
  add_meta_box('reward-image', __('Reward Image'), 'book_image_meta_box', 'rewards', 'normal', 'high');
  add_meta_box('reward-start', __('Start'), 'reward_start_meta', 'rewards', 'side');
  add_meta_box('reward-end', __('End'), 'reward_end_meta', 'rewards', 'side');

}

add_action('add_meta_boxes', 'add_reward_image_meta_box');

function reward_start_meta()
{
  global $post;

  $reward_start = get_post_meta($post->ID, 'start', true);
  ?>
  <input type="text" name="reward-start" class="widefat al-reward-date" value="<?php echo $reward_start; ?>"/>
  <?php
}

function reward_end_meta()
{
  global $post;
  $reward_end = get_post_meta($post->ID, 'end', true);
  ?>
  <input type="text" name="reward-end" class="widefat al-reward-date" value="<?php echo $reward_end; ?>"/>
  <?php
}

// Save image
add_action('save_post', 'save_reward_meta_boxes', 1, 2);
function save_reward_meta_boxes($post_id, $post)
{

  if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) return;
  if (!current_user_can('edit_post', $post_id)) return;
  if ($post->post_type != 'rewards') return;

  if (isset($_POST['upload_image_id'])) {
    update_post_meta($post_id, '_image_id', $_POST['upload_image_id']);
  }


  if (isset($_POST['reward-start'])) {
    update_post_meta($post_id, 'start', $_POST['reward-start']);
  } else {
    update_post_meta($post_id, 'start', time());
  }
  if (isset($_POST['reward-end'])) {
    update_post_meta($post_id, 'end', $_POST['reward-end']);
  } else {
    update_post_meta($post_id, 'end', time());
  }
}

add_action('admin_menu', 'rewards_register_ref_page');

function rewards_register_ref_page()
{
  add_submenu_page(
    'edit.php?post_type=rewards',
    __('Points', 'al_youcoached'),
    __('Points', 'al_youcoached'),
    'manage_options',
    'points_value',
    'points_value'
  );
  add_submenu_page(
    'edit.php?post_type=rewards',
    __('Raffle', 'al_youcoached'),
    __('Rafle', 'al_youcoached'),
    'manage_options',
    'rewards_raffle',
    'rewards_raffle'
  );
}


function points_value()
{
  $challenge_value = get_option('challenge_value', true);
  $selfgoal_value = get_option('selfgoal_value', true);

  var_dump($challenge_value, $selfgoal_value);

  $challenge_value = $challenge_value == '' ? 20 : $challenge_value;
  $selfgoal_value = $selfgoal_value == '' ? 50 : $selfgoal_value;
  ?>
  <div class="wrap full-height-div">
    <h1>Set points value for self-goals and default value for challenges</h1>
    <form action="" method="POST" enctype="multipart/form-data">
      <table class="form-table">
        <tbody>
        <tr>
          <th scope="row">
            <label for="challenge-value">Challenges default value</label>
          </th>
          <td>
            <input class="regular-text all-options" type="text" name="challenge-value" id="challenge-value"
                   value="<?php echo $challenge_value; ?>">
          </td>
        </tr>
        <!--        <tr>-->
        <!--          <th scope="row">-->
        <!--            <label for="selfgoal-value">Self-goal value</label>-->
        <!--          </th>-->
        <!--          <td>-->
        <!--            <input class="regular-text all-options" type="text" name="selfgoal-value" id="selfgoal-value" value="-->
        <?php //echo $selfgoal_value;
        ?><!--">-->
        <!--          </td>-->
        <!--        </tr>-->
        </tbody>
      </table>

      <p class="submit">

        <input type="submit" name="submit" class="button button-primary save-default-points"
               value="Save Changes">

      </p>

    </form>

    <div id="load-main">
      <div id='load'>
        <div id='block_1' class='load_block'></div>
        <div id='block_2' class='load_block'></div>
        <div id='block_3' class='load_block'></div>
      </div>
    </div>

  </div>
  <?php
}


function rewards_raffle()
{
  $args = array(
    'post_type' => 'rewards',
    'post_status' => 'publish',
    'posts_per_page' => -1,
//    'meta_query' => array(
////      'relation' => 'AND',
////      array(
////        'key'=> 'start',
////        'value' => date('d-m-Y'),
////        'compare' => '<=',
////        'type' => 'NUMERIC',
////      ),
////      array(
////        'key' => 'end',
////        'value' => date('d-m-Y'),
////        'compare' => '<',
////        'type' => 'NUMERIC',
////      ),
//    )

  );
  $data['posts'] = $clean_arr = array();
  $posts_array_all = get_posts($args);
  foreach ($posts_array_all as $reward) {
    $end = get_post_meta($reward->ID, 'end', true);
    $end_unix = $end ? strtotime($end) : false;
    if ($end_unix <= time()) {
      $clean_arr[] = $reward;
    }
  }
  $posts_array = $clean_arr;
  if (empty($posts_array)) { ?>
    <h2 class="ar-rew-title">No ended rewards</h2>
    <?php
    exit;
  }
  ?>
  <h2 class="ar-rew-title"> Rewards </h2>
  <?php
  foreach ($posts_array as $post) {

    $applied_users = get_post_meta($post->ID, 'applied_users', true);
    if ($applied_users) { ?>
      <div class="reward-one">
        <div class="reward-content">
          <div class="al-left-one">
            <p class="ar-inl">Reward - </p>
            <h2 class="ar-inl"><?php echo $post->post_title; ?></h2>
            <?php
            $applied_users = unserialize($applied_users);
            $count = 1;
            ob_start();
            foreach ($applied_users as $k => $v) {
              $user = get_user_by('id', $k);
              $user_name = $user->first_name . ' ' . $user->last_name;
              $user_name = $user_name == ' ' ? $user->nickname : $user_name;
              $i = 1;
              while ($i <= intval($v)) {
                ?>
                <li><?php echo $user_name; ?> <span> - <?php echo $user->user_email; ?></span> <a
                    href="mailto:<?php echo $user->user_email; ?>">send email</a></li>
                <?php
                $i++;
                $count++;
              }
            }
            $li = ob_get_contents();
            ob_end_clean();
            ?>
            <p class="no-marg">members quantity - <span class="ar-bold"><?php echo --$count; ?></span></p>
            <p class="no-marg">members list:</p>
            <ol class="hide-content al-randomize find-rand">
              <?php
              echo $li;
              ?>
            </ol>
            <div class="clear"></div>
          </div>
          <div class="al-right-one">
            <button class="select-winner">select winner</button>
            <p class=" ">winner:</p>
            <p class="al-result al-result-raffle"></p>
            <button class="ar-trash-reward" data-id="<?php echo $post->ID; ?>">Mark as a closed</button>

          </div>

          <div class="clear"></div>

        </div>
      </div>
      <?php
    }

  }
}

add_action('wp_ajax_al_update_points_value', 'al_update_points_value');

function al_update_points_value()
{
  if (!isset($_POST['challenge-points']) && !isset($_POST['selfgoal-points'])) {
    echo 'no data';
    die;
  }
  if (!is_user_logged_in()) {
    echo 'not_logged';
    die;
  }

  $points_chall = intval($_POST['challenge-points']);
  $points_selfgoal = intval($_POST['selfgoal-points']);

  if ($points_chall < 0) {
    $points_chall = 1;
  }
  if ($points_chall > 100) {
    $points_chall = 100;
  }

  if ($points_selfgoal < 0) {
    $points_selfgoal = 1;
  }
  if ($points_selfgoal > 100) {
    $points_selfgoal = 100;
  }

  $old_val_chall = get_option('challenge_value', true);
  $old_val_selfgoal = get_option('selfgoal_value', true);

  if ($old_val_chall !== $points_chall)
    $res1 = update_option('challenge_value', $points_chall, false);

  if ($old_val_selfgoal !== $points_selfgoal)
    $res2 = update_option('selfgoal_value', $points_selfgoal, false);

  if ((isset($res1) && $res1) && (isset($res2) && $res2)) {
    echo 'done';
    die;
  }
  if (!isset($res1) && !isset($res2)) {
    echo 'done';
    die;
  }

  if ((isset($res1) && !$res1) || (isset($res2) && !$res2)) {
    echo 'error';
    die;
  }

}


add_action('wp_ajax_al_apply_reward', 'al_apply_reward');
function al_apply_reward()
{
  if (!isset($_POST['id'])) {
    echo 'no data';
    die;
  }
  if (!is_user_logged_in()) {
    echo 'not_logged';
    die;
  }

  $id = intval($_POST['id']);
  if (!$id) {
    echo 'no ID';
    die;
  }

  $user = wp_get_current_user();

  apply_to_reward_main($user->ID, $id);

}

function apply_to_reward_main($user_id, $id)
{

  $entries_user = get_user_meta($user_id, 'entries', true);
  if (!$entries_user) {
    __json(array('error' => 'no entries'));
  }

  if ($entries_user < 1) {
    __json(array('error' => 'not enough entries'));
  }

  $old_val = get_user_meta($user_id, 'applied_rewards', true);


  if (!$old_val) {
    $new_val = array($id => 1);
  } else {
    $new_val = unserialize($old_val);
    if (array_key_exists($id, $new_val)) {
      $new_val[$id] = $new_val[$id] + 1;
    } else {
      $new_val[$id] = 1;
    }
  }


  $left_entries = decrease_entry_user($user_id);
  if ($left_entries === false) {
    __json(array('error' => 'not enough entries'));
  }
  $left_entries = intval($left_entries);
//  $left_entries = $left_entries ? $left_entries : 'error';
  add_user_to_reward($user_id, $id);
  $res = update_user_meta($user_id, 'applied_rewards', serialize($new_val));

  $res ? __json(array('applied_reward' => $id, 'left_entries' => $left_entries, 'applied_to_this' => $new_val[$id])) : __json(array('error' => 'error'));
}


function add_user_to_reward($user_id, $post_id)
{
  if (!$user_id || !$post_id)
    return false;

  $old_val = get_post_meta($post_id, 'applied_users', true);

  if (!$old_val) {
    $new_val = array($user_id => 1);
  } else {
    $new_val = unserialize($old_val);
    if (array_key_exists($user_id, $new_val)) {
      $new_val[$user_id] = $new_val[$user_id] + 1;
    } else {
      $new_val[$user_id] = 1;
    }
  }
  return update_post_meta($post_id, 'applied_users', serialize($new_val));
}


add_action('wp_ajax_al_trash_reward', 'al_trash_reward');
function al_trash_reward()
{
  if (!is_user_logged_in()) {
    echo 'error';
    die;
  }

  if (!isset($_POST['id'])) {
    echo 'error';
    die;
  }

  $id = intval($_POST['id']);

  if (!$id) {
    echo 'error';
    die;
  }

  $reward = get_post($id);

  if (!$reward) {
    echo 'error';
    die;
  }

  if ($reward->post_type != 'rewards') {
    echo 'error';
    die;
  }

  if (wp_trash_post($id)) {
    echo 'done';
  } else {
    echo 'error';
  }
  die;
}


add_action('wp_ajax_al_change_user_to_man', 'al_change_user_to_man');
function al_change_user_to_man()
{

  $user = wp_get_current_user();
  if (!$user)
    return false;

  if (isset($_POST['addToList']) && $_POST['addToList']) {
    al_add_to_men_subscriber($user->ID);
  }
  $res = update_user_meta($user->ID, 'sex', 'man');
  wp_logout();
  echo $res ? 'done' : 'error';
  die;
}


function al_add_to_men_subscriber($user_id)
{
  if (!$user_id)
    return false;

  $all_subscr = get_option('men_subscribers');
  if ($all_subscr) {
    $all_subscr = unserialize($all_subscr);
  } else {
    $all_subscr = array();
  }
  $all_subscr[] = $user_id;

  update_option('men_subscribers', $all_subscr);
}


add_action('admin_menu', 'al_men_subscriber_list');

function al_men_subscriber_list()
{
  add_menu_page('Men Subscribers', 'Men Subscribers', 'administrator', 'men_subscriber_page', 'men_subscriber_page', 'dashicons-clipboard', 35);
}

function men_subscriber_page()
{
  $args = array(
    'meta_key'     => 'sex',
    'meta_value'   => 'man'
  );
  $users = get_users( $args );
  $men_subscribers = get_option('men_subscribers', true);
  ?>
  <div class="wrap">
    <h1>Men  list:</h1>

    <?php
    if ($users) {
      ?>
      <table>
        <tr>
          <th>is subscribed</th>
          <th>name</th>
          <th>email</th>
          <th>action</th>
        </tr>

        <?php
        foreach ($users as $user) {
          echo '<tr>';
          echo in_array($user->ID, $men_subscribers) ?  '<td>yes</td>' : '<td>no</td>';
          echo '<td>'.$user->nickname.'</td>';
          echo '<td>'.$user->user_email.'</td>';
          echo '<td><button class="js-change-gender" data-id=" ' . $user->ID . ' ">Change gender</button></td>';
        }
        echo '</tr>';

        ?>
      </table>


      <?php
    } else {
      ?>
      <p>There are no men subscribed yet</p>
      <?php
    }
    ?>
  </div>
  <?php
}

/**
 * Adds Foo_Widget widget.
 */
class Footer_Copies extends WP_Widget
{

  /**
   * Register widget with WordPress.
   */
  function __construct()
  {
    parent::__construct(
      'footer_copy', // Base ID
      __('Footer Copy', 'al_youcoached'), // Name
      array('description' => __('Footer Copies', 'al_youcoached'),) // Args
    );
  }

  /**
   * Front-end display of widget.
   *
   * @see WP_Widget::widget()
   *
   * @param array $args Widget arguments.
   * @param array $instance Saved values from database.
   */
  public function widget($args, $instance)
  {
    echo $args['before_widget'];
    if (!empty($instance['title'])) {
      echo $args['before_title'] . apply_filters('widget_title', $instance['title']) . $args['after_title'];
    }
    if (!empty($instance['top_text'])) { ?>
      <p><?php echo $instance['top_text']; ?></p>
      <?php
    }
    if (!empty($instance['link_1'])) { ?>
      <a href="<?php echo $instance['link_1_href']; ?>" class="links"><?php echo $instance['link_1']; ?></a><br/>
      <?php
    }
    if (!empty($instance['link_2'])) { ?>
      <a href="<?php echo $instance['link_2_href']; ?>" class="links"><?php echo $instance['link_2']; ?></a><br/>
      <?php
    }
    if (!empty($instance['link_3'])) { ?>
      <a href="<?php echo $instance['link_3_href']; ?>" class="links"
         id='contact-us-button'><?php echo $instance['link_3']; ?></a><br/>
      <?php
    }
    if (!empty($instance['link_4'])) { ?>
      <a href="<?php echo $instance['link_4_href']; ?>" class="links"><?php echo $instance['link_4']; ?></a><br/>
      <?php
    }

    echo $args['after_widget'];
  }

  /**
   * Back-end widget form.
   *
   * @see WP_Widget::form()
   *
   * @param array $instance Previously saved values from database.
   */
  public function form($instance)
  {
    $top_text = !empty($instance['top_text']) ? $instance['top_text'] : __('YouCoached©2016', 'al_youcoached');

    $link_1 = !empty($instance['link_1']) ? $instance['link_1'] : __('Terms of use', 'al_youcoached');
    $link_1_href = !empty($instance['link_1_href']) ? $instance['link_1_href'] : __('#', 'al_youcoached');

    $link_2 = !empty($instance['link_2']) ? $instance['link_2'] : __('Privacy Policy', 'al_youcoached');
    $link_2_href = !empty($instance['link_2_href']) ? $instance['link_2_href'] : __('#', 'al_youcoached');

    $link_3 = !empty($instance['link_3']) ? $instance['link_3'] : __('Contact us', 'al_youcoached');
    $link_3_href = !empty($instance['link_3_href']) ? $instance['link_3_href'] : __('#', 'al_youcoached');

    $link_4 = !empty($instance['link_4']) ? $instance['link_4'] : __('Giveaway Rules', 'al_youcoached');
    $link_4_href = !empty($instance['link_4_href']) ? $instance['link_4_href'] : __('#', 'al_youcoached');
    ?>
    <p>
      <label
        for="<?php echo esc_attr($this->get_field_id('top_text')); ?>"><?php _e(esc_attr('Top text:')); ?></label>
      <input class="widefat" id="<?php echo esc_attr($this->get_field_id('top_text')); ?>"
             name="<?php echo esc_attr($this->get_field_name('top_text')); ?>" type="text"
             value="<?php echo esc_attr($top_text); ?>">
    </p>
    <br>
    <p>
      <label
        for="<?php echo esc_attr($this->get_field_id('link_1')); ?>"><?php _e(esc_attr('Link 1 text')); ?></label>
      <input class="widefat" id="<?php echo esc_attr($this->get_field_id('link_1')); ?>"
             name="<?php echo esc_attr($this->get_field_name('link_1')); ?>" type="text"
             value="<?php echo esc_attr($link_1); ?>">
    </p>
    <p>
      <label
        for="<?php echo esc_attr($this->get_field_id('link_1_href')); ?>"><?php _e(esc_attr('Link 1 leads:')); ?></label>
      <input class="widefat" id="<?php echo esc_attr($this->get_field_id('link_1_href')); ?>"
             name="<?php echo esc_attr($this->get_field_name('link_1_href')); ?>" type="text"
             value="<?php echo esc_attr($link_1_href); ?>">
    </p>
    <br>
    <p>
      <label
        for="<?php echo esc_attr($this->get_field_id('link_2')); ?>"><?php _e(esc_attr('Link 2 text')); ?></label>
      <input class="widefat" id="<?php echo esc_attr($this->get_field_id('link_2')); ?>"
             name="<?php echo esc_attr($this->get_field_name('link_2')); ?>" type="text"
             value="<?php echo esc_attr($link_2); ?>">
    </p>
    <p>
      <label
        for="<?php echo esc_attr($this->get_field_id('link_2_href')); ?>"><?php _e(esc_attr('Link 2 leads:')); ?></label>
      <input class="widefat" id="<?php echo esc_attr($this->get_field_id('link_2_href')); ?>"
             name="<?php echo esc_attr($this->get_field_name('link_2_href')); ?>" type="text"
             value="<?php echo esc_attr($link_2_href); ?>">
    </p>

    <br>
    <p>
      <label
        for="<?php echo esc_attr($this->get_field_id('link_3')); ?>"><?php _e(esc_attr('Link 3 text')); ?></label>
      <input class="widefat" id="<?php echo esc_attr($this->get_field_id('link_3')); ?>"
             name="<?php echo esc_attr($this->get_field_name('link_3')); ?>" type="text"
             value="<?php echo esc_attr($link_3); ?>">
    </p>
    <p>
      <label
        for="<?php echo esc_attr($this->get_field_id('link_3_href')); ?>"><?php _e(esc_attr('Link 3 leads:')); ?></label>
      <input class="widefat" id="<?php echo esc_attr($this->get_field_id('link_3_href')); ?>"
             name="<?php echo esc_attr($this->get_field_name('link_3_href')); ?>" type="text"
             value="<?php echo esc_attr($link_3_href); ?>">
    </p>

    <br>
    <p>
      <label
        for="<?php echo esc_attr($this->get_field_id('link_4')); ?>"><?php _e(esc_attr('Link 4 text')); ?></label>
      <input class="widefat" id="<?php echo esc_attr($this->get_field_id('link_4')); ?>"
             name="<?php echo esc_attr($this->get_field_name('link_4')); ?>" type="text"
             value="<?php echo esc_attr($link_4); ?>">
    </p>
    <p>
      <label
        for="<?php echo esc_attr($this->get_field_id('link_4_href')); ?>"><?php _e(esc_attr('Link 4 leads:')); ?></label>
      <input class="widefat" id="<?php echo esc_attr($this->get_field_id('link_4_href')); ?>"
             name="<?php echo esc_attr($this->get_field_name('link_4_href')); ?>" type="text"
             value="<?php echo esc_attr($link_4_href); ?>">
    </p>

    <?php
  }

  /**
   * Sanitize widget form values as they are saved.
   *
   * @see WP_Widget::update()
   *
   * @param array $new_instance Values just sent to be saved.
   * @param array $old_instance Previously saved values from database.
   *
   * @return array Updated safe values to be saved.
   */
  public function update($new_instance, $old_instance)
  {
    $all_items = array('top_text', 'link_1', 'link_1_href', 'link_2', 'link_2_href', 'link_3', 'link_3_href', 'link_4', 'link_4_href');
    $instance = array();
    foreach ($all_items as $all_item) {
      $instance[$all_item] = (!empty($new_instance[$all_item])) ? strip_tags($new_instance[$all_item]) : '';
    }
//    $instance['top_text'] = ( ! empty( $new_instance['top_text'] ) ) ? strip_tags( $new_instance['top_text'] ) : '';

    return $instance;
  }

} // class Foo_Widget

// register Foo_Widget widget
function register_footer_widget()
{
  register_widget('Footer_Copies');
}

add_action('widgets_init', 'register_footer_widget');

function arphabet_widgets_init()
{

  register_sidebar(array(
    'name' => 'Footer Copies',
    'id' => 'footer_copies_sidebar',
    'before_widget' => '<div>',
    'after_widget' => '</div>',
    'before_title' => '<h2 class="rounded">',
    'after_title' => '</h2>',
  ));

}

add_action('widgets_init', 'arphabet_widgets_init');


add_action('wp_ajax_al_logout', 'al_logout');
function al_logout()
{
  wp_logout();
  echo 'done';
  die;
}

function main_send_contact_form($user_id, $message, $name = false, $email = false)
{

  if (!trim($message)) {
    echo 'no message';
    die;
  }

  if ($user_id) {
    $user = get_user_by('id', $user_id);

    if ($user === false) {
      echo 'no such user';
      die;
    }

    $email = $user->user_email;
    $name = get_user_name($user->ID);
  } else {
    if (!trim($name)) {
      echo 'no name';
      die;
    }
    $name = ucfirst(strtolower($name));
    if (!is_email(esc_attr($email))) {
      echo 'incorrect email';
      die;
    }
  }

  $admin_email = get_option('admin_email');
  if (!$admin_email) {
    echo 'no admin email';
    die;
  }

  $headers = 'From: ' . $name . ' <' . $email . '>' . "\r\n";

  $res = wp_mail($admin_email, 'YouCoached contact form', $message, $headers);

  return $res;
}

add_action('wp_ajax_al_send_contact_email', 'al_send_contact_email');
add_action('wp_ajax_nopriv_al_send_contact_email', 'al_send_contact_email');
function al_send_contact_email()
{

  if (!isset($_POST)) {
    echo 'no POST';
    die;
  }
  $message = isset($_POST['message']) ? $_POST['message'] : '';

  if (is_user_logged_in()) {
    $user = wp_get_current_user();
    $user_id = $user->ID;
    echo main_send_contact_form($user_id, $message) ? 'done' : 'error';
  } else {
    $user_id = false;
    $email = isset($_POST['userEmail']) ? $_POST['userEmail'] : '';
    $user_name = isset($_POST['name']) ? $_POST['name'] : '';
    echo main_send_contact_form($user_id, $message, $user_name, $email) ? 'done' : 'error';;
  }
  die;
}

function get_user_name($user_id)
{
  if (!$user_id) return false;

  $user = get_user_by('id', $user_id);

  if ($user === false) return false;

  $user_name = $user->user_nicename;

  if ($user->first_name || $user->last_name) {
    $user_name = $user->first_name . ' ' . $user->last_name;
  }

  return trim($user_name);
}

// works only on when user has some quiz data in meta
function add_date_to_quiz($user_id)
{
  if (!$user_id) return false;

  $res = get_user_meta($user_id, 'quiz', true);
  if ($res) {
    $old_val = unserialize($res);
    $old_val['date'] = strtotime(date("Y") . '-' . date("m") . '-' . date("d"));
    $new_val = serialize($old_val);
  } else {
    // time can be added only to passed quiz. If there no data in quiz - error, can't work that way
    return false;
  }

  $update = update_user_meta($user_id, 'quiz', $new_val);
  return $update;
}

// add data to user inbox
function add_new_messages_into_user_inbox($user_id, $after_quiz = false)
{
  remove_filter('pre_get_posts', 'posts_for_current_author');

  global $wpdb;
  $quiz = get_user_meta($user_id, 'quiz', true);
  $quiz = $quiz ? unserialize($quiz) : array();
  if (!isset($quiz['date'])) {
    // user has no quiz yet
    return 'no date key in quiz';
  } else {
    $quiz_passed_date = intval($quiz['date']);
  }

  if (!$quiz_passed_date) {
//    echo 'user quiz data error';
    return 'incorrect date in quiz';
  }
//echo 'quiz was passed';
//var_dump($quiz_passed_date);


// getting camp IDS for current day
//  $camp_ids_calendar_today = al_get_campaign_ids_today();


// how many times campaign was published since user passed a quiz


// all campaigns in calendar since user passed a quiz
  $current_date = strtotime(date("Y") . '-' . date("m") . '-' . date("d"));
  $table = $wpdb->prefix . 'al_postscat_by_date';
  $sql = "SELECT category FROM $table WHERE date BETWEEN $quiz_passed_date AND $current_date";
  $calendar_camp = $wpdb->get_results($sql, ARRAY_A);
  $camp_calendar_history_arr = array();
  foreach ($calendar_camp as $item) {
    $camp_ids_range = isset($item['category']) ? $item['category'] : '';
    $camp_calendar_history_arr[] = array_filter(explode(',', $camp_ids_range));
  }

//echo '<pre>';
//print_r($camp_calendar_history_arr);
//echo '</pre>';
//die;// end


  $camp_ids_related_to_quiz = array();
  foreach ($quiz as $k => $v) {
    // skip for gender and personality
    if ($k == 'user_personality') continue;

    $args = array(
      'hide_empty' => true,
      'meta_query' => array(
        array(
          'key' => 'relations',
          'value' => $v,
          'compare' => 'LIKE'
        )
      ),
      'suppress_filters' => false,);

    $campaigns = get_terms($args);
    if ($campaigns) {
      foreach ($campaigns as $campaign) {
        $camp_ids_related_to_quiz[] = $campaign->term_id;
      }
    }
  }
  $camp_ids_related_to_quiz = array_unique($camp_ids_related_to_quiz);


// searching how many times users camp was published in calendar
  $camp_related_to_quiz_all_info = array();
  foreach ($camp_ids_related_to_quiz as $campaign) {
    $camp_related_to_quiz_all_info[$campaign]['was_in_calendar'] = 0;
    foreach ($camp_calendar_history_arr as $one_day) {
      if (in_array($campaign, $one_day)) $camp_related_to_quiz_all_info[$campaign]['was_in_calendar'] = $camp_related_to_quiz_all_info[$campaign]['was_in_calendar'] + 1;
    }
  }

// end

// how many messages exist in calendar/quiz campaigns
  $how_many_messages_in_campaign = array();
//  global $wp_query;
  foreach ($camp_ids_related_to_quiz as $item) {
    global $wpdb, $post, $wp_query;

//    var_dump($item);
    $args = array(
      'post_type' => 'message',
      'posts_per_page' => -1,
      'tax_query' => array(
        array(
          'taxonomy' => 'campaign',
          'field' => 'id',
          'terms' => array($item),
        ),
      )
    );

    $query = new WP_Query($args);
//    var_dump($query);
//var_dump($query->have_posts());
    $camp_related_to_quiz_all_info[$item]['has_messages'] = 0;
    if ($query->have_posts()) {
      $camp_related_to_quiz_all_info[$item]['has_messages'] = $query->post_count;
    }
  }

//echo '<h1>Here is result of checking camp in calendar repetitions and how many messages it has since passed a quiz </h1>';
//echo '<pre>';
//print_r($camp_related_to_quiz_all_info);
//echo '</pre>';
//die;


// get user campaigns that need already will be shown
  $user_camp_to_show = get_user_meta($user_id, 'camp_to_show', true);
  $user_camp_to_show = $user_camp_to_show ? $user_camp_to_show : array();

  // Store previous state
  $user_camp_to_show_old = $user_camp_to_show;
  update_user_meta($user_id, 'camp_to_show_prev', $user_camp_to_show_old);

  // if its a cron
  if (!$after_quiz) {

    foreach ($camp_related_to_quiz_all_info as $camp_id => $value) {
      $user_camp_to_show[$camp_id] = $value['was_in_calendar'] <= $value['has_messages'] ? $value['was_in_calendar'] : $value['has_messages'];
    }

    update_user_meta($user_id, 'camp_to_show', $user_camp_to_show);
    global $wpdb, $post, $wp_query;

    /* Hell goes here */

    $completed_challenges = get_user_meta($user_id, 'completed_challenges', true);
    $completed_challenges = $completed_challenges ? array_keys(unserialize($completed_challenges)) : array();

    $not_in = array_merge($completed_challenges);

    // all messages that were already viwed
    $viewed_messages = get_user_meta($user_id, 'viewed_messages', true);
    $viewed_messages_ids = $viewed_messages ? $viewed_messages : array();
    $viewed_challenges = get_user_meta($user_id, 'viewed_challenges', true);

    // only challenges that were already viwed
    $viewed_challenges = $viewed_challenges ? $viewed_challenges : array();
    $viewed_challenges_messages = array_merge($viewed_messages_ids, $viewed_challenges);

    // camp schedule
    $user_camp_sch_prev = get_user_meta($user_id, 'camp_to_show_prev', true);

    $user_camp_sch = get_user_meta($user_id, 'camp_to_show', true);

    if (!$user_camp_sch) {
      return;
    }

    $diff = array();
    foreach ($user_camp_sch as $k => $v) {
      if(isset($user_camp_sch_prev[$k])){
        if($user_camp_sch[$k] > $user_camp_sch_prev[$k]){
          $diff[$k] = $v;
        }
      }else{
        $diff[$k] = $v;
      }
    }
    // if there is nothing new - return
    if(empty($diff)){
      return;
    }

    // not empty - show scheduled messages and campaigns
    $home_user_camp_main_arr = array();
    foreach ($diff as $camp_id => $count) {
      $parent_id = get_term_meta($camp_id, 'parent', true);
      // check if number of posts for campaign > 0
      if ($count)
        $home_user_camp_main_arr[$parent_id][$camp_id] = $count;
    }
    $arr_of_posts = array();
    foreach ($home_user_camp_main_arr as $main_cat => $campaigns) {

      foreach ($campaigns as $campaign => $count_new_messages) {

        $args = array(
          'post_type' => 'message',
          'post__not_in' => $not_in,
          'posts_per_page' => -1,
          'tax_query' => array(
            array(
              'taxonomy' => 'campaign',
              'field' => 'id',
              'terms' => $campaign,
            ),
          )
        );

        $query = new WP_Query($args);
        if ($query->have_posts()) {

          $zero_arr = $order_arr = array();

          foreach ($query->posts as $post) {
            $image_id = get_post_meta(get_the_ID(), '_image_id', true);
            $image_src = wp_get_attachment_url($image_id);
            $image_src = $image_src ? $image_src : get_template_directory_uri() . '/img/card1.png';
            if ($post->menu_order == 0) {
              $post_arr = array(  "title" => $post->post_title,
                "link" => get_permalink($post),
                "cat_name" => get_term_field('name', $main_cat),
                "is_new_for_user" => !in_array(get_the_ID(), $viewed_challenges_messages),
                "img_src" => $image_src,
                "category" => get_term_link($main_cat)
              );
              array_unshift($zero_arr, $post_arr);
            } else {
              $post_arr = array(  'title' => $post->post_title,
                "link" => get_permalink($post),
                "cat_name" => get_term_field('name', $main_cat),
                "is_new_for_user" => !in_array(get_the_ID(), $viewed_challenges_messages),
                "img_src" => $image_src,
                "category" => get_term_link($main_cat)
              );
              $order_arr[] = $post_arr;
            }
          }

          usort($order_arr, function ($a, $b) {
            return strcmp($a->menu_order, $b->menu_order);
          });

          $clean_array = array_merge($order_arr, $zero_arr);

          if(isset($clean_array[$count_new_messages]))
            array_push($arr_of_posts, $clean_array[$count_new_messages]);
        }
        wp_reset_postdata();
      }
    }

    $mail_arr = array();
    foreach ($arr_of_posts as $item) {
      if($item[0]['is_new_for_user'])
        array_push($mail_arr, $item[0]);
    }

    foreach ($mail_arr as $item) {
      $subject = "New message. {$item['cat_name']}";

      $message = "<h2>New message!</h2>";
      $message .= "<div>";
      $message .= "<h3 style=\"text-align: center\"><a href={$item['link']}>{$item['title']}</a></h3>";
      $message .= "<h4 style=\"text-align: center\"><a href={$item['category']}>{$item['cat_name']}</a></h4>";
      $message .= "<img style=\"display: block; margin: auto\" src={$item['img_src']} />";
      $message .= "</div>";

      wp_mail(get_userdata($user_id)->user_email, $subject, $message);
    }

  } else {
    // if its after quiz

    // new campaigns after quiz is passed
    $user_camp_to_show_new = array();
    foreach ($camp_related_to_quiz_all_info as $camp_id => $value) {
      $user_camp_to_show_new[$camp_id] = $value['was_in_calendar'] <= $value['has_messages'] ? $value['was_in_calendar'] : $value['has_messages'];
    }

    // find the difference between user already has campaigns and new that needs to be added. Add only new campaigns
    $user_camp_to_show_diff = array_diff_assoc($user_camp_to_show_new, $user_camp_to_show);

    $user_camp_to_show_clean = $user_camp_to_show;
    // add new campaigns and count to user meta
    foreach ($user_camp_to_show_diff as $key => $val) {
      $user_camp_to_show_clean[$key] = $val;
    }

    update_user_meta($user_id, 'camp_to_show', $user_camp_to_show_clean);
    return get_user_meta($user_id, 'camp_to_show');
  }
}


/*
	Change Expiration Text
*/
function my_pmpro_level_expiration_text($text, $level)
{
  if ($level->expiration_number) {
    $expiration_text = "";
  } else
    $expiration_text = "";

  return $expiration_text;
}

add_filter("pmpro_level_expiration_text", "my_pmpro_level_expiration_text", 5, 2);

add_action('wp_ajax_al_change_user_to_female', 'al_change_user_to_female');

function al_change_user_to_female(){

  $user_id = $_POST['id'] ? intval($_POST['id']) : false;

  if(!$user_id){
    return false;
  }
  $user = get_userdata( $user_id );
  if ( $user === false ) {
    // there is no such a user
    return false;
  }
  $subsc_men = get_option('men_subscribers', true);

  if($subsc_men){
    // there are no men subscribers
    foreach ($subsc_men as $k => $v) {
      if($v == $user_id){
        unset($subsc_men[$k]);
      }
    }

    update_option('men_subscribers', $subsc_men);
  }

  $res = delete_user_meta($user_id, 'sex');
  echo $res ? 'done' : 'error';
  die;
}


include_once('include/challenge-post-type.php');
