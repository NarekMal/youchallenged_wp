<?php


////  http://youcoached.com/wp-json/api/test

//add_action( 'rest_api_init', function () {

//  register_rest_route( 'api', '/test', array(

//    'methods' => 'POST',

//    'callback' => 'al_api_test',

//  ) );

//} );

//function al_api_test( WP_REST_Request $request ) {

//  $slug = $request->get_param( 'ID' );

//  var_dump($slug);

//  __json( is_user_logged_in() );

//}


add_action('rest_api_init', function () {

    $_POST = $_SERVER['REQUEST_METHOD'] === 'POST' ? json_decode(file_get_contents('php://input'), true) : $_GET;

});


//  http://youcoached.com/wp-json/api/user_quiz

add_action('rest_api_init', function () {

    register_rest_route('api', '/user_quiz', array(

        'methods' => 'POST',

        'callback' => 'al_user_quiz_api',

    ));

});

/**
 * Records user quiz answers
 * takes 'quest', 'answer'
 *  returns 'done' or 'error'
 * */

function al_user_quiz_api(WP_REST_Request $request)
{

    if (is_user_logged_in()) {

        $key = $request->get_param('quest');

        $val = strtolower(str_replace(' ', '_', $request->get_param('answer')));

        $key = strip_tags(trim($key));
        $val = strip_tags(trim($val));
        if (!$val || !$key) {
            __json('error');
        }


        $user = wp_get_current_user();

        $res = get_user_meta($user->ID, 'quiz', true);

        if ($res) {

            $old_val = unserialize($res);

            if ($old_val[$key] == $val) {

                __json('done');

            }

            $old_val[$key] = $val;

            $new_val = serialize($old_val);

        } else {

            $new_val = serialize(array($key => $val));

        }

        $res = update_user_meta($user->ID, 'quiz', $new_val);


        $res ? __json('done') : __json('error');

    }

}


//  http://youcoached.com/wp-json/api/personality_quiz

add_action('rest_api_init', function () {

    register_rest_route('api', '/personality_quiz', array(

        'methods' => 'POST',

        'callback' => 'al_personality_quiz_api',

    ));

});


/**
 * Records users personality quiz
 * takes in json $data where all qusetions Q1, Q2 ... Q32
 *  returns 'done' or 'error'
 * */

function al_personality_quiz_api(WP_REST_Request $request)
{

    if (is_user_logged_in()) {

        $user = wp_get_current_user();

        $answ = json_decode(file_get_contents("php://input"));
		$IE = 30 - $answ->Q3 - $answ->Q7 - $answ->Q11 + $answ->Q15 - $answ->Q19 + $answ->Q23 + $answ->Q27 - $answ->Q31 > 24 ? 'E' : 'I';
		$SN = 12 + $answ->Q4 + $answ->Q8 + $answ->Q12 + $answ->Q16 + $answ->Q20 - $answ->Q24 - $answ->Q28 + $answ->Q32 > 24 ? 'N' : 'S';
		$FT = 30 - $answ->Q2 + $answ->Q6 + $answ->Q10 - $answ->Q14 - $answ->Q18 + $answ->Q22 - $answ->Q26 - $answ->Q30 > 24 ? 'T' : 'F';
		$JP = 18 + $answ->Q1 + $answ->Q5 - $answ->Q9 + $answ->Q13 - $answ->Q17 + $answ->Q21 - $answ->Q25 + $answ->Q29 > 24 ? 'P' : 'J';

/*         $IE = 30 - $answ['Q3'] - $answ['Q7'] - $answ['Q11'] + $answ['Q15'] - $answ['Q19'] + $answ['Q23'] + $answ['Q27'] - $answ['Q31'] > 24 ? 'E' : 'I';
        $SN = 12 + $answ['Q4'] + $answ['Q8'] + $answ['Q12'] + $answ['Q16'] + $answ['Q20'] - $answ['Q24'] - $answ['Q28'] + $answ['Q32'] > 24 ? 'N' : 'S';
        $FT = 30 - $answ['Q2'] + $answ['Q6'] + $answ['Q10'] - $answ['Q14'] - $answ['Q18'] + $answ['Q22'] - $answ['Q26'] - $answ['Q30'] > 24 ? 'T' : 'F';
        $JP = 18 + $answ['Q1'] + $answ['Q5'] - $answ['Q9'] + $answ['Q13'] - $answ['Q17'] + $answ['Q21'] - $answ['Q25'] + $answ['Q29'] > 24 ? 'P' : 'J'; */


        $personality_res = $IE . $SN . $FT . $JP;
        $res = get_user_meta($user->ID, 'quiz', true);


        if ($res) {
            $old_val = unserialize($res);
            if ($old_val['user_personality'] == $personality_res) {
                // update quiz date
                add_date_to_quiz($user->ID);
                // update users inbox
                add_new_messages_into_user_inbox($user->ID, true);

                al_create_selfie_badge($user->ID);
                __json('done');
            }

            $old_val['user_personality'] = $personality_res;
            $new_val = serialize($old_val);

        } else {

            $new_val = serialize(array('user_personality' => $personality_res));

        }


        $update = update_user_meta($user->ID, 'quiz', $new_val);
        // update quiz date
        add_date_to_quiz($user->ID);
        // update users inbox
        add_new_messages_into_user_inbox($user->ID, true);
        al_create_selfie_badge($user->ID);
        $update ? __json('done') : __json('error');
    }
}


//  http://youcoached.com/wp-json/api/save_to_favourite

add_action('rest_api_init', function () {

    register_rest_route('api', '/save_to_favourite', array(

        'methods' => 'POST',

        'callback' => 'al_save_to_favourite_api',

    ));

});

/**
 * Saves message to favourite
 * takes (int) message_id
 *  returns 'done' or 'error'
 * */

function al_save_to_favourite_api(WP_REST_Request $request)
{

    if (is_user_logged_in()) {

        $message_id = $request->get_param('message_id');

        $user = wp_get_current_user();

        if ($user->ID !== 0) {
            $res = get_user_meta($user->ID, 'saved_messages', true);
            if ($res) {
                if (in_array($message_id, $res)) {
                    $new_val = array_diff($res, array($message_id));
                } else {
                    $res[] = $message_id;
                    $new_val = $res;
                }
            } else {
                $new_val = array($message_id);
            }
            $update = update_user_meta($user->ID, 'saved_messages', $new_val);
            $update ? __json('done') : __json('error');
        }
    }
}


//  http://youcoached.com/wp-json/api/accept_challenge

add_action('rest_api_init', function () {

    register_rest_route('api', '/accept_challenge', array(

        'methods' => 'POST',

        'callback' => 'al_accept_challenge_api',

    ));

});

/**
 * Gives ability to accept challenge and undo
 * takes (int) message_id
 * returns 'added' or 'deleted' or 'error'
 * */

function al_accept_challenge_api(WP_REST_Request $request)
{

    if (is_user_logged_in()) {


        $message_id = $request->get_param('message_id');


        $user = wp_get_current_user();

        if ($user->ID !== 0) {

            $date_added = strtotime(date("Y") . '-' . date("m") . '-' . date("d"));

            $old_val = get_user_meta($user->ID, 'accepted_challenges', true);

            $action = '';


            if ($old_val) {

                $old_val = unserialize($old_val);

                if (array_key_exists($message_id, $old_val)) {

                    $action = 'deleted';

                    unset($old_val[$message_id]);

                    $new_val = $old_val;

                } else {

                    $action = 'added';

                    $new_val = $old_val;

                    $new_val[$message_id] = $date_added;
                    al_add_viewed_challenge($user->ID, $message_id);

                }

            } else {

                $action = 'added';

                $new_val = array($message_id => $date_added);

            }

            $update = update_user_meta($user->ID, 'accepted_challenges', serialize($new_val));

            $update ? __json($action) : __json('error');

        }

    }

}


//  http://youcoached.com/wp-json/api/complete_challenge

add_action('rest_api_init', function () {

    register_rest_route('api', '/complete_challenge', array(

        'methods' => 'POST',

        'callback' => 'al_complete_challenge_api',

    ));

});

/**
 * Gives ability to complete challenge and undo
 * takes (int) message_id
 * returns 'added' or 'deleted' or 'error'
 * */

function al_complete_challenge_api(WP_REST_Request $request)
{

    if (is_user_logged_in()) {
        $message_id = $request->get_param('message_id');
        $user = wp_get_current_user();
        if ($user->ID !== 0) {
            $date_added = strtotime(date("Y") . '-' . date("m") . '-' . date("d"));
            $old_val = get_user_meta($user->ID, 'completed_challenges', true);
            $points_challenge = get_post_meta($message_id, 'points', true);

            $accepted_cahllenges = get_user_meta($user->ID, 'accepted_challenges', true);
            $accepted_cahllenges = $accepted_cahllenges ? array_keys(unserialize($accepted_cahllenges)) : array();


            if (!in_array($message_id, $accepted_cahllenges)) {
                __json('not accepted');
            }

            $action = '';
            if ($old_val) {
                $old_val = unserialize($old_val);
                if (array_key_exists($message_id, $old_val)) {
                    // no deleting completed challenge (29.09.2016)
//          $action = 'deleted';
//          unset($old_val[$message_id]);
//          $new_val = $old_val;
//          al_del_viewed_challenge($user->ID,$message_id);
                    __json('already completed');
                } else {
                    al_add_viewed_challenge($user->ID, $message_id);
                    al_add_points_user($user->ID, $points_challenge);
                    $action = 'added';
                    $new_val = $old_val;
                    $new_val[$message_id] = $date_added;
                }
            } else {
                al_add_points_user($user->ID, $points_challenge);
                $action = 'added';
                $new_val = array($message_id => $date_added);
            }
            $update = update_user_meta($user->ID, 'completed_challenges', serialize($new_val));
            if ($update) {
                add_delete_accepted_challenge($user->ID, $message_id);
                __json($action);
            } else {
                __json('error');
            }
        }
    }
}


//  http://youcoached.com/


add_action('rest_api_init', function () {
    register_rest_route('api', '/add_self_goal', array(
        'methods' => 'POST',
        'callback' => 'al_add_self_goal_api',
    )); 
});

/**
 * Gives ability to add self goal
 * takes (string) goal
 * returns id of added goal or 'error', 'empty goal'
 * */
function al_add_self_goal_api(WP_REST_Request $request)
{
    if (is_user_logged_in()) {
        $text = $request->get_param('goal');
        if (empty($text)) {
            __json(array('status' => 'no goal'));
        }
        $text = strip_tags(trim($text));

        $category = strtolower($request->get_param('category'));
        if (!$category) {
          __json(array('status' => 'no category'));
            die;
        }

        $update = strtolower($request->get_param('update'));

        add_self_goal($text, $category, $update);
    }

}


add_action( 'rest_api_init', function () {
  register_rest_route( 'api', '/al_get_user_points', array(
    'methods' => 'GET',
    'callback' => 'al_get_user_points_api',
  ) );
} );

function al_get_user_points_api(){
  if(is_user_logged_in()){
    $arr = get_user_points_array();
    if($arr){
      __json($arr);
    }
    __json('error');
  }
}


//  http://youcoached.com/wp-json/api/status_self_goal
add_action('rest_api_init', function () {
    register_rest_route('api', '/status_self_goal', array(
        'methods' => 'POST',
        'callback' => 'al_status_self_goal_api',
    ));
});
function al_status_self_goal_api(WP_REST_Request $request)
{
    if (is_user_logged_in()) {
        $user = wp_get_current_user();
        $key = (int)$request->get_param('id');
        $old_val = get_user_meta($user->ID, 'self_goals', true);

        if (!$old_val) {
            return;
        } else {

            $old_val = unserialize($old_val);

            if (isset($old_val[$key]['status'])) {
                if ($old_val[$key]['status'] == 0) {
                    $old_val[$key]['status'] = 1;
                } else {
                    $old_val[$key]['status'] = 0;
                }
                // todo points self goal from admin
                $points_self_goal = 50;
                al_add_points_user($user->ID, $points_self_goal);
                $res = update_user_meta($user->ID, 'self_goals', serialize($old_val));
                $res ? __json('done') : __json('error');
            }
        }
    }
}


//  http://youcoached.com/wp-json/api/delete_self_goal

add_action('rest_api_init', function () {

    register_rest_route('api', '/delete_self_goal', array(

        'methods' => 'POST',

        'callback' => 'al_delete_self_goal_api',

    ));

});

/**
 * Deletes self goal
 * takes (int) id
 * returns 'done' or 'error'
 * */

function al_delete_self_goal_api(WP_REST_Request $request)
{

    if (is_user_logged_in()) {

        $user = wp_get_current_user();

        $key = (int)$request->get_param('id');

        $old_val = get_user_meta($user->ID, 'self_goals', true);


        if (!$old_val) {

            __json('no goals');


        } else {

            $old_val = unserialize($old_val);

            if (isset($old_val[$key])) {

                unset($old_val[$key]);

            }


            $res = update_user_meta($user->ID, 'self_goals', serialize($old_val));

            $res ? __json('done') : __json('error');

        }

    }

}


//  http://youcoached.com/wp-json/api/save_viewed_history

add_action('rest_api_init', function () {

    register_rest_route('api', '/save_viewed_history', array(

        'methods' => 'POST',

        'callback' => 'al_save_viewed_history_api',

    ));

});

/**
 * added viewed messaged to viewed ( after user have been seen it more than 3s )
 * takes (int) id
 * type (string) message / challenge
 * returns
 * */

function al_save_viewed_history_api(WP_REST_Request $request)
{


    if (is_user_logged_in()) {

        $message_id = (int)$request->get_param('message_id');

        $type = (int)$request->get_param('type');


        if ($type == 'challenge' || $type == 'message') {

            $message_type = $type;

        } else {

            __json('error');

        }


        $user = wp_get_current_user();


        if ($message_type == 'challenge') {

            // it's a challenge

            al_add_viewed_challenge($user->ID, $message_id);

        } else {

            // it's a message

            $old_val = get_user_meta($user->ID, 'viewed_messages', true);

            if (!empty($old_val)) {

                if (!in_array($message_id, $old_val)) {

                    $new_val = $old_val;

                    $new_val[] = $message_id;

                } else {

                    return;

                }

            } else {

                $new_val = array($message_id);

            }

            update_user_meta($user->ID, 'viewed_messages', $new_val);

        }

    }

}


//  http://youcoached.com/wp-json/api/save_profile_front

add_action('rest_api_init', function () {

    register_rest_route('api', '/save_profile_front', array(

        'methods' => 'POST',

        'callback' => 'save_profile_front_api',

    ));

});

/**
 * Editing user profile
 * @param email
 * @param pass1
 * @param pass2
 * @param first_name
 * @param last_name
 * @return done, Passwords do not match, incorrect email, email is used by another user,
 * */
function save_profile_front_api(WP_REST_Request $request)
{
//  __json( (is_user_logged_in()) ? ' Am I logged ? - true' : ' Am I logged ? - false');
    if (is_user_logged_in()) {
        $user = wp_get_current_user();
        $pass1 = $request->get_param('pass1');
        $pass2 = $request->get_param('pass2');


        /* Update user password. */
        if (!empty($pass1) && !empty($pass2)) {
            if ($pass1 == $pass2) {
                wp_update_user(array('ID' => $user->ID, 'user_pass' => esc_attr($pass1)));
            } else {
                __json('passwords do not match');
            }
        }

        $email = $request->get_param('email');
        if (!empty($email)) {
            if (!is_email(esc_attr($email))) {
                __json('incorrect email');
            } elseif (email_exists(esc_attr($email)) && $user->ID !== email_exists(esc_attr($email))) {
                __json('email is used by another user');
            } else {
                wp_update_user(array('ID' => $user->ID, 'user_email' => esc_attr($email)));
            }
        }


        $first_name = $request->get_param('first_name');

        if (!empty($first_name))

            update_user_meta($user->ID, 'first_name', esc_attr($first_name));


        $last_name = $request->get_param('last_name');

        if (!empty($last_name))

            update_user_meta($user->ID, 'last_name', esc_attr($last_name));


        __json('done');

    } else {
        __json('not logged');
    }

}


//  http://youcoached.com/wp-json/api/save_profile_image

add_action('rest_api_init', function () {

    register_rest_route('api', '/save_profile_image', array(

        'methods' => 'POST',

        'callback' => 'save_profile_image_api',

    ));

});


/**
 * Save profile image
 * @param $_FILES ['user_avatar']
 *
 * */


function save_profile_image_api(WP_REST_Request $request)
{


    if (is_user_logged_in()) {

        if (!isset($_POST['user_avatar']))

            __json('no file');


        $data = $_POST['user_avatar'];

//    __json($data);


        list($type, $data) = explode(';', $data);

        list(, $extension) = explode('/', $type);

        list(, $data) = explode(',', $data);

        $data = base64_decode($data);


        $user = wp_get_current_user();

        $upload_dir = wp_upload_dir();

        $avatar_dir = $upload_dir['basedir'] . "/avatars/" . $user->ID . '/';


        if (!file_exists($avatar_dir))

            $res = mkdir($avatar_dir, 0777, true);


        // deleting old avatar

        if (is_dir($avatar_dir) && $av_dir = opendir($avatar_dir)) {

            while (false !== ($avatar_file = readdir($av_dir))) {

                @unlink($avatar_dir . '/' . $avatar_file);

            }

            closedir($av_dir);

        }


        $res = file_put_contents($avatar_dir . time() . "-bpfull." . $extension, $data);


        if ($res) {

            __json('uploaded');

        }


    }

}

function save_profile_image_api_old(WP_REST_Request $request)
{


    if (is_user_logged_in()) {

        if (!isset($_FILES['user_avatar']))

            __json('no file');


        if ($_FILES['user_avatar']['size'] > 1048576)

            __json('file size more than 1MB');


        $ext = pathinfo($_FILES['user_avatar']['name'], PATHINFO_EXTENSION);

        if ($ext !== 'jpg' || $ext !== 'png')

            __json('not allowed file extension');


        $user = wp_get_current_user();


        $upload_dir = wp_upload_dir();

        $avatar_dir = $upload_dir['basedir'] . "/avatars/" . $user->ID . '/';


        if (!file_exists($avatar_dir))

            mkdir($avatar_dir, 0777, true);


        // deleting old avatar

        if (is_dir($avatar_dir) && $av_dir = opendir($avatar_dir)) {

            while (false !== ($avatar_file = readdir($av_dir))) {

                @unlink($avatar_dir . '/' . $avatar_file);

            }

            closedir($av_dir);

        }

        @rmdir($avatar_dir);


        $res = move_uploaded_file($_FILES['user_avatar']['tmp_name'], $avatar_dir . time() . "-bpfull." . $ext);


        __json($res);


    }

}


//  http://youcoached.com/wp-json/api/al_registration_front

add_action('rest_api_init', function () {

    register_rest_route('api', '/al_registration_front', array(

        'methods' => 'POST',

        'callback' => 'al_registration_front_api',

    ));

});


/**
 * Editing user profile
 * @param $_FILES ['user_avatar']
 * @param user_name
 * @param email
 * @param pass1
 * @param pass2
 * @return a lot of and 'done' if all done
 * */


function al_registration_front_api()
{

//  $json = file_get_contents('php://input');

//  $obj = json_decode($json);

//  __json($obj);

//  __json($_POST);


    if (isset($_POST)):


        if (empty($_POST['user_name'])) {

            __json('Please enter name');

        }


        if (empty($_POST['email'])) {

            __json('Please enter a email');

        }


        if (empty($_POST['pass1'])) {

            __json('Please enter a password');

        }


        if (empty($_POST['pass2'])) {

            __json('Please enter a confirm password');

        }


        if ((!empty($_POST['pass2']) && !empty($_POST['pass1'])) && ($_POST['pass1'] != $_POST['pass2'])) {

            __json('Entered password did not match');

        }


        $user_login = esc_attr($_POST['user_name']);

        $user_email = esc_attr($_POST['email']);

        $user_pass = esc_attr($_POST['pass1']);


        $sanitized_user_login = sanitize_user($user_login);

        $user_email = apply_filters('user_registration_email', $user_email);


        if (!is_email($user_email)) {

            __json('Invalid e-mail');

        } elseif (email_exists($user_email)) {

            __json('This email is already registered');

        }


        if (empty($sanitized_user_login) || !validate_username($user_login)) {

            __json('Invalid user name');

        } elseif (username_exists($sanitized_user_login)) {

            __json('User name already exists');

        }


        $user_id = wp_create_user($sanitized_user_login, $user_pass, $user_email);


        if (!$user_id) {

            __json('error');

        } else {

            update_user_option($user_id, 'default_password_nag', true, true);
            wp_new_user_notification($user_id, $user_pass);
            wp_cache_delete($user_id, 'users');
            wp_cache_delete($user_login, 'userlogins');
            do_action('user_register', $user_id);
            $user_data = get_userdata($user_id);
            if ($user_data !== false) {

                __json('done');
            }
        }


    endif;

}

/*======== 30.08.16 =========*/
//  http://youcoached.com/wp-json/api/al_apply_reward
// todo check this
add_action('rest_api_init', function () {
    register_rest_route('api', '/apply_reward', array(
        'methods' => 'POST',
        'callback' => 'al_apply_reward_api',
    ));
});

function al_apply_reward_api(WP_REST_Request $request)
{
    if (is_user_logged_in()) {
        $id = $request->get_param('id');

        if (!$id) {
            __json('no ID');
        }

        $id = intval($id);
        if (!$id) {
            __json('no ID');
        }

        $user = wp_get_current_user();

        apply_to_reward_main($user->ID, $id);

    } else {
        __json('not logged');
    }
}


/*======== 06.09.16 =========*/
//  http://youcoached.com/wp-json/api/al_retrieve_user_pass
add_action('rest_api_init', function () {
    register_rest_route('api', '/al_retrieve_user_pass', array(
        'methods' => 'POST',
        'callback' => 'al_retrieve_user_pass',
    ));
});

function al_retrieve_user_pass(WP_REST_Request $request)
{

    $login_user_post = $request->get_param('user_login');

    if (empty($login_user_post)) {
        __json('Enter a username or email address');
    } elseif (strpos($login_user_post, '@')) {
        $user_data = get_user_by('email', trim($login_user_post));
        if (empty($user_data))
            __json('There is no user registered with that email address');
    } else {
        $login = trim($login_user_post);
        $user_data = get_user_by('login', $login);
    }

    if (!$user_data) {
        __json('Invalid username or email');
    }

    // Redefining user_login ensures we return the right case in the email.
    $user_login = $user_data->user_login;
    $user_email = $user_data->user_email;
    $key = get_password_reset_key($user_data);

//  if ( is_wp_error( $key ) ) {
//    return $key;
//  }

    $message = __('Someone has requested a password reset for the following account:') . "\r\n\r\n";
    $message .= network_home_url('/') . "\r\n\r\n";
    $message .= sprintf(__('Username: %s'), $user_login) . "\r\n\r\n";
    $message .= __('If this was a mistake, just ignore this email and nothing will happen.') . "\r\n\r\n";
    $message .= __('To reset your password, visit the following address:') . "\r\n\r\n";
    $message .= '<' . network_site_url("wp-login.php?action=rp&key=$key&login=" . rawurlencode($user_login), 'login') . ">\r\n";

    if (is_multisite())
        $blogname = $GLOBALS['current_site']->site_name;
    else
        /*
         * The blogname option is escaped with esc_html on the way into the database
         * in sanitize_option we want to reverse this for the plain text arena of emails.
         */
        $blogname = wp_specialchars_decode(get_option('blogname'), ENT_QUOTES);

    $title = sprintf(__('[%s] Password Reset'), $blogname);

    $title = apply_filters('retrieve_password_title', $title, $user_login, $user_data);

    $message = apply_filters('retrieve_password_message', $message, $key, $user_login, $user_data);

    if ($message && !wp_mail($user_email, wp_specialchars_decode($title), $message))
        __json('The email could not be sent.');

    __json('done');
}

//  http://youcoached.com/wp-json/api/al_contact_us_form
add_action('rest_api_init', function () {
    register_rest_route('api', '/al_contact_us_form', array(
        'methods' => 'POST',
        'callback' => 'al_contact_us_form',
    ));
});

function al_contact_us_form(WP_REST_Request $request)
{
    $message = $request->get_param('message');
    if (!trim($message)) {
        __json('no message');
    }

    if (is_user_logged_in()) {
        $user = wp_get_current_user();
        main_send_contact_form($user->ID, $message) ? __json('done') : __json('error');
    } else {
        $name = $request->get_param('name');
        if (!trim($name)) {
            __json('no name');
        }

        $email = $request->get_param('email');
        if (!trim($email)) {
            __json('no email');
        }

        if (!is_email(esc_attr($email))) {
            __json('incorrect email');
        }

        main_send_contact_form(false, $message, $name, $email) ? __json('done') : __json('error');
    }
}